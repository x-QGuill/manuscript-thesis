1. Introduction
  1. The need for HPC
  2. The problems with current HPC systems/solutions
    - IO
  3. The harvesting problem
  4. Autonomic Computing
  5. Control Theory
  6. Experimentation

2. State-of-the-Art
  0. HPC?
  1. Harvesting
    - boinc
    - bebida
    - sc22 thing
    - nimrod
  2. Autonomic Computing
    - defs
    - concepts
    - examples
    - drawbacks
  3. Control Theory
    - defs
    - concepts
    - examples of application in CS
    - drawbacks
  4. Distributed Experimentation
    - what is difficult
    - enoslib, et al
    - reprendre le sota cluster
  5. Conclusion
    - explain the needs
      - harvesting of resources with respect to IO 
      - experimentation tools and methods for reproducible distributed experiments
3. CiGri
  1. Gricad
  2. What is the goal of CiGri
  3. The issues of CiGri
    - load on the scheduler
    - IO
  4. Conclusion: the need for adaptation and control

4. Modelization of CiGri jobs
  - Papier compas
  - HERE?
    - The argumentation goes with the folding
  
-- part: Controlling the injection of BE jobs

5. Harvesting of idle resources while regulating the load of the file system
  - PI load
  - moving refs based on darshan

6. Improving resource utilization from IO load knowledge
  - papier ICSTCC

7. Study of the reusability of the controllers
  - MFC
  - papier reuse

8. (Maybe) Taking into account the batch scheduler knowledge to avoid the killing of jobs
  - give the idea 
  - todo
  
Conclu?
  
-- part: Distributed experiments

9. Introduction on the problematic
  - def
  - why so difficult
  - what we'll look at:
    - reproducibility of distributed systems
    - folding of deployments

10. Reproducibility of distributed systems
  - papier nxc cluster

11. Folding of experiments
  - defs
  - why
  - vs Simulation
  - NFS
  - PVFS

Conclu?

12. Conclusion
