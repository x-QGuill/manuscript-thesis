---
#title: "Autonomic approach to runtime management of HPC cluster resources"
title: "Control-based runtime management of HPC systems with support for reproducible experiments"
titlefr: "Une approche automatique à la régulation en ligne de systèmes HPC, avec un support pour la reproductibilité des expériences"
author: 'Quentin \textsc{Guilloteau}'
date: 15/12/2023
institution: "Université Grenoble Alpes"
division: 'Mathematics and Natural Sciences'
advisor: 'Eric \textsc{Rutten}'
altadvisor: 'Olivier \textsc{Richard}'
department: 'Mathematics'
degree: 'Bachelor of Arts'
header-includes:
    - \usepackage{setspace}\onehalfspacing

abstract: |
  High-Performance Computing (HPC) systems have become increasingly more complex, and their performance and power consumption make them less predictable.
  This unpredictability requires cautious runtime management to guarantee an acceptable Quality-of-Service to the end users.
  Such a regulation problem arises in the context of the computing grid middleware CiGri that aims at harvesting the idle computing resources of a set of cluster by injection low priority jobs.
  A too aggressive harvesting strategy can lead to the degradation of the performance for all the users of the clusters, while a too shy harvesting will leave resources idle and thus lose computing power.
  There is thus a tradeoff between the amount of resources that can be harvested and the resulting degradation of users jobs, which can evolve at runtime based on Service Level Agreements and the current load of the system.

  We claim that such regulation challenges can be addressed with tools from Autonomic Computing, and in particular when coupled with Control Theory.
  This thesis investigates several regulation problems in the context of CiGri with such tools.
  We will focus on regulating the harvesting based on the load of a shared distributed file-system, and improving the overall usage of the computing resources.
  We will also evaluate and compare the reusability of the proposed control-based solutions in the context of HPC systems.

  The experiments done in this thesis also led us to investigate new tools and techniques to improve the cost and reproducibility of the experiments.
  We will present a tool named NixOS-compose able to generate and deploy reproducible distributed software environments.
  We will also investigate techniques to reduce the number of machines needed to deploy experiments on grid or cluster middlewares, such as CiGri, while ensuring an acceptable level of realism for the final deployed system.

abstract-fr: |
    Les systèmes de calcul haute performance (HPC) sont devenus de plus en plus complexes, et leurs performances ainsi que leur consommation d'énergie les rendent de moins en moins prévisibles.
    Cette imprévisibilité nécessite une gestion en ligne et prudente, afin garantir une qualité de service acceptable aux utilisateurs.
    Un tel problème de régulation se pose dans le contexte de l'intergiciel de grille de calcul CiGri qui vise à récolter les ressources inutilisées d'un ensemble de grappes via l'injection de tâches faiblement prioritaires.
    Une stratégie de récolte trop agressive peut conduire à la dégradation des performances pour tous les utilisateurs des grappes, tandis qu'une récolte trop timide laissera des ressources inutilisées et donc une perte de puissance de calcul.
    Il existe ainsi un compromis entre la quantité de ressources pouvant être récoltées et la dégradation des performances pour les tâches des utilisateurs qui en résulte.
    Ce compromis peut évoluer au cours de l'exécution en fonction des accords de niveau de service et de la charge du système.

    Nous affirmons que de tels défis de régulation peuvent être résolus avec des outils issus de l'informatique autonomique, et en particulier lorsqu'ils sont couplés à la théorie du contrôle.
    Cette thèse étudie plusieurs problèmes de régulation dans le contexte de CiGri avec de tels outils.
    Nous nous concentrerons sur la régulation de la récolte de ressources libres en fonction de la charge d'un système de fichiers distribué partagé et sur l'amélioration de l'utilisation globale des ressources de calcul.
    Nous évaluerons et comparerons également la réutilisabilité des solutions proposées dans le contexte des systèmes HPC.

    Les expériences réalisées dans cette thèse nous ont par ailleurs amené à rechercher de nouveaux outils et techniques pour améliorer le coût et la reproductibilité des expériences.
    Nous présenterons un outil nommé NixOS-compose capable de générer et de déployer des environnements logiciels distribués reproductibles.
    Nous étudierons de plus des techniques permettant de réduire le nombre de machines nécessaires pour expérimenter sur des intergiciels de grappe, tels que CiGri, tout en garantissant un niveau de réalisme acceptable pour le système final déployé.

resume-fr: |
    Ce document commence par une introduction présentant le contexte général de cette thèse.
    La première partie du manuscrit se concentre sur la récolte autonomique de ressources HPC, et est composée de 6 chapitres.

    Dans le Chapitre 1, nous présentons l'état de l'art dans les différentes facettes de notre problème (la collecte de ressources de calculs, la gestion autonomique de systèmes informatiques, et la théorie du contrôle appliquée aux systèmes informatiques).
    Nous présentons aussi CiGri, qui est l'objet central de cette thèse.
    Ce premier chapitre se conclut avec la présentation des questions de recherche qui seront abordées dans les chapitres qui suivent.

    Le Chapitre 2 présente l'analyse d'une étude des caractéristiques statistiques des taches exécutée par CiGri.
    Nous utilisons les résultats de ce chapitre pour pouvoir ensuite modéliser des taches synthétiques de manière réaliste.

    Le Chapitre 3 introduit un contrôleur Proportionnel-Intégral pour réguler la charge du système de fichiers distribue d'un cluster de calcul.
    Nous présentons les signaux considérés (actionneur, capteur) et suivons de manière pédagogique la méthodologie du design d'un contrôleur de la théorie du contrôle.
    Le chapitre contient une analyse expérimentale du contrôleur sur le système réel.

    Une des limitations du contrôleur proposé dans le chapitre précédent est son lien fort avec le système pour lequel il a été conçu, ce qui limite sa réutilisabilité sur d'autres systèmes.
    Dans le Chapitre 4, nous considérons 2 autres contrôleurs (3 au total) et évaluons leur performance et utilisabilité.
    Nous concluons qu'il n'y a pas de globalement meilleur contrôleur, et exhibons des situations dans lesquelles certains contrôleurs sont plus adaptés.

    Dans les chapitres 3 et 4, la problématique principale est la régulation de la charge du système de fichiers distribués.
    Le Chapitre 5 se concentre sur la réduction du temps de calcul perdu, et cela, en considérant les ressources de calculs inutilisées, mais aussi le temps de calculs perdu par une tache de faible priorité s'étant fait arrêter par l'ordonnanceur.
    Nous avons modifié légèrement l'ordonnanceur afin d'implémenter un nouveau capteur nous permettant d'avoir une estimation de l'usage de la plateforme dans le futur.
    Nous présentons un contrôleur Proportionnel-Intégral utilisant ce nouveau capteur.
    Le chapitre présente une évaluation expérimentale montrant qu'il est possible de réduire le temps de calcul perdu total, mais également la consommation énergétique du cluster.

    Le Chapitre 6 conclu la première partie de ce manuscrit et donne des perspectives.

    La seconde partie de cette thèse se concentre sur les couts expérimentaux et la reproductibilité dans le contexte des expériences en informatique distribuée.
    Cette partie commence par le Chapitre 7 qui donne le contexte général et présente l'état de l'art.

    Le Chapitre 8 présente NixOS-compose, un nouvel outil capable de générer et déployer des environnements logiciels distribués complexes.
    Ce chapitre contient également une présentation technique de l'outil, ainsi qu'une comparaison expérimentale aux outils de l'état de l'art.

    Dans le chapitre 9 nous présentons une technique pour réduire le nombre de machines à déployer pour représenter un cluster.
    Nous nous concentrons sur l'impact sur les performances en I/O (Entrée/Sortie) de cette technique pour juger de ses limites.
    Nous évaluons cette technique avec deux systèmes de fichiers distribués de natures différentes et avec des configurations de cluster différentes.

    Le Chapitre 10 présente les premiers pas vers un simulateur pour les boucles de rétroactions dans CiGri présentées dans la première partie de ce manuscrit.
    Il contient une description technique de l'implémentation du simulateur, et d'une comparaison expérimentale contre un système réel.
    Nous concluons ce chapitre avec les limites de simulateurs de l'état de l'art pour l'introduction de boucle de rétroaction complexes, et pour la simulation de systèmes de fichiers distribués.

    Les Chapitres 11 et 12 concluent respectivement la seconde partie du manuscrit et le manuscrit.
    Ils présentent des perspectives et des questions de recherche soulevées par les travaux de cette thèse.


acknowledgements: |
    J'aimerais commencer par remercier les membres du jury pour leurs retours, et en particulier Alessandro Papadopoulos et Alexandru Costan pour avoir lu ce document de bout en bout.

    Un immense merci à Eric et Olivier pour m'avoir accueilli dès le stage de M2, et de m'avoir supporté pendant presque 4 ans.
    Deux encadrants, deux styles et domaines d'expertise bien différents, mais tout de même très complémentaires !
    Vous m'avez tous les deux tant appris, scientifiquement et humainement.
    Merci aux équipes CtrlA, DataMove, Polaris pour cet environnement de recherche si sain et stimulant.
    Merci à Imma, Annie, Maud, sans qui je ne serais pas allé bien loin (géographiquement et administrativement).
    Merci aux post-docs, ingénieurs, et jeunes chercheuses et chercheurs pour vos connaissances et pour avoir montré la voie à suivre avec autant de bienveillance.
    Je pense notamment à Raphaël, Sophie, Millian, Danilo, Adrien, Jonathan.
    Merci à tous les thésard.e.s avec qui j'ai partagé ces trois années et tous ces moments de convivialité autour d'un verre et/ou avec des cartes.
    Merci à tous les résidents du bureau 431, pour leur sympathie, et avoir supporté toutes mes tentatives au basket.
    Merci aux brésiliens pour m'avoir fait découvrir vos coutumes culinaires (parfois douteuses).
    Merci à Grid'5000 sans qui cette thèse aurait été bien fade expérimentalement parlant.
    Merci à tous les enseignants qui m'ont laissé une marque pendant mon parcours scolaire.
    Merci à Frédéric et Thomas pour m'avoir fait découvrir la beauté du parallélisme et du distribué, et Arnaud et Jean-Marc pour m'avoir sensibilisé aux subtilités de l'évaluation expérimentale et des statistiques.

    Merci à ma famille pour tout leur soutien (Luky, c'est à ton tour maintenant !).

    Et enfin, merci infiniment à Sofi pour son support quotidien inconditionnel, et sans qui ces derniers mois de stress auraient été bien plus difficiles à vivre.    

dedication: |
  You can have a dedication here if you wish. 
preface: |
  This is an example of a thesis setup to use the reed thesis document class 
  (for LaTeX) and the R bookdown package, in general.
  
# Specify the location of the bibliography below
bibliography: references.bib
lot: true
lof: true
top-level-division: chapter
---

