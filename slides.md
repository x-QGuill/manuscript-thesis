---
title: 'Control-based runtime management of HPC systems with support for reproducible experiments'
short_title: 'PhD Thesis Defense'
subtitle: 'PhD Thesis Defense'
date: 11/12/2023
affiliations:
    - name: UGA, INRIA
      mark: ''
      signature: 'Univ. Grenoble Alpes, INRIA, CNRS, LIG'
      email: 'Quentin.Guilloteau@univ-grenoble-alpes.fr'
      authors:
        - firstname: Quentin
          lastname: Guilloteau
          team: "Ctrl-A and DataMove teams"
          is_presenter: true
header-includes:
    - \usepackage{tikz}
    - \usepackage{subcaption}
    - \usepackage{MnSymbol,wasysym}
    - \usepackage{transparent}
    - \newcommand{\semitransp}[2][35]{\textcolor{fg!#1}{#2}}
    - \usepackage{amssymb}
bibliography: references.bib
lang: en
---

## High Performance Computing (HPC)

\begin{columns}
   \begin{column}{0.6\textwidth}
       \begin{figure}
           \centering
           \includegraphics[width=\textwidth]{figs/eiffel.jpg}
       \end{figure}
   \end{column} 
   \hfill
   \begin{column}{0.4\textwidth}
        \begin{figure}
            \centering
            \begin{tikzpicture}
                \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=0.7\textwidth]{figs/X7200_laptop_computer.JPG}};
                \node[red, scale=2.9] at (2, 1.5) {\frownie{}};
            \end{tikzpicture}
            \begin{tikzpicture}
                \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=0.8\textwidth]{figs/gricad.jpg}};
                \node[green, scale=2.9] at (2, 1) {\smiley{}};
            \end{tikzpicture}
        \end{figure}
   \end{column}
\end{columns}

\begin{center}
{\Large Computations too demanding $\rightsquigarrow$ need \textbf{several powerful} machines \\

   $\hookrightarrow$ expensive $\rightsquigarrow$ shared $\rightsquigarrow$ \alert{reservation process}}
\end{center}

## Resouces and Job Management System

\begin{columns}
    \begin{column}{0.4\textwidth}
        \begin{block}{HPC Jobs}
            \begin{itemize}
                \item Some computations
                \item Static resource allocation
                \item Static time allocation
            \end{itemize}
        \end{block}
        \begin{block}{HPC Cluster}
            \begin{itemize}
                \item Computing nodes
                \item Interconnected
                \item High speed I/O
            \end{itemize}
        \end{block}
    \end{column}
    \begin{column}{0.6\textwidth}
        \only<1>{
            \begin{figure}
                \centering
                \includegraphics[width=\textwidth]{./figs/rjms.pdf}
                \textbf{R}esources and \textbf{J}obs \textbf{M}anagement \textbf{S}ystem\ \cite{bleuse:tel-01722991}
            \end{figure}
        }
        \only<2->{
            \begin{figure}
                \centering
                   \includegraphics[width = \textwidth]{./figs/slides/gantt_small.png}
                   Gantt Chart
            \end{figure}
        }
    \end{column}
\end{columns}
\onslide<3->{
\begin{tikzpicture}[remember picture,overlay]
    \fill[black,opacity=0.5] (current page.south west) rectangle (current page.north east);
    \node[font=\Huge,fill=white,draw=orange] at (current page.center) {$\hookrightarrow$ \alert{How to harvest?}};
\end{tikzpicture}
}
\onslide<2->{
\begin{center}
{\Large \alert<-2>{Idle Resources = Wasted Computing Power} \textcolor<2>{fg!35}{and Money}}
\end{center}
}

<!---
## Context

\begin{columns}
   \begin{column}{0.4\textwidth}
       \begin{figure}
           \centering
           \includegraphics[width=0.8\textwidth]{figs/slides/tarn.hr18.png}
       \end{figure}
   \end{column} 
   \hfill
   \begin{column}{0.5\textwidth}
        \begin{figure}
            \centering
            \begin{tikzpicture}
                \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=0.45\textwidth]{figs/slides/X7200_laptop_computer.JPG}};
                \node[red, scale=2.9] at (2, 1.5) {\frownie{}};
            \end{tikzpicture}
            \begin{tikzpicture}
                \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=0.6\textwidth]{figs/slides/gricad.jpg}};
                \node[green, scale=2.9] at (2, 1) {\smiley{}};
            \end{tikzpicture}
        \end{figure}
   \end{column}
\end{columns}


\begin{block}{}
\begin{center}
   "Laptops" \textbf{too small} $\rightsquigarrow$ need \textbf{several powerful} machines \\

   $\hookrightarrow$ expensive $\rightsquigarrow$ shared $\rightsquigarrow$ \textbf{reservation process}
\end{center}
\end{block}


## Computing Clusters


\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{./figs/top500.pdf}
    \caption{TOP500\cite{cornebize2021high}}
\end{figure}

\begin{center}
Moore's Law: $\nearrow$ performance $\rightsquigarrow$ $\nearrow$ more parallelism
\end{center}

## Several users

\begin{center}
Expensive $\rightsquigarrow$ shared among institutions $\rightsquigarrow$ \textbf{reservation process}
\end{center}

\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{./figs/rjms.pdf}
   \caption{Illustration of a Resource and Job Management System (RJMS) \cite{bleuse:tel-01722991}}
   \label{fig:rjms}
\end{figure}

\begin{center}
$\hookrightarrow$ How many resources? For how long? How to map \emph{jobs} to machines?
\end{center}


## Scheduling and Gantt Chart

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{./figs/slides/gantt_small.png}
\end{figure}

\begin{center}
    Idle HPC Resources $\implies$ Lost Computing Power $\rightsquigarrow$ \textbf{How to Harvest?}
\end{center}
--->

## Harvesting Idle Resources

\alert{Main idea}: Use smaller, killable jobs (e.g., Big Data\ \cite{mercier2017big}, FaaS\ \cite{przybylski2022using})

\begin{columns}
    \begin{column}{0.5\textwidth}
		\begin{block}{\textit{CiGri} \cite{cigri}}
			\begin{itemize}
                \item Grid middleware used at Gricad
				\item \textbf{Bag-of-tasks}: many, multi-parametric
				\item \textbf{Best-effort Jobs}: Lowest priority
                \item \textbf{Objectives}:
                    \begin{itemize}
                        \item Collect grid idle resources
                        \item Reduce pressure on RJMS
                    \end{itemize}
                \item Submits like a \textit{periodic tap}
                    \begin{itemize}
                        \item submits jobs then,
                        \item waits for \emph{all} jobs to terminate
                        \item[$\hookrightarrow$] \alert{suboptimal}!
                    \end{itemize}
			\end{itemize}
		\end{block}
    \end{column}
    \begin{column}{0.35\textwidth}
		\begin{figure}
			\centering
\includegraphics[width = \textwidth]{./figs/cigri_oar.pdf}
		\end{figure}
    \end{column}
\end{columns}

## CiGri jobs \cite{guilloteau:cigri_jobs}

\begin{columns}
    \begin{column}{0.5\textwidth}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{./figs/jobs_cigri/global/ecdf_comparison.pdf}
10 years, 44 Millions jobs
\end{figure}
    \end{column}
    \begin{column}{0.5\textwidth}
\begin{block}{Example: BigGNSS\ \cite{deprez2018toward}}
\begin{itemize}
    \item A lot of satellites $\implies$ a lot of data
    \item Several stations $\rightsquigarrow$ \textbf{Campaigns}
    \item Subdivision of the processing $\rightsquigarrow$ \textbf{Jobs}
    \item Unique binary + different inputs
\end{itemize}
\begin{figure}
\centering
\includegraphics[width=0.55\textwidth]{./figs/slides/biggnss.pdf}
\end{figure}
\end{block}
    \end{column}
\end{columns}

## Problem formulation

\begin{alertblock}{{\Large Problem}}
{\Large
\begin{center}
    $\nearrow$ Harvesting $\implies$ $\nearrow$ Performance Degradation $\rightsquigarrow$ \textbf{Trade-off} \\
\end{center}
\begin{center}
    $\hookrightarrow$ Unpredictability $\implies$ \textbf{runtime management}
\end{center}
}
\end{alertblock}

\vspace{0.3cm}
\onslide<2->{
\begin{block}{{\Large In this PhD thesis}}
\vspace{0.4cm}
{\Large
\begin{enumerate}
\item<2-> How to \alert{submit} CiGri jobs to harvest idle resources with \alert{controlled} degradation for priority users?
\item<3-> How to improve the \alert{cost} and \alert{reproducibility} of experiments on grid/cluster systems?
\end{enumerate}
}
\end{block}
}

# Harvesting idle resources

## Runtime Management: Autonomic Computing (AC) \onslide<2->{and Control Theory}

\begin{columns}
\begin{column}{0.45\textwidth}
\onslide<1->{
\begin{block}{AC and the MAPE-K Loop\ \cite{kephart2003vision}}
\begin{itemize}
\item \alert{Auto-regulation} given \alert{high-level objectives}\\
\item implementations: rules, AI, etc.
\end{itemize}
\end{block}
}

\onslide<2->{
\begin{block}{Control Theory}
\begin{itemize}
\item Regulate dynamical systems
\item physical systems
\item mathematically proven properties
\item performance, robustness, \alert{explainability}
\end{itemize}
\end{block}
}
\end{column}

\begin{column}{0.55\textwidth}
\onslide<1->{
\begin{figure}
\centering
\includegraphics[height=0.4\textheight]{./figs/mapek.jpg}
\end{figure}
}
\onslide<2->{
\begin{figure}
    \centering
    \scalebox{0.5}{
    \begin{tikzpicture}[scale=0.8]
        \draw [-latex] (-2, 5) -- (-0.4, 5) node[pos=.2, above] {Reference};
        \draw [-latex] (0.4, 5) -- (2, 5) node[pos=.5, above] {Error};
        \draw (2, 6) rectangle (6, 4) node[pos=.5] {Controller};
        \draw [-latex] (6, 5) -- (8, 5) node[pos=.5, below] {Input};

        \draw (8, 6) rectangle (12, 4) node[pos=.5] {System};
        \draw [-latex] (10, 7) -- (10, 6) node[pos=-.1, above] {Disturbances};

        \draw [-latex] (12, 5) -- (16, 5) node[pos=.5, above] {Output};
        \draw (5, 2) rectangle (9, 0) node[pos=.5] {Sensor};
        \draw [-latex] (14, 5) -- (14, 1) -- (9, 1);
        \draw [-latex] (5, 1) -- node[pos=.5, above] {Measure} (0, 1) -- (0, 4.6);
        \draw (0, 5) circle (.4);
        \draw [-] (0.28, 5.28) -- (-0.28, 4.72);
        \draw [-] (-0.28, 5.28) -- (0.28, 4.72);
        \draw (.5, 4.5) node {-};
        \draw (-.3, 5.7) node {+};
    \end{tikzpicture}
}
\end{figure}
}
\end{column}
\end{columns}

## 1. Identify the goals

\begin{columns}
\begin{column}{0.46\textwidth}
\begin{alertblock}{Problem formulation}
\begin{itemize}
\item Use Control Theory to....
\item ...harvest idle resources...
\item ...in a \alert{non-intrusive} way
\item max cluster utilization
\item min degradation of performance
\end{itemize}
\end{alertblock}
\vspace{0.7cm}
{$\hookrightarrow$ Focus on I/O degradation}
\end{column}
\begin{column}{0.54\textwidth}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{./figs/filieri.pdf}
Steps to design a controller\ \cite{filieri2015software}
\end{figure}
\end{column}
\end{columns}

## 2. Identify the knobs
<!---
Sensors \& Actuators
--->

\begin{columns}
\begin{column}{0.6\textwidth}
\begin{block}{Actuators ({\color{blue}\textbf{u}})}
\vspace{0.1cm}
Number of jobs submitted by CiGri
\end{block}

\begin{block}{Sensors ({\color{red}\textbf{y}})}
\begin{itemize}
\item File-System (NFS):
\begin{itemize}
\item \alert{indirect} measure of overhead
\item \texttt{/proc/loadavg} \cite{ferrari1987empirical}
\begin{itemize}
\item $\simeq$ number of processes running
\item well known by system administrators
\item Exponential Smoothing $\rightsquigarrow$ Inertia\\$\hookrightarrow$ Nice for the control
\end{itemize}
\item<2-> \alert{know limits of sensor}
\end{itemize}
\item<3-> \semitransp{Cluster: OAR API (nb running, waiting jobs)}
\end{itemize}
\end{block}

\end{column}
\begin{column}{0.4\textwidth}
\begin{figure}
\centering
\begin{tikzpicture}
    \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[height = 0.9\textheight]{./figs/pi/identification_limit_nfs.pdf}};
    \onslide<2->{
        \draw[red,ultra thick,rounded corners] (4.94, 0.5) rectangle (5.4, 7);
        \node[red, rotate=45] at (2.75, 3.85) {\Huge \textbf{Overload!}};
    }
\end{tikzpicture}
\end{figure}
\end{column}
\end{columns}

## Feedback loop in CiGri

```{=latex}
\begin{tikzpicture}[remember picture, overlay]
\node (0) at (current page.north east) {};
% \node[below left=of 0]{%
\node[] at (12.6, -0.4) {%
\includegraphics[width=0.25\textwidth]{./figs/slides/feedback.png}
};
\end{tikzpicture}
```
\vspace{-0.5cm}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{./figs/pi/cigri-hor.pdf}
\end{figure}

\begin{center}
\textbf{Reference value} $\rightsquigarrow$ acceptable \texttt{load} on the File-System $\simeq$ chosen by system administrators
\end{center}

## 3. Devise the model

\begin{block}{First, \textbf{a Model ...} (i.e., how does the system behave without Control)}

\begin{center}
\scalebox{1}{
    $\displaystyle {\color{red}\textbf{y}}(k + 1) = \sum_{i = 0}^k a_i \times {\color{red}\textbf{y}}(k - i) + \sum_{j = 0}^k b_j \times {\color{blue}\textbf{u}}(k - j)$
}
\end{center}
\end{block}

\begin{block}{... then \textbf{a (P\onslide<2->{I}\onslide<3->{\textcolor<4->{fg!35}{D}}) Controller} (i.e., the Closed-Loop behavior)}
\begin{center}
\scalebox{1}{
$\displaystyle {\color{blue}\textbf{u}}(k) = \textbf{K}_p \times Error(k) \onslide<2->{+ \textbf{K}_i \times \sum_i^k Error(i)}\onslide<3->{\textcolor<4->{fg!35}{~+~\textbf{K}_d \times \left(Error(k) - Error(k-1)\right)}}$
}
\end{center}
\end{block}
\vspace{-0.5cm}
\begin{columns}
\begin{column}{0.47\textwidth}
\begin{alertblock}{Sensors \& Actuators}
    \begin{itemize}
        \item Actuator: \#jobs to sub $\rightsquigarrow {\color{blue}\textbf{u}}$
        \item Sensor: FS Load $\rightsquigarrow {\color{red}\textbf{y}}$
        \item $Error(k) = Reference - Sensor(k)$
    \end{itemize}
\end{alertblock}
\end{column}
\begin{column}{0.53\textwidth}

\begin{block}{Methodology}
    \begin{enumerate}
\item Open-Loop experiments (fixed ${\color{blue}\textbf{u}}$)
\item Model parameters ($a_i, b_j$)
\item Choice controller behavior ($\textbf{K}_{*}$)
    \end{enumerate}
\end{block}
\end{column}
\end{columns}

## 3. Devise the model - Open-Loop Experiments

\begin{columns}
\begin{column}{0.25\textwidth}
\begin{itemize}
\item "step" inputs
\item $\neq$ I/O loads ($f$)
\item observe behavior
\item \alert{linear} model
\end{itemize}
\vspace{0.5cm}
\begin{equation*}
{\color{red}y_{ss}} = \alpha + \beta_1 f + \beta_2 {\color{blue}u} + \gamma f {\color{blue}u}
\end{equation*}
\end{column}
\begin{column}{0.75\textwidth}
\begin{figure}
   \centering
   \includegraphics[width = \textwidth]{./figs/pi/plot_pi_ident.pdf}
\end{figure}
\end{column}
\end{columns}

## 3. Devise the model - First order model

\alert{First order model}: ${\color{red}\textbf{y}}(k + 1) = a \times {\color{red}\textbf{y}}(k) + b \times {\color{blue}\textbf{u}}(k)$ $\rightsquigarrow$ $\textcolor<2->{fg!35}{a}, \textbf<2->{b}$ = ? \onslide<2->{\textcolor{fg!35}{($a$ from def of \texttt{loadavg})}}

\onslide<3->{
\begin{block}{In steady state (ss)}
\begin{align*}
{\color{red}\textbf{y}_{ss}} = a \times {\color{red}\textbf{y}_{ss}} + b \times {\color{blue}\textbf{u}_{ss}}\onslide<4->{ \implies b = \frac{{\color{red}\textbf{y}_{ss}} \times (1 - a)}{{\color{blue}\textbf{u}_{ss}}}} \onslide<5->{&\implies \frac{(\alpha + \beta_1 f + \beta_2 {\color{blue}\textbf{u}_{ss}} + \gamma f {\color{blue}\textbf{u}_{ss}}) \times (1 - a)}{{\color{blue}\textbf{u}_{ss}}}}\\
\onslide<6->{&\implies b \simeq (\beta_2 + \gamma f) \times (1 - a)}
\end{align*}
\end{block}
}

\onslide<7->{
\begin{block}{Where are we?}
\begin{center}
$
\begin{array}{c}
\text{Open-Loop} \\
\text{Experiments}
\end{array}
\onslide<7->{{\color{mycolor}{\checkmark}}}
\onslide<8->{
\rightsquigarrow
\begin{array}{c}
\text{Model (1st order)} \\
{\color{red}\textbf{y}}(k + 1) = a \times {\color{red}\textbf{y}}(k) + b \times {\color{blue}\textbf{u}}(k)
\end{array}
}
\onslide<8->{{\color{mycolor}{\checkmark}}}
\onslide<9->{
\rightsquigarrow
\begin{array}{c}
\text{Controller Gains} \\
\textbf{K}_p, \textbf{K}_i, \semitransp{\textbf{K}_d}
\end{array}}$
\end{center}
\end{block}
}

## 4. Design the controller

\begin{columns}
\begin{column}{0.46\textwidth}
\begin{block}{Controller Gains are ...}
functions of the model and
\begin{itemize}
\item $k_s$: maximum \textbf{time} to steady state
\item $M_p$: maximum \textbf{overshoot} allowed
\end{itemize}
\end{block}
\onslide<2->{
\begin{center}
{\Large \alert{Can choose the behavior!}}
\end{center}
}
\onslide<3->{
\begin{block}{Non-Intrusive Harvesting}
\begin{itemize}
\item no overshoot
\item but "fast" response
\end{itemize}
\end{block}
}
\end{column}
\begin{column}{0.54\textwidth}
\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{./figs/pi/closed_loop_behaviour.pdf}
\end{figure}
\end{column}

\end{columns}

## 5./6. Implement and validate the controller - Evaluation with synthetic jobs

\vspace{-0.4cm}
\begin{columns}
\begin{column}{0.75\textwidth}
\begin{figure}
\centering
\begin{tikzpicture}
    \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=\textwidth]{./figs/pi/step_pi_single.pdf}};
    \onslide<2->{
        \draw[orange, ultra thick] (4.5 ,6) circle (0.55);
        \draw[-latex, orange, ultra thick] (11, 3) -- (5, 5.5);
    }
\end{tikzpicture}
\end{figure}
\end{column}
\begin{column}{0.25\textwidth}
\textbullet\ constant reference

\textbullet\ synthetic jobs

\textbullet\ step disturbance

\begin{center}
\alert{Manage to control the load of the File-System}
\end{center}

\onslide<2->{
\begin{center}
takes time to react\\ $\hookrightarrow$ might cause \textbf{overload}
\end{center}
}



\end{column}
\end{columns}

## Trade-off: Idleness versus Performance degradation (I/O Overhead)

\begin{columns}
\begin{column}{0.7\textwidth}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{./figs/pi/mb2_2.pdf}
\end{figure}
\end{column}
\begin{column}{0.3\textwidth}
\textbullet\ MADBench2\ \cite{mb2}

\textbullet\ various reference values

\textbullet\ compute idle resources

\textbullet\ compute I/O overhead

\begin{center}
\alert{Trade-off between Harvesting \& I/O overhead through the reference value}
\end{center}

\end{column}
\end{columns}

<!---
## How to choose the reference value?

\begin{itemize}
\item<1-> Normalized \texttt{loadavg} then fix to 75\%, 90\%, 95\%, etc.
\item<2-> How much \alert{burst} to sustain?
\begin{itemize}
\item<3-> dynamic reference value 
\item<3-> based on number of \textbf{priority jobs} and \textbf{historical I/O data} (e.g., Darshan\ \cite{darshan1})
\end{itemize}
% \item<3-> How much \alert{burst} to sustain? \onslide<4->{dyn. ref: $y_{ref,k} = y_{max} - \max(0, y_{max} - f_{model}(d_k, \bar{f}))$}
\end{itemize}

\onslide<3->{
\begin{columns}
\begin{column}{0.5\textwidth}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{./figs/pi/dirac.png}
\end{figure}
\end{column}
\begin{column}{0.5\textwidth}
\begin{figure}
  \centering
  \scalebox{0.55}{
  \begin{tikzpicture}[scale=0.6]
    \draw [->] (-5, 5) -- (-2.9, 5) node[pos=.2, above] {$y_{\max}$};
    \draw [->] (-2.1, 5) -- (-0.4, 5) node[pos=.5, above] {$y_{ref,k}$};
    \draw [->] (0.4, 5) -- (2, 5) node[pos=.5, above] {$e_k$};
    \draw (2, 6) rectangle (6, 4) node[pos=.5] {Controller};
    \draw [->] (6, 5) -- (8, 5) node[pos=.5, above] {$u_{k}$};
    \draw (8, 6) rectangle (12, 4) node[pos=.5] {System};
    \draw [->] (12, 5) -- (16, 5);
    \draw [->] (14, 5) -- (14, 3) -- node[pos=0.5, above]{$y_k$} (0, 3) -- (0, 4.6);

    \draw (0, 5) circle (.4);
    \draw [-] (0.28, 5.28) -- (-0.28, 4.72);
    \draw [-] (-0.28, 5.28) -- (0.28, 4.72);
    \draw (.5, 4.5) node {-};
    \draw (-.3, 5.5) node {+};

    \draw (-2.5, 5) circle (.4);
    \draw [-] (-2.22, 5.28) -- (-2.78, 4.72);
    \draw [-] (-2.78, 5.28) -- (-2.28, 4.72);
    \draw (-3.2, 5.3) node {+};
    \draw (-2.8, 5.5) node {-};

    \draw [->] (10, 8.5) -- node[above,pos=0.01, text width=2.5cm] {Priority jobs (Disturbance)} (10, 6);
    \draw [->] (10, 7) -- node[pos=0.5, above]{$d_k$}(4, 7);
    \draw [->] (1, 7) -- node[pos=0.5, above]{$y_{prio,k}$}(-2.5, 7) -- (-2.5, 5.4);
    \draw [->] (2.5, 8.5) -- node[above,pos=0.01] {$\bar{f}$} (2.5, 7.5);
    \draw (1, 6.5) rectangle (4, 7.5) node[pos=.5] {$f_{model}$};
    \end{tikzpicture}
    }
\end{figure}
\end{column}
\end{columns}
}
--->


<!---
## Beyond idle resources

\begin{center}
\Large Wasted computing power: Idle resources, but also \pause \alert{killed jobs}!
\end{center}


\begin{columns}
\begin{column}{0.65\textwidth}

\begin{figure}
  \centering
  \scalebox{0.7}{
  \begin{tikzpicture}[scale=0.6]
\onslide<2->{
    \draw [->] (0.4, 5) -- (2, 5) node[pos=.5, above] {$e_k$};
    \draw (2, 6) rectangle (6, 4) node[pos=.5] {Controller};
    \draw [->] (6, 5) -- (8, 5) node[pos=.5, above] {$u_{k}$};
    \draw (8, 6) rectangle (12, 4) node[pos=.5] {System};
    \draw [->] (12, 5) -- (16, 5);
    \draw [->] (14, 5) -- (14, 3) -- node[pos=0.5, above]{$running_k + waiting_k$} (0, 3) -- (0, 4.6);

    \draw (0, 5) circle (.4);
    \draw [-] (0.28, 5.28) -- (-0.28, 4.72);
    \draw [-] (-0.28, 5.28) -- (0.28, 4.72);
    \draw (.5, 4.5) node {-};
    \draw (-.3, 5.5) node {+};


    \draw [->] (10, 8) -- node[above,pos=0.01, text width=2.5cm] {Priority jobs (Disturbance)} (10, 6);
}

    \onslide<2-3>{
        \draw [->] (-5, 5) -- (-0.4, 5) node[pos=.2, above] {$r_{\max}$};
    }
    \onslide<4->{
        \draw [->] (-5, 5) -- (-2.9, 5) node[pos=.2, above] {$r_{\max}$};
        \draw [->] (-2.1, 5) -- (-0.4, 5) node[pos=.5, above] {$y_{ref,k}$};
        \draw (-2.5, 5) circle (.4);
        \draw [-] (-2.22, 5.28) -- (-2.78, 4.72);
        \draw [-] (-2.78, 5.28) -- (-2.28, 4.72);
        \draw (-3.2, 5.3) node {+};
        \draw (-2.8, 5.5) node {-};

        \draw [->, orange, line width=0.5mm] (10, 7) -- node[pos=0.5, above]{$d^h_k$}(-2.5, 7) -- (-2.5, 5.4);
    }

    \end{tikzpicture}
}
\end{figure}
\end{column}
\begin{column}{0.35\textwidth}
\begin{itemize}
\item<3-> \alert{anticipate} variations in available resources
\item<4-> new sensor \semitransp{(modify OAR)}
\item<4-> \textbf{provisional} Gantt chart
\item<5-> \alert{horizon}
\end{itemize}
\end{column}
\end{columns}
\vspace{0.7cm}
\onslide<6->{
\begin{center}
\Large Can reduce \alert{both idle and killed} time, and energy usage!
\end{center}
}
--->

<!---
## Beyond Idle Resources - Results

\begin{columns}
\begin{column}{0.42\textwidth}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{./figs/feedforward/compa_energy_pi.pdf}
\end{figure}
\end{column}
\begin{column}{0.58\textwidth}
\begin{itemize}
\item PI Controller
\item various horizons
\item various job durations (pj)
\item but single CiGri frequency (30s)
\item Energy: Active $\simeq 2\times$ Idle
\end{itemize}
\pause
\begin{center}
\large Can reduce \alert{both idle and killed} time
\end{center}
\end{column}
\end{columns}
--->

## A note on controllers' reusability

\begin{columns}
\begin{column}{0.6\textwidth}
\begin{itemize}
\item Controllers \alert{linked} to the identified system
\item what if new cluster? new configuration?
\item Grid/Cluster administrators\\ $\hookrightarrow$ \textbf{not} control theory experts!
\item compared 3 controllers (w.r.t. portability, guarantees, competence required)
\item example: Portability vs. Performance
\end{itemize}
\begin{center}
{\Large $\hookrightarrow$ \alert{gave recommendations} for system administrators}
\end{center}
\end{column}
\begin{column}{0.4\textwidth}
\begin{figure}
\centering
\onslide<2->{
\begin{tikzpicture}
    \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width=.9\textwidth]{./figs/tuto_ctrl.png}};
    \node[red, rotate=45] at (2.25, 2.85) {\LARGE \textbf{And a tutorial!}};
\end{tikzpicture}
}
\end{figure}
\end{column}
\end{columns}


## Control-based harvesting of idle resources: Wrapping up

\begin{block}{Objectives}
\begin{itemize}
\item Control CiGri submissions based on File-System load \alert{$\checkmark$}
\item \semitransp{Control CiGri submissions to reduce idle/killed wasted time} \alert{$\checkmark$}
\item Can merge controllers! \semitransp{(with some subtelties)}
\item Guidelines for system administrators \alert{$\checkmark$}
\item Tutorial to introduce control theory to computer scientists \alert{$\checkmark$}
\end{itemize}
\end{block}

\begin{block}{Limitations and Perspectives}
\begin{itemize}
\item Tested with \emph{synthetic} jobs $\rightsquigarrow$ real trace
\item Need more info about CiGri jobs' I/O patterns
\item Submissions to several clusters
\item Sensor for Parallel File-System (PFS) ?
\end{itemize}
\end{block}

\onslide<2>{
\begin{tikzpicture}[remember picture,overlay]
    \fill[black,opacity=0.5] (current page.south west) rectangle (current page.north east); 
    \node[draw=orange,fill=white] at (current page.center) {\Large \alert{Take Away}: Control Theory \alert{valuable approach} to exploit such trade-offs};
\end{tikzpicture}
}


# Experiment costs and reproducibility

## A grid middleware needs ... a grid!

<!---
\begin{center}
{\LARGE \alert{How many machines} are required to perform \alert{realistic} experiments on a grid/cluster middleware like CiGri?}
\end{center}
--->

\begin{columns}
\begin{column}{0.5\textwidth}
\begin{center}
{\Large \alert{How many machines} required to perform \alert{realistic} experiments on a grid middleware like CiGri?}
\end{center}
\begin{itemize}
\item<2-> \textbf{Simulation}: fast \smiley{}, modeled \frownie{}, poor sensor support \frownie{}, poor PFS support \frownie{}
\item<3-> \textbf{Full scale}: real environment \smiley{}, expensive and difficult \frownie{}
\item<4-> \textbf{Reduced scale}: real environment \smiley{}, cheaper \smiley{}, \alert{realistic ?} \frownie{}
\end{itemize}
\onslide<5->{
\alert{Objective}: Low cost, realist experiments on the real system
}
\end{column}
\begin{column}{0.5\textwidth}
\begin{figure}
\scalebox{0.7}{
\begin{tikzpicture}
\draw[step=2] (0, 0) grid (8, 8);
\draw[step=4, very thick] (0, 0) grid (8, 8);
\draw[very thick,->](0,0) -- (8.5,0);% node[anchor=west] {Realism};
\draw[very thick,->](0,0) -- (0,8.5);% node[above] {Cost (Energy/Development/Deployment) };

\draw (2, 0) node[below] {Modeled system}; 
\draw (6, 0) node[below] {Real system}; 
\draw (4, -0.75) node[below] {\textbf{Realism}};

\draw (0, 6) node[rotate=90,above] {High}; 
\draw (0, 2) node[rotate=90,above] {Low}; 
\draw (-0.75, 4) node[rotate=90,above] {\textbf{Cost (Energy/Development/Deployment)}};

\onslide<3->{
\draw (7, 7) node[text width=2cm, align=center] {\textbf{Full scale}};
}
\onslide<4->{
\draw (5, 5) node[text width=2cm, align=center] {\textbf{Reduced scale}};
}

\onslide<2->{
\draw (2, 2) node[text width=2cm, align=center] {\textbf{Simulation}};
\draw (2, 6) node[text width=2.5cm, align=center] {\semitransp{Cycle-accurate simulation}};
}

\onslide<5->{
\draw (7, 1) node[text width=2cm, align=center, thick] {\alert{Objective}};
}

\end{tikzpicture}
}
\end{figure}

\end{column}
\end{columns}

<!---
## A grid middleware needs ... a grid!

\begin{center}
{\LARGE \alert{How many machines} are required to perform \alert{realistic} experiments on a grid/cluster middleware like CiGri?}
\end{center}

\begin{center}
{\Large
$\hookrightarrow$ 10 machines? $\rightsquigarrow$ too few, 100 machines? $\rightsquigarrow$ \pause \alert{too costly!}
}
\end{center}
\pause
\Large
\begin{columns}
  \begin{column}{0.5\textwidth}
    \begin{itemize}
      \item[$\hookrightarrow$] Simulation:
        \begin{itemize}
          \item[+] needs only one node/core
          \item[+] fast
          \item[+] can represent any cluster
          \item[-] \textbf{modeled system/softwares}
        \end{itemize}
    \end{itemize}
  \end{column}
  \hfill
  \begin{column}{0.5\textwidth}
    \pause
    \begin{itemize}
      \item[$\hookrightarrow$] Reduce number of nodes:
        \begin{itemize}
          \item[+] \textbf{real system/softwares}
          \item[+] less resources deployed
          \item[-] not representative...
          \item[-] real time
        \end{itemize}
    \end{itemize}
  \end{column}
\end{columns}

\pause
\vspace{0.3cm}

\begin{alertblock}{{\Large Intermediate solution}}
\begin{center}
{\Large less resources deployed + real system/softwares}
\end{center}
\end{alertblock}

--->

## Emulating a full scale cluster by folding its deployment \cite{guilloteau:hal-04038000}

\begin{center}
\alert{The idea}: Deploy more "virtual" resources on one physical machine {\small\semitransp{($\simeq$ oversubscribing)}}
\end{center}

\begin{columns}
\begin{column}{0.23\textwidth}
\only<1>{
\begin{figure}
\centering
\includegraphics[width=\textwidth]{figs/folding/schemas/folding_r_1.pdf}
\alert{Scale 1:1}
\end{figure}
}
\only<2>{
\begin{figure}
\centering
\includegraphics[width=\textwidth]{figs/folding/schemas/folding_r_05.pdf}
\alert{Scale 1:2}
\end{figure}
}
\only<3->{
\begin{figure}
\centering
\includegraphics[width=\textwidth]{figs/folding/schemas/folding_r_025.pdf}
\alert{Scale 1:4}
\end{figure}
}
\onslide<6->{
\textbf{Protocole}:

\textbullet\  IOR\ \cite{ior}

\textbullet\  increase folding

\textbullet\  NFS, OrangeFS
}
\end{column}
\begin{column}{0.77\textwidth}
\begin{columns}
  \begin{column}{0.47\textwidth}
\begin{itemize}
  \item<4->[+] less resources deployed
  \item<4->[+] represents full scale system
\end{itemize}
  \end{column}
  \begin{column}{0.53\textwidth}
\begin{itemize}
  \item<4->[+] real system/environment
  \item<5->[-] \textbf{new job model}: \texttt{sleep + \alert{dd}}
\end{itemize}
  \end{column}
\end{columns}
\onslide<6->{
\begin{figure}
  \centering
  \includegraphics[width=0.9\textwidth]{./figs/folding/graphs/orangefs/segmented_read.pdf}
\end{figure}
}
\end{column}
\end{columns}

\onslide<7>{
\begin{tikzpicture}[remember picture,overlay]
    \fill[black,opacity=0.5] (current page.south west) rectangle (current page.north east); 
    \node[draw=orange,fill=white,font=\Huge] at (current page.center) {\Large $\hookrightarrow$ Folding is appropriate until a \alert{breaking point} that we can model};
\end{tikzpicture}
}

<!---
## Folding a deployment \cite{guilloteau:hal-04038000}

\begin{center}
\alert{The idea}: Deploy more "virtual" resources on one physical machine {\small\semitransp{($\simeq$ oversubscribing)}}
\end{center}

\begin{figure}
     \centering
     \pause
     \begin{subfigure}[b]{0.3\textwidth}
         \centering
         \includegraphics[width=0.75\textwidth]{figs/folding/schemas/folding_r_1.pdf}
         \caption{Folding w/ factor 1.}
         \label{fig:r_1}
     \end{subfigure}
     \hfill
     \pause
     \begin{subfigure}[b]{0.3\textwidth}
         \centering
         \includegraphics[width=0.75\textwidth]{figs/folding/schemas/folding_r_05.pdf}
         \caption{Folding w/ factor 2.}
         \label{fig:r_05}
     \end{subfigure}
     \hfill
     \pause
     \begin{subfigure}[b]{0.3\textwidth}
         \centering
         \includegraphics[width=0.75\textwidth]{figs/folding/schemas/folding_r_025.pdf}
         \caption{Folding w/ factor 4.}
         \label{fig:r_025}
     \end{subfigure}
\end{figure}

\vspace{-0.5em}

\pause
\begin{columns}
  \begin{column}{0.45\textwidth}
\begin{itemize}
  \item[+] less resources deployed \smiley{}
    \pause
  \item[+] represents full scale system
    \pause
\end{itemize}
  \end{column}
  \hfill
  \begin{column}{0.55\textwidth}
\begin{itemize}
  \item[+] real system/environment \smiley{}
    \pause
  \item[-] new job model $\rightsquigarrow$ \texttt{sleep + dd}
    \pause
\end{itemize}
  \end{column}
\end{columns}

\vspace{0.5em}

\onslide<9>{
\begin{tikzpicture}[remember picture,overlay]
    \fill[black,opacity=0.5] (current page.south west) rectangle (current page.north east); 
    \node[draw=orange,fill=white,font=\Huge] at (current page.center) {\Large $\hookrightarrow$ How does the folding \alert{impact the performance} of applications?};
\end{tikzpicture}
}


## Impact on performance in I/O

\begin{columns}
\begin{column}{0.25\textwidth}
\begin{block}{Protocole}
\begin{itemize}
\item IOR Benchmark
\item increase folding
\item NFS, OrangeFS
\end{itemize}
\end{block}
\onslide<3->{
\begin{block}{Rule of thumb}
\begin{center}
$f_{break} \simeq 1 + \frac{3 nb_{cpu}}{10} - \frac{nb_{io}}{2}$
\end{center}
\end{block}
}
\end{column}
\begin{column}{0.75\textwidth}
\onslide<2->{
\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{./figs/folding/graphs/orangefs/segmented_read.pdf}
\end{figure}
}
\end{column}
\end{columns}
\onslide<3->{
\begin{center}
{\Large Folding appropriate until a \alert{breaking point}}
\end{center}
}
--->


## Complex Software Environments

\begin{figure}
\centering
\includegraphics[width=\textwidth, height=3.5cm]{./figs/cigri_deps.pdf}
Graph of CiGri's software dependencies
\end{figure}

\pause \Large $\hookrightarrow$ and RJMS, PFS, jobs, etc. $\rightsquigarrow$ very complex to \textbf{manage/modify}

\pause

\begin{center}
{\LARGE How to \alert{develop/deploy easily} complex software environments in a \alert{reproducible} fashion?}
\end{center}

## Generating Distributed Software Environments

\begin{center}
$\hookrightarrow$ \textbf{Difficult}, \textbf{Time-consuming}, \textbf{Script-based} tools, and \alert{Iterative} process
\end{center}

\begin{figure}
  \centering
     \begin{subfigure}[b]{0.45\textwidth}
         \centering
            \begin{tikzpicture}
            \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width = .9\textwidth]{./figs/nxc/xkcd_building.png}};
            \draw[-latex,orange,ultra thick] (5.75, 3) to[bend left] (8, 3);
            \draw[-latex,orange,ultra thick] (8, 2) to[bend left] (5.75, 2);
            \end{tikzpicture}
         {$\simeq \textbf{10/15~mins}$}
     \end{subfigure}
     \hfill
     \begin{subfigure}[b]{0.45\textwidth}
         \centering
         \includegraphics[width=.9\textwidth]{./figs/nxc/xkcd_deploying.png}
         {$\simeq \textbf{5/10~mins}$}
     \end{subfigure}
\end{figure}

\onslide<2->{
\begin{tikzpicture}[remember picture,overlay]
    \fill[black,opacity=0.5] (current page.south west) rectangle (current page.north east);
    \node[draw=orange,fill=white,font=\Huge] at (current page.center) {\Large $\hookrightarrow$ Usual tools \alert{do not encourage good reproducibility} practices};
\end{tikzpicture}
}


## One tool, One platform

\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{./figs/sota.pdf}
\end{figure}

\vspace{0.6cm}

\myblockquote{So essentially, I want to create a debian12-nfs.qcow2 for VMs equivalent to grid5000's debian12-nfs image. One \alert{painful way} to achieve this would be to install every single thing using the package manager and resolving conflicts by hand.}{Grid'5000 User, 2023}


\onslide<2->{
\begin{tikzpicture}[remember picture,overlay]
    \fill[black,opacity=0.5] (current page.south west) rectangle (current page.north east);
    \node[draw=orange,fill=white,font=\Huge] at (current page.center) {\Large $\hookrightarrow$ Be able to \alert{develop} distributed environments \alert{locally} and then export};
\end{tikzpicture}
}

<!---
Script-based approaches $\rightsquigarrow$ "stack of states"
Base images, changing one layer requires to re-exec following commands

tgz-g5k
on g5k we want to deploy system images to control everything
--->

<!---
\pause
\begin{center}
\Large $\hookrightarrow$ Harness Nix(OS) to generate/deploy \alert{reproducible distributed} environments.
\end{center}
--->

<!----
## The Reproducibility Problem

\begin{block}{Different Levels of Reproducibility \cite{feitelson2015repeatability, acm-badges}}
    \begin{enumerate}
        \item<1-> \textbf{Repetition}:~~~~~~~~~~ Run exact same experiment
        \item<2->\textbf{Replication}:~~~~~~~~~ Run experiment with different parameters
        \item<3-> \alert{Variation}\ \cite{mercier2018considering}: Run experiment with different environments
    \end{enumerate}
\end{block}

\onslide<4->{
    \begin{center}
        $\hookrightarrow$ \textbf{Share the experimental environment and how to build/modify it}
    \end{center}
}

\onslide<5->{
\begin{block}{How to share a Software Environment in HPC?}
    \begin{itemize}
        \item<6-> Containers? $\rightsquigarrow$ need \texttt{Dockerfile} to rebuild/modify.

        \item<7-> Modules? $\rightsquigarrow$ cluster dependent. how to modify?

        \item<8-> Spack\ \cite{gamblin_spack_2015}? $\rightsquigarrow$ share through modules...
    \end{itemize}
\end{block}
}
--->

## Functional package managers (in one slide!)


\begin{columns}
  \begin{column}{0.58\textwidth}
\begin{itemize}
\item Nix \includegraphics[width=0.04\textwidth]{figs/slides/nix_logo.png}, Guix \includegraphics[width=0.04\textwidth]{figs/slides/guix_logo.pdf} \alert{reproducible by design!}
\item packages = functions
  \begin{itemize}
  \item inputs = dependencies
  \item body = commands to build the package
  \end{itemize}
\item base packages defined in Git
\item sandbox, no side effect
\item \texttt{/nix/store/}\emph{hash(inputs)}\texttt{-my-pkg}
\item immutable, read-only
\item \alert{precise} definition of \texttt{\$PATH}
\item \textbf{can build: container, VM, \emph{system images}}
\item \alert{not a silver bullet}: still possible to mess up!\\ $\hookrightarrow$ but limits the range of issues to investigate
\end{itemize}
  \end{column}
  \begin{column}{0.42\textwidth}
\only<1>{
    \begin{figure}
      \centering
      \includegraphics[width=\textwidth]{./figs/slides/func1.pdf}
    \end{figure}
}
\only<2->{
    \begin{figure}
      \centering
      \includegraphics[width=\textwidth]{./figs/slides/func2.pdf}
    \end{figure}
}
  \end{column}
\end{columns}
\onslide<3>{
\begin{tikzpicture}[remember picture,overlay]
    \fill[black,opacity=0.5] (current page.south west) rectangle (current page.north east); 
    \node[draw=orange,fill=white, text width=16cm, align=center] at (current page.center) {\Large \alert{Take Away}: Can generate software environments which are \alert{reproducible}/traceable/easy to modify};
\end{tikzpicture}
}


<!---
## Did you say "functional"?!

\begin{columns}
\begin{column}{0.45\textwidth}
\only<1>{
    \begin{figure}
      \centering
      \includegraphics[width=\textwidth]{./figs/slides/func1.pdf}
    \end{figure}
}
\only<2->{
    \begin{figure}
      \centering
      \includegraphics[width=\textwidth]{./figs/slides/func2.pdf}
    \end{figure}
}
\end{column}
\begin{column}{0.55\textwidth}
\begin{itemize}
\item<1-> base packages defined in Git
\item<3-> built packages stored in a "\texttt{store}"
\item<3-> \texttt{/nix/store/}\emph{hash(inputs)}\texttt{-my-pkg}
\item<3-> can have several versions of \emph{same} package
\item<3-> \alert{precise} definition of \texttt{\$PATH}
\item<3-> symbolic links between packages
\end{itemize}
\end{column}
\end{columns}
--->


## NixOS Compose \cite{nxc}

\begin{columns}
\begin{column}{0.4\textwidth}
    \scalebox{0.585}{
        \begin{tikzpicture}
        \node[anchor=south west,inner sep=0] at (0,0) {\lstinputlisting{listings/k3s.nix}};
        % ------ ROLE
        % \draw[red,ultra thick,rounded corners] (0.5, 10.6) rectangle (10, 5.2);
        %\draw [decorate, decoration = {brace, mirror, amplitude=8pt}, qorange, ultra thick] (0.3, 10.6) --  (0.3, 5.2);
        %\draw[-latex, qorange, ultra thick](-2, 7.9) -- (-0.3, 7.9) node[pos=0.26,above]  {Role} ;

        % ------ PACKAGES
        % \draw[blue,ultra thick,rounded corners] (0.9, 10.2) rectangle (9.8, 9);
        % \draw [decorate, decoration = {brace, amplitude=8pt}, orange, ultra thick] (9.8, 10.1) --  (9.8, 9.1);
        \draw[orange, latex-, ultra thick]  (5, 10.75) -- (7, 10.75) node[right, align=right, anchor=west] {\LARGE \textbf{Packages}};
        \draw[orange, latex-, ultra thick]  (5, 9.5)   -- (7, 9.5)   node[right, align=right, anchor=west] {\LARGE \textbf{Ports}};
        \draw[orange, latex-, ultra thick]  (5, 8)     -- (7, 8)     node[right, align=right, anchor=west] {\LARGE \textbf{Services}};
        %\draw[-latex, qpurple, ultra thick](12, 9.6) -- (10.5, 9.6) node[pos=0.46,above]  {Packages} ;

        % ------ PORTS
        % \draw[blue,ultra thick,rounded corners] (0.9, 9) rectangle (9.8, 7.85);
        %\draw [decorate, decoration = {brace, amplitude=8pt}, qpurple, ultra thick] (9.8, 8.9) --  (9.8, 7.95);
        %\draw[-latex, qpurple, ultra thick](12, 8.425) -- (10.5, 8.435) node[pos=0.46,above]  {Ports} ;

        % ------ SERVICES
        % \draw[blue,ultra thick,rounded corners] (0.9, 7.85) rectangle (9.8, 5.55);
        %\draw [decorate, decoration = {brace, amplitude=8pt}, qpurple, ultra thick] (9.8, 7.75) --  (9.8, 5.65);
        %\draw[-latex, qpurple, ultra thick](12, 6.70) -- (10.5, 6.70) node[pos=0.46,above]  {Services} ;
    \end{tikzpicture}
    }
\end{column}
\begin{column}{0.6\textwidth}
\begin{itemize}
\item Python + Nix ($\simeq$ 4000 l.o.c.)
\item for distributed systems
\item \alert{single description} (in Nix), multiple targets
\item docker{\small \semitransp{-compose}}, VM, ramdisk, system image
\item build, deploy, connect: \alert{unique interface}
\item contextualization (ssh keys, \texttt{/etc/hosts}, etc.)
\item can \alert{quickly} setup distributed envs \alert{locally!}
\item integration with Execo \cite{imbert_execo}
\item a few, but happy, users \smiley
\end{itemize}
\end{column}
\end{columns}

## Comparisons - Setting up a distributed environment on Grid'5000

\begin{columns}
\begin{column}{0.45\textwidth}
\begin{block}{Kameleon \cite{ruiz_reconstructable_2015}}
\begin{figure}
\centering
\includegraphics[width=0.95\textwidth]{./figs/eval_nxc_kameleon_single.pdf}
\end{figure}
build $\rightsquigarrow$ modify (add \texttt{hello}) $\rightsquigarrow$ build again
\end{block}
\end{column}
\begin{column}{0.55\textwidth}
\vspace{-1.2cm}
\begin{block}{EnOSlib \cite{cherrueau_enoslib_2022}}
\begin{figure}
\centering
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width = \textwidth]{./figs/eval_nxc_enoslib_single.pdf}};
\onslide<2->{
    % flent
    \draw[red,ultra thick,rounded corners] (4.2, 1.10) rectangle (5.95, 2.5);
}
\end{tikzpicture}
\end{figure}
NixOS Compose $\rightsquigarrow$ provisioning done in image
\end{block}
\end{column}
\end{columns}

\onslide<3>{
\begin{tikzpicture}[remember picture,overlay]
    \fill[black,opacity=0.5] (current page.south west) rectangle (current page.north east); 
    \node[draw=orange,fill=white, text width=16cm, align=center] at (current page.center) {
\Large $\hookrightarrow$ Fast builds, \alert{faster rebuilds} $\rightsquigarrow$ reduces development cycles \\
\Large $\hookrightarrow$ Fast deploys, \alert{reduce provisioning phases}

};
\end{tikzpicture}
}



<!---
## Comparisons - System Images with 


\begin{figure}
\centering
\includegraphics[width=\textwidth]{./figs/eval_nxc_kameleon.pdf}
\end{figure}

\onslide<2>{
\begin{center}
 {\Large $\hookrightarrow$ Fast builds, \alert{faster rebuilds} $\rightsquigarrow$ reduces development cycles}
\end{center}
}

## Comparisons - EnOSlib \cite{cherrueau_enoslib_2022}

Evaluation deployment cycles vs. EnOSlib with \alert{reproducibility considerations}

\begin{figure}
\centering
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width = \textwidth]{./figs/eval_nxc_enoslib.pdf}};
\onslide<2->{
    % flent
    \draw[red,ultra thick,rounded corners] (4.13, 1.25) rectangle (5.65, 2.5);
    % k3s
    \draw[red,ultra thick,rounded corners] (10.7, 1.25) rectangle (12.17, 2.5);
}
\end{tikzpicture}
\end{figure}


\onslide<2>{
\begin{center}
\Large $\hookrightarrow$ Fast deploys, \alert{reduce provisioning phases}
\end{center}
}
--->

## Experiment costs and reproducibility: Wrapping up

\begin{columns}
\begin{column}{.51\textwidth}
\begin{block}{Objectives}
\begin{itemize}
\item Reduce cost of experimenting with grid/cluster middlewares \alert{$\checkmark$}
\item Improve development cycles for reproducible experiments \alert{$\checkmark$}
\end{itemize}
\end{block}
\begin{block}{Limitations and Perspectives}
\begin{itemize}
\item More popular Parallel File-Systems
\item Source of the performance loss unclear 
\item Other platforms for NixOS Compose
\item Hybrid/folded deployments
\item Simulation: PFS and sensors
\end{itemize}
\end{block}
\end{column}
\begin{column}{0.49\textwidth}
\begin{figure}
\scalebox{0.62}{
\begin{tikzpicture}
\draw[step=2] (0, 0) grid (8, 8);
\draw[step=4, very thick] (0, 0) grid (8, 8);
\draw[very thick,->](0,0) -- (8.5,0); % node[anchor=west] {Realism};
\draw[very thick,->](0,0) -- (0,8.5); % node[above] {Cost/Difficulty};



\draw (2, 0) node[below] {Modeled system}; 
\draw (6, 0) node[below] {Real system}; 
\draw (4, -0.75) node[below] {\textbf{Realism}};

\draw (0, 6) node[rotate=90,above] {High}; 
\draw (0, 2) node[rotate=90,above] {Low}; 
\draw (-0.75, 4) node[rotate=90,above] {\textbf{Cost (Energy/Development/Deployment)}};
\draw (2, 2) node[text width=2cm, align=center] {Simulation};

\onslide<1-2>{
\draw (7, 7) node[text width=2cm, align=center] {Full scale};
\draw (5, 5) node[text width=2cm, align=center] {Reduced scale};
}

\draw (7, 1) node[text width=2cm, align=center, thick] {\alert{Objective}};

\onslide<2>{
\draw (6.5, 5) node[text width=2cm, align=center] {\textbf{Folding}};
}

\onslide<3->{
\draw (7, 6.5) node[text width=2cm, align=center] {Full scale};
\draw (5, 4.5) node[text width=2cm, align=center] {Reduced scale};
\draw (6.5, 4.5) node[text width=2cm, align=center] {\textbf{Folding}};
\draw[ultra thick,->,orange](8.5,6.5) -- (8.5,4) node[below,text width=2cm, align=center, anchor=west,pos=0.5] {\textcolor{black}{\textbf{NixOS Compose}}};
}


\end{tikzpicture}
}
\end{figure}
\end{column}
\end{columns}
\onslide<4>{
\begin{tikzpicture}[remember picture,overlay]
    \fill[black,opacity=0.5] (current page.south west) rectangle (current page.north east); 
    \node[draw=orange,fill=white, text width=16cm, align=center] at (current page.center) {\Large \alert{Take Away}: Reduced the time/energy cost to experiment with distributed systems, and improve reproducibility};
\end{tikzpicture}
}

# Concluding thoughts

## Conclusion and Perspectives

\begin{alertblock}{Initial Problem}
\begin{center}
How to harvest HPC idle resources while controlling the impact on the priority jobs?
\end{center}
\end{alertblock}

\begin{block}{Contributions}
\begin{itemize}
\item Design/implement an Autonomic loop in CiGri...
\begin{itemize}
\item to control the load of the File-System $\rightsquigarrow$ control overhead, avoid overload
\item to reduce the wasted computing power (idle and killed)
\end{itemize}
\item ... using Control Theory
\begin{itemize}
\item yields guarantees and explainability
\item guidelines for system administrators, tutorial
\end{itemize}
\item Reduce experiment costs
\begin{itemize}
\item reduce number of machines to deploy without loss of realism
\item tool for developing and deploying reproducible distributed environments
\end{itemize}
\end{itemize}
\end{block}


<!---
\begin{itemize}
\item liste de perspectives aussi ctrl
\item First steps in simulating CiGri (in Batsim\ \cite{dutot:hal-01333471})
   \begin{itemize} 
    \item limited by the lack of sensors/probes
    \item lack of support for File-System simulation
   \end{itemize} 

\item global picture: cost vs. realism
\item nfs-store: plus rapide, moins repro
\end{itemize}
--->

## Open question

\begin{block}{CiGri}
Improve usage of computing clusters
\end{block}

\begin{block}{Folding}
Reduce number of physical machines required to represent a full scale cluster
\end{block}

\begin{block}{NixOS Compose}
Reduce development time, and reduce "test" deployments
\end{block}

<!----
\begin{block}{Simulation}
First steps of simulating CiGri, but \alert{lack of support for PFS simulation}
\end{block}
--->
\pause

\vspace{0.5cm}

\begin{center}
\Large But can it introduce a \alert{rebound effect}?
\end{center}
