{ stdenv, fetchgit, simgrid, boost, cmake }:

stdenv.mkDerivation rec {
  pname = "chord";
  version = "0.1.0";

  src = fetchgit {
    url = "https://gitlab.inria.fr/me/chord";
    rev = "069d2a5bfa4c40...";
    sha256 = "sha256-ff4f...";
  };

  buildInputs = [ simgrid boost cmake ];

  # configurePhase = "cmake .";
  # buildPhase = "make";
  # installPhase = "mkdir -p $out/bin &&\
        mv chord $out/bin";
}
