
## This Thesis {.unnumbered}


<!---
HPC systems are complex and expensive, and they are not making use of all their resources.
This is due to the rigid nature of the jobs.
The idle computing power can be harvested by executing smaller and interruptible jobs.
However, those jobs can introduce perturbations in the system and degrade the performances of the premium users jobs (\eg\ via an overload of the distributed file-system).
As the jobs harvesting the idle resources are interruptible, it is tempting to kill them when the system is about to get overloaded.
However, this killing also represent a lost of computing power as these jobs do now usually implement any check pointing mechanism and will need to be restarted from the beginning later on.
--->
We believe that the harvesting of idle resources represents a regulation problem which can be tackled via an Autonomic Computing approach.
To provide a runtime behavior with guarantees, we claim that using tools and methods from Control Theory will help in terms of performance and adoption.
However, controllers are usually tuned for a specific system, which might reduce their reusability on different systems.

When designing and implementing feedback loops, especially using Control Theory tools, the quality and robustness of the experimental setup is crucial.
Reproducible experiments are a must to ensure the validity of the resulting controller.
Moreover, distributed experiments involving computing cluster middlewares (\eg\ batch scheduler, Parallel File-Systems) are especially tricky.
Faithful and realistic experiments with such middlewares would require deploying clusters at full scale, which cost is excessive.
Simulation techniques can be used but require correct and proven models.
Intermediate strategies to reduce the number of machines to deploy the experiments and keeping a full scale behavior of the system would allow to reduce the experimental costs while performing meaningful experiments.

From the above two observations, the work of this thesis will be organized in two parts:
In Part I, we investigate an Autonomic Controller using Control Theory tools to regulate the harvesting of idle HPC cluster resources.

- In Chapter \ref{chap:sota}, we give some context to this work, and present the \cigri\ middleware which will be the central piece of this thesis.

- Chapter \ref{chap:cigri_jobs} studies the statistical characteristics of the \cigri\ jobs.

- Chapter \ref{chap:harvest_fs} presents a controller regulating the \cigri\ injection speed based on the load of the distributed file-system to reduce the overhead for premium users jobs.

- We discuss the reusability of autonomic controllers using control theory tools from one system to another in Chapter \ref{chap:reuse}.

- In Chapter \ref{chap:ff}, we present a controller reducing the wasted computing time of a cluster with \cigri\ by also considering the killed low priority jobs.

- We conclude Part I and give perspectives in Chapter \ref{chap:conclu_ctrl}.

In Part II, we discuss distributed experiments and their reproducibility.

- Chapter \ref{chap:distributed_expes} gives context to the state of distributed experiments and their reproducibility.

- In Chapter \ref{chap:nxc}, we present \nxc, a new tool to create and deploy reproducible distributed software environments.

- Chapter \ref{chap:folding} investigates the reduction of deployment of a distributed experiment involving a distributed file-system while keeping a realistic full scale behavior. 

- Chapter \ref{chap:batcigri} presents the first step towards a simulator of \cigri\ and our autonomic controllers of Part I using \bat.

- We conclude Part II and give perspectives in Chapter \ref{chap:conclu_expe}.

Chapter \ref{chap:meta_conclu} wraps up the entire work done in this thesis.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{./deps.pdf}
    \caption{Dependencies between the different chapters of this thesis.}
    \label{fig:chap_deps}
\end{figure}

Some chapters can be read independently.
Figure \ref{fig:chap_deps} depicts the dependency graph of the chapters of this thesis.

## Work dissemination {.unnumbered}

The following are the communications resulting from this thesis.

#### International conferences {.unnumbered}

- Quentin Guilloteau, Jonathan Bleuzen, Millian Poquet, and Olivier Richard. “Painless Transposition of Reproducible Distributed Environments with NixOS Compose”. CLUSTER 2022\ \cite{nxc}

- Quentin Guilloteau, Bogdan Robu, Cédric Join, Michel Flies and Eric Rutten. “Model-free control for resource harvesting in computing grids”. CCTA 2022\ \cite{guilloteau:mfc}

- Quentin Guilloteau, Olivier Richard, Bogdan Robu, and Eric Rutten. “Controlling the Injection of Best-Effort Tasks to Harvest Idle Computing Grid Resources”. ICSTCC 2021\ \cite{guilloteau:icstcc}

  
#### National conferences {.unnumbered}

- Quentin Guilloteau, Adrien Faure, Millian Poquet, and Olivier Richard. “Comment rater la reproductibilité de ses expériences ?” COMPAS 2023\ \cite{rater_repro}.

- Quentin Guilloteau, Jonathan Bleuzen, Millian Poquet, and Olivier Richard. “Transposition d'environnements distribués reproductibles avec NixOS Compose”. COMPAS 2022\ \cite{guilloteau:hal-03696485}

- Quentin Guilloteau, Olivier Richard, and Éric Rutten. “Étude des applications Bag-of-Tasks du méso-centre Gricad”. COMPAS 2022\ \cite{guilloteau2022etude}

- Quentin Guilloteau, Olivier Richard, Eric Rutten, and Bogdan Robu. “Collecte de ressources libres dans une grille en préservant le système de fichiers : une approche autonomique”. COMPAS 2021\ \cite{guilloteau:compas}

#### Working papers {.unnumbered}

- Quentin Guilloteau, Olivier Richard, Raphaël Bleuse, and Eric Rutten. “Folding a Cluster containing a Distributed File-System”. 2023\ \cite{guilloteau:hal-04038000}

- Quentin Guilloteau. “Simulating a Multi-Layered Grid Middleware”. 2023\ \cite{guilloteau:hal-04101015}


#### Tutorials {.unnumbered}

- Quentin Guilloteau, Jonathan Bleuzen, Millian Poquet, and Olivier Richard. Initiation to NixOS Compose. \cite{tuto_nxc}

- Quentin Guilloteau, Sophie Cerf, Eric Rutten, Raphaël Bleuse, and Bogdan Robu. Introduction to Control Theory for Computer Scientists. \cite{tuto_control}



