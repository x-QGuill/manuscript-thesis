## Background {.unnumbered}


<!---
- need for automatization

- requirements more and more complex

- hpc used a lot by scientists

- more and more powerful (top500)

- very costly

- need to squeeze every ounce of performance and usage

- green500, getting better

- but idle resources still consume a lot

- very difficult to use scheduling strategies to use the resources better: a lot of variation and unpredictable stuff

- need a runtime management

--->

<!---
### High-Performance Computing {.unnumbered}
--->

As calculations started to be too complex to be carried out by hand by slow and error-prone humans, scientists developed machines able to help them perform computations.
These machines improved though history, from the Abacus, the Pascal's calculator, the Thomas de Colmar's Arithmometer, the tabulating machine for punched cards from Herman Hollerith, or the famous Turing machine.

As every field of Science continued to go forward, and challenges to grow larger and more complex, the amount of computation needed to perform experiments kept increasing, and required the machines to also improve.
These improvements were possible with the reduction in size of transistors and circuit boards, as well as the improvements of the raw components (processors, memory, buses, etc.).
As computers reached Moore's law, but problem sizes were still increasing, a single machine could not manage the entire copmutational workload.
The solution was to link several machines together and distribute the workload on this network of nodes, creating a *computing cluster*.
To keep up with the computing demands, these clusters have to add more machines and/or change for newer and more powerful computers.
Figure \ref{fig:top500} shows the evolution of the numbers of cores, CPU frequency and performance of the clusters of the Top500 list.
Even if the CPU frequency has stayed stable for close to 20 years, the performance keeps improving thanks to the increase of parallelism.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{./figs/top500.pdf}
  \caption{Evolution of the number of cores, CPU frequency, and the maximum performance of the Top500 machines. The solid line represents the median, and the ribbons the 5\% and 95\%. Updated from \cite{cornebize2021high} with the data from \cite{dlenski_top500}.}
  \label{fig:top500}
\end{figure}


Building computing clusters is very expensive, and is usually a cooperative effort between several actors (laboratories, research institutes, universities, etc.).
This cooperation leads to shared access to the resulting cluster for the members of the different contributing entities.
To ensure *fair* access to the machines, a reservation mechanism is set up.
Users have to submit their computations alongside the number of machines, also called nodes, required and the estimated duration to the Resource and Job Management System (RJMS).
The RJMS, whose architecture is depicted in Figure \ref{fig:rjms}, is responsible for managing the users computations, scheduling them onto the resources (machines, CPU, or even cores), and managing the resources (deploying and starting the computations, as well as cleaning the environment when they completed).

\begin{figure}
    \centering
\begin{tikzpicture}%
[
	% platform
	fs/.style={
		execute at end node={\includegraphics[height=4ex]{figs/database}},
	},
	rack/.style={
		execute at end node={\includegraphics[height=4ex]{figs/server}},
	},
	interconnect/.style={
		very thick,
		line cap=round,
	},
	netlink/.style={
		thin,
	},
	% RJMS
	rjms/.style={
		draw,
		inner sep=.5em,
		dashed,
	},
	component/.style={draw,
		align=center,
		text width=6em,
		minimum height=4ex,
		outer sep=2,
	},
	label/.style={
		font=\vphantom{Ag},
		anchor=south west,
	},
	% misc.
	users/.style={
		execute at end node={\includegraphics[height=4ex]{figs/group}},
	},
	link/.style={
		shorten >=3pt,
		shorten <=3pt,
		thick,
	},
]
	\coordinate (netbase) at (0,0);
	\node [rack] (rack0) [below of=netbase] {};
	\node [rack] (rack1) [right of=rack0]   {};
	\node [rack] (rack2) [right of=rack1]   {};
	\node [rack] (rack3) [right of=rack2]   {};
	\node [fs] (fs0) [right of=rack3, xshift=1em] {};
	\node [fs] (fs1) [right of=fs0]               {};
	\draw [interconnect] (netbase.center) -- (netbase.center -| fs1);
	\draw [netlink] (rack0) -- (rack0 |- netbase);
	\draw [netlink] (rack1) -- (rack1 |- netbase);
	\draw [netlink] (rack2) -- (rack2 |- netbase);
	\draw [netlink] (rack3) -- (rack3 |- netbase);
	\draw [netlink] (fs0) -- (fs0 |- netbase);
	\draw [netlink] (fs1) -- (fs1 |- netbase);
	\draw (rack0.south) -- node [anchor=north] {computing nodes} (rack3.south);
	\draw (fs0.south) -- node [anchor=north] {file system} (fs1.south);
	\coordinate [yshift=4ex] (rjmsbase) at (netbase -| fs1);
	\node [component,anchor=south east] (resman) at (rjmsbase)    {res.\@ manager};
	\node [component,anchor=east]       (sched)  at (resman.west) {scheduler};
	\node [component,anchor=east]       (appman) at (sched.west)  {job manager};
	\node (rjms) [rjms,fit=(appman)(sched)(resman)] {};
	%\node (rjms) [rjms] {};
	\node [label, anchor=south east] at (rjms.north east) {RJMS};
	\coordinate [yshift=6ex] (usersbase) at (rjms.north west);
	\node [users] (users) at (usersbase) {};
	\node [anchor=north] at (users.south) {users};
	\draw [link,->] (users.east) -| node [pos=0.26,anchor=south] {\footnotesize job submission} (appman.30);
	\draw [link,<->] (resman.south) -- (resman.south |- netbase);

\end{tikzpicture}
    \caption{Illustration of a Resource and Job Management System (RJMS) \cite{bleuse:tel-01722991}}
    \label{fig:rjms}
\end{figure}

This reservation process is not perfect, and can lead to the idling of some resources due to the requirements on the resources.
While machines are idle, they still consume energy, which is around half of a machine used at full power\ \cite{heinrich2017predicting}.
This idling can represent a significant loss of energy and thus money.
Turning them on and off is possible but introduces new energetic costs, and degradation of the Quality-of-Service (QoS) if users need the resources.
As HPC jobs are large and rigid in the number of resources they require, it makes it difficult to improve the use of the idle CPU time.
A lot of effort has been put on the research of better scheduling algorithms for the RJMS, meaning how to map computation to physical machines.
However, they often require the users to estimate correctly the duration of their computation, which is notoriously difficult for humans to do correctly \cite{cirne2001comprehensive,mu2001utilization}.
In practice, despite all this work, and because of the conservative behavior of the HPC community, the majority of HPC centers use the same, or a variation of, scheduling policy: EASY Backfilling.
This scheduling technique is simple to implement, to understand, and with good enough performance, which eases its adoption in HPC centers.

The idle computing power can be harvested by executing smaller and interruptible jobs seen as second-class citizens.
However, those jobs can introduce perturbations in the system and degrade the performances of the premium users jobs (\eg\ via an overload of the distributed file-system).
As the jobs harvesting the idle resources are interruptible, it is tempting to kill them when the system is about to get overloaded.
However, this killing also represent a lost of computing power as these jobs do not usually implement any check-pointing mechanism and will thus need to be restarted from the beginning later on.

In general, HPC systems are very unpredictable \cite{bhatele2013there, skinner2005understanding, dorier2014calciom}.
The performance of an HPC application can be affected by many exterior factors: network contention, file-system load, temperature in the rack, etc.
All of these factors are near impossible to predict correctly, and computational costly to solve optimally, and thus impossible to incorporate into the decision process of the RJMS.


To deal with such runtime variations of computing systems, the field of Autonomic Computing proposes a feedback loop point-of-view to regulate the under control system and to steer it towards a desired behavior/state.
The feedback loop of the Autonomic Computing can be interpreted and implemented in various fashions.
One particularly interesting way is to use tools from the Control Theory field.
Control Theory is usually applied to physical dynamical systems, but very rarely to computing systems.
However, it provides mathematically proven properties on the controlled system, which makes it quite appealing compared to more black boxes such as Machine Learning approaches, or to simpler rule based strategies.


<!---
### Experiments \& Reproducibility {.unnumbered}
--->

Experimenting on distributed systems, such as grid or cluster middlewares, is complex.
To realistic evaluate a new solution, researchers need to deploy their own cluster, with a RJMS, a parallel file-system, etc.
In practice, such deployments are rarely done, as they require a lot of machines, and because the software environment to deploy is deep, fragile, and complex.
The fragility of the software environment is a crucial reproducibility issue, as it means that an experiment done today could not be reproduced in the future.
To still perform the experiment, one might use simulation techniques.
But even state-of-the-art simulators fail to fully reproduce the behavior of experiment in real conditions.


<!---
The scientific community as a whole has been traversing a \repro\ crisis for the last decade, and computer science does not make an exception\ \cite{randallIrreproducibilityCrisisModern2018,baker500ScientistsLift2016}.
The \repro\ of the research work is essential to build on robust knowledge, and it increases the trustworthiness of results while limiting the number of methodology and analysis bias.
To highlight the reproducible research works, several publishers (like ACM or Springer) set up an artifact evaluation of a submission.
This peer review process of the experimental artifact can yield one or several badgers to the authors based on the level of \repro\ of their artifacts.

The increase of \repro\ issues in experimental computer science research is partially linked to the growth in complexity of software and hardware.
And this complexity is even more exacerbated in distributed systems.
--->


<!---
\begin{figure}
  \centering
  \includegraphics[width=0.95\textwidth]{./figs/comparison_lu_ep_idle.pdf}
  \caption{Power consumption over time when running NAS-EP, NAS-LU, HPL or idling. From \cite{heinrich2017predicting}.}
\end{figure}
--->

