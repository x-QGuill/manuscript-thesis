
<!---
## Introduction
Scientists from all fields have to perform computations.
These computations often cannot be executed on a personal machine.
This is due to the requested amount of memory, storage, or reasonable execution times.
Thus, scientists turned to computing grid and computing clusters. 
Computing clusters are composed of numerous powerful machines linked together with fast network.
Accessing machines of a cluster usually goes through a reservation process.
Users submit their computation, estimated execution time (*walltime*), as well as the requirements on the nodes (\eg\ GPU) to Resource and Job Management System (RJMS).
This RJMS, often just called *scheduler*, is responsible to map users computation to the nodes.
The reservation and scheduling processes can lead to some resources being left idle, either because of the lack of demand from the users, or because of requirements.
This idleness represents a waste of computing power as the machines are still being powered on and must be exploited.

Harvesting the idle resources of a set of machines as been studied in different contexts.
BOINC\ \cite{anderson_boinc_2004} is a volunteer computing project aiming at using the idle CPU time of personnal machines.
Users, which are not necessary scientists, can register their machine to the BOINC network.
When their machine goes idle, a request for work is sent to BOINC which will send back some computations to be executed.
Some examples of research project relying on BOINC are Seti@Home\ \cite{anderson_setihome_2002}, Folding@Home, TODO.
Condor\ \cite{litzkow1987condor} has a similar approach but for workstations.
Users of Condor can submit large computation that will then be executed on the idle registered workstations of the network.

In the context of High-Performance Computing (HPC), there have been several approaches to use the idle resources of a computing cluster.
Bebida\ \cite{mercier_big_2017} uses those HPC idle resources to perform Big-Data analysis.
It works by starting a Hadoop\ \cite{shvachko2010hadoop} service on the newly idle nodes.
The Hadoop server will then submit jobs to the now Hadoop nodes.
The HPC scheduler does not see the Big-Data nodes and consider them idle, which allows Bebida to not disturb regular users of the cluster. 
In\ \cite{przybylski2022using}, authors present a Serverless approach to harvest the idle resources of an HPC cluster with OpenWhisk\ \cite{openwhisk}.
Another approach is the one taken by \cigri\ by submitting \BoT\ applications with the lowest priority to the scheduler
-->
 
This chapter is based on \cite{guilloteau2022etude} published at ComPAS 2022 with Olivier Richard and Eric Rutten.
In this Chapter, we study the characteristics of the \BoT\ applications executed by \cigri\ on the Gricad computing center.
The objective is to characterize such applications to be able to generated realistic synthetic \cigri\ workloads for the study of our controllers presented in the following chapters.
This study relies on the \cigri\ jobs executed on the \gricad\ mesocenter between January 2013 and April 2023.
The datasets as well as the analysis scripts are available on Zenodo\ \cite{zenodo_cigri}.
<!---
\footnote{\url{https://zenodo.org/record/6787030}}. 
--->
