
## Global Study of the \cigri\ Jobs {#sec:global}


\begin{figure}
  \centering
  \includegraphics[width=0.7\textwidth]{./figs/jobs_cigri/global/ecdf_comparison.pdf}
  \caption{
Empirical Cumulative Distribution Function (ecdf) for the execution times of the \BoT\ jobs executed on the grids \gricad\ and \emph{DAS2}.
Globally, the \BoT\ jobs executed on the \gricad\ center are longer than on \emph{DAS2}.
}
  \label{fig:ecdf_global}
\end{figure}



\begin{table}
   \centering
    \scalebox{1.2}{
     \begin{tabular}{ l  r  r }
     \toprule
     \multirow{2}*{Metrics} & \multicolumn{2}{c}{Computing Grids} \\
     \cmidrule(lr){2-3}
       & \gricad\ & \emph{DAS2} \\
      \midrule
      Number of BoT jobs & $\simeq 4.4\times 10^7$ & $\simeq 10^5$ \\%97167\\
      Number of clusters & 8 & 5 \\
      \midrule
      Minimum $t_{exec}$ & 1s & 1s\\
      Average $t_{exec}$ & 12m 43s & 3m 51s \\
      Maximum $t_{exec}$ & 9d 1h 27m & 10h \\
      \midrule
      Median         $t_{exec}$ & 1m 13s & 24s\\
      Quantile 75 \% $t_{exec}$ & 5m 25s & 2m 51s \\
      Quantile 95 \% $t_{exec}$ & 48m 16s & 15m 1s \\
      Quantile 99 \% $t_{exec}$ & 2h 49m 13s & 43m 35s \\
      \bottomrule
     \end{tabular}
    }
   \caption{Table summarazing the dataset. $t_{exec}$ represents the execution times of the \BoT\ jobs from the two computing grids \gricad\ and \emph{DAS2}.}
   \label{table:table}
\end{table}

In this section, we are interested in the global characteristics of the \cigri\ jobs.
Figure\ \ref{fig:ecdf_global} shows the empirical cumulative distribution function (ecdf) for the execution times of the \cigri\ jobs, as well as the \BoT\ jobs from the DAS2 grid.
\emph{DAS2}\ \cite{das2} is the second generation of computing grids for Dutch universities.
This is the only available workload containing explicit \BoT\ applications that we are aware of. 
Table\ \ref{table:table} shows a side-to-side comparison of execution time metrics between the two considered computing grids.
Globally, the execution times of the \BoT\ jobs executed on the \gricad\ center are longer than on \emph{DAS2}.
Indeed, half of the jobs on *DAS2* last less than 30 seconds, whereas half of the jobs on \gricad\ last less than a minute.  
The longest job on *DAS2* runs for a few hours and the longest job for \gricad\ lasts several days.
This difference could be explained by the usage of the grid by the users, the type of jobs, the number of available machines, etc.

\begin{lesson}{}{}
Half of the \cigri\ jobs last less than a minute.
\end{lesson}

Figure\ \ref{fig:ecdf_global} shows the aggregated distributions during the last 10 years.
The evolution during the years of the number of jobs executed by \cigri\ is depicted in Figure\ \ref{fig:evolution_nb_jobs}. 
We can see that there are at least 100000 jobs executed every quarter and often around a million \BoT\ jobs.
We do not observe any trend either in increase or decrease of usage of \cigri.

\begin{lesson}{}{}
In average, a million of \cigri\ jobs are being executed every quarter.
\end{lesson}

\begin{figure*}
  \centering
  \includegraphics[width=\textwidth]{./figs/jobs_cigri/temporal_analysis/global/temporal_evolution_nb_jobs_global.pdf}
  \caption{
Quarterly evolution of the number of \BoT\ jobs executed by \cigri\ on the \gricad\ mesocenter.
Every quarter, there are at least 100000 jobs being executed, and in average around a million jobs.
}
  \label{fig:evolution_nb_jobs}
\end{figure*}


Figure\ \ref{fig:evolution_duration} depicts the evolution of the mean and median execution times of the jobs executed per quarters of year.
The median execution time is in the order of a few minutes, whereas the mean execution time is more in the order of dozen minutes or one hour.
We observe variations in the two metrics, which hints for changes in usage of \cigri.
But, as seen on Figure\ \ref{fig:evolution_nb_jobs}, the number of jobs does not vary much, which indicates that the execution time of the jobs executed varies.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{./figs/jobs_cigri/temporal_analysis/global/temporal_evolution_duration_global.pdf}
  \caption{
Quarterly evolution of the mean and median execution times of the \BoT\ jobs executed by \cigri\ on the \gricad\ mesocenter.
The median execution time is in the order of a few minutes, whereas the mean execution time is more in the order of dozen minutes or one hour.
  }
  \label{fig:evolution_duration}
\end{figure}

To understand the reason of this change of behavior through the years, let us look at the projects executed.
We remind the reader that a project contains campaigns which themselves contain jobs. 
Campaigns from the same project have similar behavior (number of jobs and execution times).
Figure\ \ref{fig:hist_quater} shows the evolution of the distribution of the job execution times per quarter.
We can see that there are often several modes of distributions.
These modes correspond to the dominant usage of jobs from the same projects.
For example, from the last quarter of 2019 (19-4) to the third quarter of 2022 (22-3) we can see that there is always a mode of distribution around one minute.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{./figs/jobs_cigri/temporal_analysis/global/temporal_evolution_campaign.pdf}
  \caption{
Distribution of the execution times per quarter.
We can see the dominant usage of \cigri\ by some project.
From 19-4 to 22-3, most of the jobs last around 1 minutes, which hints that most of the jobs come from the same project.
}
  \label{fig:hist_quater}
\end{figure}

Figure\ \ref{fig:projects_work} shows the proportion of the \BoT\ jobs executed by quarter which belonging to given projects.
It shows also the proportion of work (\ie\ total execution time times the number of machine used) for the \cigri\ projects. 
We can see that there is often a project submitting the majority of jobs executed for a given quarter.
For example, during the years 2020 and 2021, the project `biggnss` was responsible for the majority of the \BoT\ jobs executed.
However, it is noteworthy that the project having the majority of jobs executed during a period, is not necessary the one that consumed the most resources (work). 
For the same period, between 2020 and 2021, the `biggnss` project does not have the majority of the \cigri\ jobs executed during this period (quarter 4 of 2020 for example).
Note that we can now indeed confirm that from 19-4 to 22-3, the same project (`biggnss` in this case) had the majority of jobs being executed.

\begin{lesson}{}{}
The project with the most jobs executed is not always the one doing the most work.
This hints that there are different "shapes" of \cigri\ campaigns: a lot of small jobs or fewer jobs but longer.
\end{lesson}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{./figs/jobs_cigri/global/proportion_project_temporal.pdf}
  \caption{
Evolution of the proportion of \BoT\ jobs executed by \cigri\ on the \gricad\ computing center over the years, as well as the evolution of the work (execution time times the number of resources) of \cigri\ jobs.
We observe that there is often one project which has the majority of the jobs executed during a quarter.
However, this project does not necessarily perform the most work.
For example, for the year 2020, the \texttt{biggnss} project has the majority of jobs executed, but not the majority of executed work.
}
  \label{fig:projects_work}
\end{figure}

## Study of the \cigri\ Projects {#sec:projects}


\begin{figure*}
  \centering
  \includegraphics[width=\textwidth]{./figs/jobs_cigri/distributions/histograms_project.pdf}
  \caption{
Distribution of the execution times for the 10 \cigri\ projects with the most jobs.
We observe that most projects have a clear unique mode of distribution.
This mode can be very wide, like for \texttt{f-image} and \texttt{simsert}, or thin, like for \texttt{biggnss} and \texttt{pr-mdcp}.
}
  \label{fig:hist_10_projects}
\end{figure*}

Let us now focus on the distribution of execution times among the projects.
Figure\ \ref{fig:hist_10_projects} depicts the distribution of the execution times of the 10 \cigri\ projects with the most jobs.
We can see that for most of the project, there is one clear mode for the execution time as well as a heavy distribution tail.
The projects like `teembio` and `sdmtk` have two modes for the execution times.
In this case, there are several types of campaigns in the same project, one for each mode.
The width of the mode is also different from project to project.
For example, `biggnss` and `pr-mdcp` have a very thin mode of a few minutes, where projects like `f-image` and `simsert` have a range from a few seconds to a couple of hours.
Note that as the jobs are executed on a computing grid composed of several clusters, we also plot the clusters on which the jobs have been executed.


To model the distributions showed in Figure\ \ref{fig:hist_10_projects}, we perform a *goodness of fit* test with the following long-tail distributions: \norm, \lnorm, \frechet, \gammad\ and \weibull.
We consider only the 10 projects with the most jobs, and follow the methodology presented in\ \cite{javadi2009mining, brevik2003quantifying}.
For each campaign of these projects, we randomly chose 50 jobs.
As the behavior of the jobs can vary for different clusters of the computing grid, we will consider the distributions per pair (project, cluster).
For each of these pairs, and for each of the selected long-tail distributions, we perform a Cramer-von Mises test\ \cite{darling1957kolmogorov}.
This test computes the distance between the empirical distribution function of the execution times of the selected jobs and the empirical distribution function of the selected distributions.
The smaller the distance, the more likely the execution times distribution follows the tested distribution.    
As the Cramer-von Mises test generates *randomly* the empirical distribution function of the selected distributions, we repeat this process 30 times and take the mean of the distances.
<!---
Table\ \ref{tab:model} presents for each pair (project, cluster), the distribution fitting the best the execution times of this pair.
--->
Globally, the distributions which fit the best the execution times are the laws \lnorm, \frechet\ and \weibull.
The quality of the fitting for these laws is near identical.
This concurs with the result of\ \cite{iosup2010grid, iosup2008grid} for other computing grids.
<!---
We see also that some projects have the same distribution for all the clusters.
This is not the case for other projects such as TODO.
In some cases, there are not enough jobs for some clusters to correctly perform a goodness of fit test. 
--->

\begin{lesson}{}{}
Different projects have different distribution of execution times.
Most project have a clearly defined unique mode and a long tail.
The laws \lnorm, \frechet, and \weibull\ fit the best the distributions of execution times.
\end{lesson}


## Study of the \cigri\ Campaigns {#sec:campaigns}


\begin{figure}
  \centering
  \includegraphics[width=0.7\textwidth]{./figs/jobs_cigri/campaigns/ecdf_duration.pdf}
  \caption{
Empirical cumulative distribution function of the mean execution time of a campaign.
The average mean duration is around 1 hour, but the median mean duration is around 10 minutes.
}
\label{fig:campaigns_duration}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.7\textwidth]{./figs/jobs_cigri/campaigns/ecdf_nb_jobs.pdf}
  \caption{
Empirical cumulative distribution function of the number of jobs per campaign.
Half of the campaigns have less than 50 jobs, but there are in average 2500 jobs per campaign.
}
  \label{fig:campaigns_nb_jobs}
\end{figure}

Figures\ \ref{fig:campaigns_duration} and \ref{fig:campaigns_nb_jobs} show respectively the empirical cumulative distribution function for the mean job duration and the number of jobs in a campaign. 
Half of the campaigns have less than 50 jobs, and half of the campaigns have a mean execution time in the order of the dozen of minutes.

\begin{lesson}{}{}
The average campaign has around 2500 jobs with a mean execution time of 1 hour and 15 minutes.
\end{lesson}

\begin{figure}
  \centering
  \includegraphics[width=0.7\textwidth]{./figs/jobs_cigri/campaigns/nb_jobs_vs_duration.pdf}
  \caption{
Relation between the number of jobs in a campaign and the mean duration of its jobs.
We observe that large campaigns have short jobs, and small campaigns have long-lasting jobs. 
}
  \label{fig:nb_jobs_vs_duration}
\end{figure}


Figure\ \ref{fig:nb_jobs_vs_duration} plots the relation between the mean execution times of the jobs in a campaign and the number of jobs in this campaign.
We observe that large campaigns have short jobs, and small campaigns have long-lasting jobs.

\begin{lesson}{}{}
Large campaigns have short jobs, and small campaigns have long-lasting jobs.
\end{lesson}


<!---
## Generating Realistic \cigri\ Campaigns {#sec:gen_campaigns}

In this Section, we present a method to generate a realistic \cigri\ campaign.
We will focus on campaigns from the `biggnss` project as this is the project with the most jobs executed on the platform.

### Protocol

As seen on Figure\ \ref{fig:hist_10_projects}, the `biggnss` jobs seems to belong in two classes.
Once class with a mean execution time of 1 minutes, and the other class with a mean execution time of 15 minutes.
`biggnss` campaigns do contain jobs belonging to the two modes.

We perform a fit of a mixture of guassians on the `log` of the execution times.
So, in reality we are fitting a mixture of log-gaussians.




### Evaluation


To evaluate the quality of our generated campaigns, we take $N$ true `biggnss` campaigns at random from the data set, and generate $N$ campaigns with our method.
We then plot the distributions of the number of jobs per campaign, as well as the distribution of execution times.  

\begin{figure*}
  \centering
  \begin{subfigure}{0.7\textwidth}
    \centering
    \includegraphics[width=\textwidth]{./figs/jobs_cigri/cigri_io1.pdf}
    \caption{\todo{}}
  \end{subfigure}
  \begin{subfigure}{0.7\textwidth}
    \centering
    \includegraphics[width=\textwidth]{./figs/jobs_cigri/cigri_io2.pdf}
    \caption{\todo{}}
  \end{subfigure}
  \caption{\todo{}. From \cite{emeras2014analysis}.}

\end{figure*}
--->
