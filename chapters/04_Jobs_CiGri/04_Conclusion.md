## Job and Campaign model used in this Thesis

In this thesis, we will represent a campaign as a set of jobs having the exact same theoretical execution time.
In practice, we implement a job using the `sleep` command with the desired execution time.
Choosing this job representation, instead of executing real \bag\ applications, allows us to better control the characteristics of the campaign and create all sorts of synthetic scenarios.
<!---
We motivate this choice of job model from the experimental point of view in Section \ref{sec:eval:setup}.
--->

Some work remains to be done to be able to generate *realistic* \cigri\ campaigns.
The *realistic* aspect might be tricky as there are several dimensions to a campaign (\eg\ number of jobs, job execution times, distribution of execution times), each with their variability.
The work done in \cite{cornebize2022simulation} for MPI applications might give a good example and starting point.

There are still some open questions.
In particular, the origin of the long-tail of the execution time distribution is still unknown.
We suspect that it can come from the overhead of the scheduler to start and stop the jobs and/or from the load of the cluster (\eg\ file-system, network) at the moment of the execution.
