### The \gricad\ Computing Center

The \gricad\ computing center\footnote{\url{https://gricad.univ-grenoble-alpes.fr}} provides computing and storing infrastructures to the researchers of the region of Grenoble, France.
The center is composed of several computing clusters, each with a focus (\eg\ `luke` for data processing, `froggy` for HPC, or `dahu` for HPCDA\footnote{\emph{High Performance Computing and Data Analysis}}).
These clusters are linked together to form a computing grid.
This grid is also impacted by the issue of unused resources explained above. 

### The \cigri\ middleware

\cigri\ \cite{georgiou2007evaluations, cigri_sw} is a computing grid middleware set up in the \gricad\ computing center.
It interacts with the \oar\ schedulers \cite{capit_batch_2005, oar3} of each cluster.
The goal of \cigri\ is to use the idle resources of the entire computing grid.
Originally, \cigri\ was design to reduce the stress on the different schedulers of the grid when users had to execute large campaigns (tens of thousand of jobs or more).

Users of \cigri\ submit \bag\ applications to the middleware.
These applications are composed of numerous small, identical, and independent tasks making them a perfect candidate for the harvesting the idle resources.
An example of such application is Monte-Carlo simulations, where the user will execute a large batch of random independent experiments to conclude on the aggregated results.
 
Once the application submitted to the middleware, \cigri\ will submit batches of jobs to the clusters of the grid.
The jobs are submitted to the schedulers with the lowest priority (\be), which allows \oar\ to kill those \be\ job if a normal/premium user needs the resources.
Figure \ref{fig:cigri} summarizes the interaction between \cigri\ and the different schedulers of the computing grid.

\begin{figure}
  \centering
  \includegraphics[width=0.4\textwidth]{./figs/feedforward/cigri.png}
  \caption{Graphical representation of the interaction between \cigri\ and the different \oar\ schedulers of a computing grid.}
  \label{fig:cigri}
\end{figure}


### \cigri\ jobs

The hierarchy of \cigri\ jobs is as follows:

- a *job* belongs to a *campaign*

- a *campaign* belongs to a *project*

From the point of view of \cigri, a *campaign* is a \bag\ application.

One example of project is the processing of massive GPS data for the deformation of the Earth surface \cite{deprez2018toward}.
There are several geographic stations, with a lot of GPS data.
The processing of each station can be grouped as a *campaign*, where the processing of a subregion is a *job*. 

In this section, we present some characteristics of the \cigri\ jobs.
The data presented in this section represent the jobs submitted from January $2^{nd}$ 2017 to October $8^{th}$ 2021 (58 months), representing about 30 millions jobs. 
More details can be found in \cite{guilloteau2022etude}.

<!---

### The need for regulation

The current submission algorithm of \cigri\ works as a tap.
\cigri\ first submits a batch of \be\ jobs to the cluster scheduler, and then waits for all the submitted jobs to terminate before submitting again.
The size of the batch is defined by the simple ad-hoc algorithm \ref{algo:original}.

\begin{algorithm}
\SetAlgoLined
\SetKwInOut{Input}{Input}
\Input{$rate$ (init. 3),\\ $increase\_factor$ (constant 1.5)
}
\eIf{no running jobs}{
 $rate$ = min($rate$ $\times$ $increase\_factor$, 100)\;
 submit $rate$ jobs\;
} {
  submit 0 job\;
}
 \caption{%
\cigri\ current job submission.
It submits jobs like a tap: opens the tap and submits jobs to \oar.
Then closes the tap and waits for all the jobs to be executed.
Then it opens the tap again and submits jobs.
The values of $rate$ and $increase\_factor$ have been empirically chosen by the administrators of \cigri.
}\label{algo:original}
\end{algorithm}


One can think of scenarios where such a submission algorithm can lead to both the idling of resources or the unnecessary killing of \be\ jobs. 
For example, we can think of a situation where there are a lot of idle resources on the cluster, but the jobs from the previous submission are still running and thus \cigri\ will not submit new jobs.

We believe that the utilization of the system can be improved by introducing feedback mechanism to the submission decision process.
In particular, we think that using an Autonomic Computing \cite{kephart2003vision} point-of-view coupled with tools from \CT\ can yield an improvement in the total utilization of the platform.
--->