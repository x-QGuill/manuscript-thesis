---
lang: fr
---

# L'exemple de l'intergiciel de grille \cigri

Dans cette section, nous présentons un exemple motivant cette étude: \cigri.

\cigri\ est un intergiciel pour grilles de calculs mis en place sur le \meso\ \gricad.
Il se place au-dessus d'un ensemble de grappes gérées par des ordonnanceurs \oar \cite{capit_batch_2005}.
Son but est d'utiliser les ressources libres du \meso\ \gricad.

Les utilisateurs de \cigri\ soumettent à l'intergiciel des applications \BoT.
Ces applications sont composées de nombreuses tâches courtes, indépendantes et ayant des comportements similaires (temps d'exécutions, I/O, etc).
Un exemple d'application \BoT\ sont les simulations \emph{Monte-Carlo} consistant à exécuter un grand nombre d'expériences aléatoires courtes et conclure sur les résultats agrégés.
Ce genre d'applications, aussi caractérisé de \emph{"embarassingly parallel"}, est ainsi propice à la collecte de ressources libres.

\cigri\ va ensuite soumettre ces tâches aux différentes grappes avec la priorité la plus faible (\emph{Best-Effort}).
Cela permet aux ordonnanceurs des grappes de pouvoir arrêter l'exécution des tâches \cigri\ si des utilisateurs plus prioritaires ont besoin des ressources.

Les applications soumises à \cigri\ appartiennent à des \emph{projets} et sont composées de \emph{campagnes}.
Chaque \emph{campagne} comprend un ensemble de tâches à exécuter.

\begin{figure}
    \centering
    \includegraphics[width = 0.45\textwidth]{figs/cigri.png}
    \caption{Interactions entre l'intergiciel \cigri\ et les ordonnanceurs \oar\ des différentes grappes de calculs du \meso\ \gricad.}
    \label{fig:cigri}
\end{figure}

La figure \ref{fig:cigri} résume les interactions entre l'intergiciel \cigri\ et les ordonnanceurs \oar\ des différentes grappes de la grille de calculs.

Dans le cadre de plusieurs travaux \cite{guilloteau:mfc, guilloteau:compas, guilloteau:icstcc, stahl2018towards, yabo2019control}, nous sommes amenés à modifier \cigri, et à tester nos modifications.
Comme nous ne pouvons pas raisonnablement déployer sur l'entièreté du \meso\ \gricad, et que nous voulons garder le comportement réel (et non simulé) de \cigri\ et \oar, nous avons recours à une stratégie de repliement. 

\todo{model de job, pas de CPU, mais des IO}

\newpage