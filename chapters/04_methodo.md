---
lang: fr
---

# Méthodologie

## Objectif

Dans ce papier, nous souhaitons étudier les variations en performances d'une application distribuée utilisant un système de fichiers distribué causées par le repliement du système sur lui-même.

## Dispositif expérimental

Nous avons exécuté les expériences sur la grappe `gros` du site \grid\cite{grid5000} de Nancy.
Les machines de cette grappe ont un processeur Intel Xeon Gold 5220 avec 18 cœurs par CPU, 96 GiB de RAM et un réseau 2 x 25 Gbps (SR‑IOV). 
La reproductibilité de l'environnement déployé est assurée par \nxc\cite{nxc}. 


## Benchmark

L'application choisie est IOR\cite{ior}.
IOR est un benchmark qui mesure les performances des opérations \es.
C'est le benchmark le plus populaire dans le domaine des \es\ en HPC\cite{boito2018checkpoint}, et un des benchmarks utilisé pour la liste IO500\cite{io500}.
Comme IOR est une application MPI, un process MPI représentera, dans la suite, une ressource virtuelle. 
Nous regarderons les performances en écriture, mais aussi en lecture.

IOR considère plusieurs paramètres tels que la taille du fichier à lire ou écrire, la taille de transfert, etc.

- POSIX

- NETWORK

- BLOCK AND STUFF


## Protocole

Nous déployons $N$ nœuds de calculs et un nœud supplémentaire pour le système de fichiers distribué.
En première approche, nous avons choisi d'utiliser NFS.
L'expérience continue comme suit :

1. Calcul du nombre de `slot` par nœud

2. Exécution du benchmark IOR (5 fois)

3. Sauvegarde des résultats d'IOR

4. Suppression d'un nœud de calculs de la liste des `hosts`

5. Retour au point 1 si la liste des `hosts` contient au moins un nœud

Nous activons également l'option `--oversubscribe` d'`mpirun` pour répartir les process sur les nœuds de calculs.


\newpage



