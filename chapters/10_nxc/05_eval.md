## Evaluation {#sec:nxc:eval}

\nxc\ brings reproducibility guarantees to distributed environments contrary to state-of-the-art solutions.
The overall goal of this evaluation section is to determine whether this is done with a significant overhead or not.

### Experimental Setup {#sec:expe_setup}

The following experiments have been carried out on the `dahu` cluster of the \grid\ testbed.
This cluster has machines with 2 Intel Xeon Gold 6130 CPUs with 16 cores per CPU and 192 Gib of memory.
The nodes of this cluster have SSD SATA Samsung MZ7KM240HMHQ0D3 disks with a capacity of 240 GB formatted in ext4.

The experiments conducted in this article are repeatable with variation. Data and analysis scripts are available on Zenodo\footnote{\url{https://zenodo.org/record/6568218}} with the link to the experiments' repository.

### Comparison to \kam\ {#sec:eval_kam}

\grid\ provides base images for several Linux distributions and versions.
Users need to build their own images if they want to use more complete images.
This is usually done with \kam\ on \grid\ --- in fact all the images provided by \grid\ are generated by \kam\ recipes.

In this study, we want to compare the performance of \nxc\ and \kam\ to build images.
We will focus on the image build time, as well as the size of the generated images.
We also want to evaluate whether caching the \store\ enables an interesting build time speedup.

##### Protocol

The following steps are executed in this order.

1. Construction. Build an image from a recipe.

2. Modification. Change the recipe slightly.

3. Reconstruction. Build an image from the new recipe.

We first build a \texttt{base} image with \nxc\ and \kam, measuring its build time and the size of the generated images.
\texttt{base} contains the basic software needed to conduct a distributed experiment: \texttt{grid5000/debian11-x64-nfs} for \kam\ as this is the most convenient and common image for distributed experiments on \grid, and all the packages required by the flavour for \nxc.
Then we add the \texttt{hello} package to the recipes and build a new image (\texttt{base + hello}) while measuring the same metrics.

This experiment has been executed using \grid's NFS (mounted on \texttt{/home}) or without it (using local disks on machines mounted on \texttt{/tmp}), in order to compare the performance of the tools depending on the filesystem setup used.

We clear the \store\ before building the \texttt{base} image, but not before building \texttt{base + hello}, in order to evaluate the impact of cached builds on \nxc.
\kam\ has an indirect caching mechanism via the HTTP proxy Polipo \cite{polipo}.
However, we did not manage to make it work with \kam, and Polipo is no longer maintained as its utility has become arguable -- most of today's traffic is encrypted, including fetching packages from mirrors.

##### Results and Comments

\begin{figure*}
    \centering
    \includegraphics[width = 0.99\textwidth]{./figs/eval_nxc_kameleon.pdf}
    \caption{Performance comparison between \kam\ and \nxc\ (\texttt{nxc}). A \texttt{base} image is first built, then a new image (\texttt{base + hello}) that contains the additional \texttt{hello} package is built. As \grid\ is the targeted platform of this experiment, images are built with the \texttt{g5k-ramdisk} and \texttt{g5k-image} flavours. Shown values are averages over 5 repetitions. Error bars represent 99 \% confidence intervals.}\label{fig:compa_kam}
\end{figure*}

As seen on Figure \ref{fig:compa_kam}, \nxc\ substantially outperforms \kam\ in terms of image build time.
When building from an empty cache on local disks, \nxc\ is 11x faster than \kam.
Moreover, \nxc\ uses its local cache efficiently, which enables it to build the image variation 1.7x faster than the initial image build time when the filesystem is used efficiently (local disks).

Figure \ref{fig:compa_kam} also shows that \nxc\ produces bigger images than \kam.
This is mainly because we have not optimized the content size of the images as we write these lines --- \eg\ many firmwares are kept in the images instead of only the ones needed on \grid\ (see Figure \ref{fig:plot_melissa}).
Another reason for this image size comes from our design choice to prioritize compression speed over compression quality.
This is important for \nxc\ as image variations should be built as fast as possible to improve user experience.
For information, we have measured that \nxc\ takes about 25 s to compress each image, which is a non-negligible portion of a variation build time (30 - 35 %).

Finally, Figure \ref{fig:compa_kam} shows how the filesystem setup impacts the build time.
Here, using an efficient filesystem setup (local disks) greatly benefits to \nxc\ as it makes it 4x faster.
This is caused by the many smalls writes done by \nix\ in the \store.
Filesystem setup has little impact on \kam\ as it uses local disks by default to generate the whole image, that is later on copied to the NFS.

\begin{lesson}{}{}
\nxc\ builds system images faster than \kam, but really shines when introducing a variation in the environment.
\nix\ is victim of the NFS slowness, and build times can be even more improved when not building on NFS.
\end{lesson}

### Comparison to \enos

\enos\ is a state-of-the-art solution to conduct distributed experiments.
\enos\ does not claim to be reproducible, but it inherits the \repro\ of its underlying components.
In this section, our goal is to compare the performance of \nxc\ and \enos\ to set up *fully reproducible* distributed environments.
In particular, we want to know how much time is taken for each phase of a deployment.

##### Case Studies

We chose to study the two following distributed applications that are already implemented in \enos:

- *k3s* \cite{k3s} is a lightweight version of Kubernetes.

- *flent* \cite{flent} is a network benchmarking tool.

Our *k3s* environment consists in two nodes: one *k3s* server and one *k3s* agent.
The agent deploys a `nginx` web server to the agents.
The *run* part of the experiment simply consists in querying the webserver to retrieve the `nginx` web page.

Our *flent* environment consists in 2 nodes: one server and one client.
The *run* part of the experiment simply runs the *flent* benchmark.

For both case studies, we made sure that \nxc\ and \enos\ set up similar environments, and that they execute the same *run* part of the experiment after the deployment and provisioning phases have been done.

##### Protocol

\enos's approach is mostly based on the configuring/provisioning phase, which executes commands to set up a desired environment (software, services...) on a machine that is already available.
As this limits \repro\ on the kernel side, we decided to deploy traceable images on the nodes.
We used the \texttt{grid5000/debian11-x64-nfs} image that is pre-built by \grid.
\enos\ deploys the image with \kad, then executes the provisioning phase,
and then execute the *run* part of the experiment once the environment is ready.

In \nxc\ most of the configuration is done inside the composition and thus in the image.
This enables us to completely skip the provisioning phase for \emph{flent}.
For \emph{k3s}, our provisioning phase simply consists in waiting that the web server becomes available.

For both tools we measure the time to build the image, to deploy it, to execute the provisioning and to run the experiment script.
We use an empty \store\ for a fair build time.

##### Results and Comments

\begin{figure*}
    \centering
    \includegraphics[width = 0.99\textwidth]{./figs/eval_nxc_enoslib.pdf}
    \caption{Time spent in the different phases of the deployment of a distributed experiment (build, submission + deploy, provisioning, run). We compare \enos\ and \nxc\ (with the flavours \texttt{g5k-ramdisk} and \texttt{g5k-image}) on two examples: a network benchmarking tool (\emph{flent}) and a containers' orchestrator (\emph{k3s}). The errors bars represent the confidence intervals at 99 \%.}\label{fig:compa_enos}
\end{figure*}

As seen on Figure \ref{fig:compa_enos}, \nxc's `g5k-ramdisk` flavour is faster to deploy than a full image as it uses `kexec` and does not require a full reboot.
As expected, we also can see that the solutions with \nxc\ have a smaller provisioning time than with \enos.
This is because \nxc\ includes this as part of this provisioning in the build and deployment phase.

Please note that \enos\ does not directly provide the time taken by the deployment phase, but only provides the time between the submission and the first command of the provisioning.
That is why the submission and deployment time (Sub + Deploy) are shown together on Figure \ref{fig:compa_enos} both for \enos\ and \nxc, for the sake of fairness.

From Figure \ref{fig:compa_enos}, it seems that packing part of the provisioning in the image improves the provisioning time without deteriorating the deployment time.
The only drawbacks are the non-negligible build times.
However, those times can be improved by utilizing the \store\ as a local cache (see Section \ref{sec:eval_kam}).
Note that \nxc\ proposes several flavours that can be executed locally to develop and test the environment.
The cost of the construction of these images is thus amortized by the numerous quick and light local deployments.
Finally, please also note that the build time of \enos\ is null on Figure \ref{fig:compa_enos} as a pre-built image is used.
However, depending on their scenario users may need to actually build an image via another technology (\eg \kam\ or \nxc).

\begin{lesson}{}{}
Using \nxc\ instead of \enos\ can reduce the time spent after deployment to configure services (\emph{provisioning}) to the build time thanks to the declarative nature of \nixos\ services.
\end{lesson}
