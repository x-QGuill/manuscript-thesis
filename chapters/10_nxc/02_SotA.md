<!---
## State of the Art {#sec:sota}

Several terminologies exist on the reproducibility of experiments.
In this article we use the terms defined by Feitelson in \cite{feitelson_repeatability_2015}.
In particular, an experiment is said to be \emph{repeatable} if one can rerun it and obtain the same results by using the original artifacts (scripts and configuration files needed to run the experiment).
If the same can be achieved without using the original artifacts but by recreating them, the experiment is said to be \emph{replicable}.
\emph{Variation} is an orthogonal concept that can be applied to repeat or replicate an experiment with a modification of some parameters.
Mercier et al. emphasize in \cite{mercier2018considering} that the \emph{production} environment (the final one used to execute the experiment) is not enough to achieve variation in an experiment, as the \emph{development} environments contain valuable information for the experiment and are also required.

### Reproducibility of a local software environment

Several approaches exist to encapsulate a software environment.
A first solution is to provide virtual machine (VM) images or containers of the environment.
VMs are portable but induce a virtualization cost at runtime which hinders some experiments.
Containers are much more lightweight but limit the possible usages (\eg Docker containers do not encapsulate the version/configuration of the Linux kernel, while this may impact how software behaves).
Building VMs and containers in a reproducible fashion can be hard,
but tools like Packer \cite{packer} can recreate identical virtualized images (either virtual machines or containers) from a unique description.

Another approach is to rely on the properties of package managers.
Spack \cite{gamblin_spack_2015} is a package manager used on HPC platforms to share software environments.
Spack does not control all the dependencies by design as it can reuse software already present and configured on the system, which is detrimental for \repro.
Courtes et al. \cite{courtes_functional_2013} presents purely functional package managers (\nix\ \cite{dolstra_nix_2004}, \emph{Guix} \cite{courtes_reproducible_2015}) as good candidates to share complex software environments between users.
Both approaches are very similar but \nix\ has a more complete ecosystem as we write these lines, so we only consider \nix\ in the remainder of this article.

Each \nix\ package is defined as a function without side effects,
which means \nix\ should produce the exact same package if the function inputs are the same (source code version, build options, build and runtime dependencies and their versions, build toolchain and their versions, target architecture).
A software environment is defined as a list of \nix\ packages.
As \nix\ can reproduce each package, the entire environment is reproducible.
This partially solves the well known issue of \emph{Dependency Hell} as \nix\ demands to explicit every dependency and their version to build the package.
\nix\ is limited to user-space environments, but the environment on the kernel side matters as well.
The \nixos\ Linux distribution, based on \nix, solves this issue by describing the entire operating system (\ie\ kernel, softwares, configuration files, services) with a descriptive \nix\ expression.
The descriptive approach taken by \nixos\ greatly simplifies the work of a reproducer wanting to perform variations of the environment \cite{mercier2018considering}.

Perenniality of environments can be hard to achieve on methods based on the reconstruction of a target software and every of their dependencies, as some source code can disappear over time.
To solve this issue, reconstruction approaches can use source code from initiatives such as Software Heritage \cite{softwareheritage}, whose goal is to collect, preserve and share publicly available software in source code form.

### Reproducibility of a distributed software environment

Achieving \repro\ in a distributed setting brings new challenges,
as the environments must be deployed on all the involved machines.
A common approach is to somehow generate full system \emph{images}, then to deploy them on the nodes with tools such as \kad\ \cite{georgiou2006tool}.
Creating such images manually can be cumbersome.
Tools such as \kam\ \cite{ruiz_reconstructable_2015} take an \emph{imperative} approach to build images, as they execute a pipeline of scripts to create images that contain the desired environment.
Such images are not enough to achieve \repro, as scripts must often be executed after the image has been deployed to set up relations between nodes (\eg\ to mount a custom NFS server among the deployed nodes).
These scripts are often written manually with bash or Python, and sometimes use helper libraries such as \emph{Execo} \cite{execo}.
They may also be only valid for a single target platform and are often fragile and difficult to maintain.
One solution to avoid this scripted configuration phase would be to encapsulate part of it in the images themselves.

The development phase of the images for a distributed environment is an iterative and time-consuming process.
Reducing the duration of the development cycles goes towards improving the user experience, and we think that this is very important to reduce the friction to adopt reproducible experimental practices.
Tools such as \enos\ \cite{cherrueau_enoslib_2022} or Vagrant \cite{vagrant} go in this direction as they enable users to describe the configuration of the environment while abstracting the target platform.
\enos\ takes an \emph{imperative} approach by defining the environment with a pipeline of scripts that describes the provisioning phase and is executed at the beginning of an experiment to set up the desired software environment.
The target abstraction makes it easier to test the environment locally before deploying it at full scale.
However, \repro\ of the software stack is not a focus for these solutions, and they mostly just inherit \repro\ properties of the underlying platform and technologies.
Typically, one probably needs to combine \enos\ with tools such as \kam\ to make sure the kernel is reproducible, and the end user would be responsible for the \repro\ of the provisioning scripts.

Projects such as \emph{NixOps} \cite{nixops}, \emph{deploy-rs} \cite{deploy-rs} or \emph{Disnix(OS)} \cite{disnix, disnixos} enable to activate new \nixos\ configuration on target machines, but they are limited to machines that already run \nixos.
These technologies only change the running configuration on the machines without rebooting them, which keeps a state on the machines and is detrimental for \repro.

### Research Gap, Positioning

\begin{figure}
    \centering
    \includegraphics[width = 0.9\textwidth]{./figs/state.pdf}
    \caption{Motivation of \nxc. Currently, to produce a reproducible environment for each platform, users must maintain a description file for each target platform. We want \nxc\ to only use a \textbf{single description} file (called a composition) that can build reproducible distributed environments and deploy them to \textbf{several platforms}.}\label{fig:nxc_motiv}
\end{figure}

We believe that there is a necessity to propose a solution to \textbf{deploy reproducible environments} for a distributed system with \textbf{fast development cycles}.
Figure \ref{fig:nxc_motiv} summarizes the motivation that has led us to create \nxc.
To generate reproducible environments with the current solutions, users most often need a configuration file for every platform they target.
\nxc\ aims at having a \textbf{single description} of the distributed environment that can be deployed to \textbf{several platforms}.
\nxc\ relies on \nix\ and \nixos\ and thus inherits their properties to make the environment \textbf{completely reproducible}.
We fully embraced the \emph{descriptive} approach for \repro reasons and decided to do most of the configuration at the image build time.
Some part of the configuration must however still be done at runtime via a provisioning phase, typically to synchronize services between different machines.
--->
