## Conclusion {#sec:conclu}

<!---
### Discussion
--->

This Chapter has presented \nxc, a free tool, under MIT license\ \cite{nxc_sw}, that enables the generation of reproducible distributed environments.
We have showed that \nxc\ deploys the exact same software stack on various platforms of different natures, without requiring specific work from users.
The software stack is reconstructible by design, as \nxc\ inherits its \repro\ properties from \nix\ and \nixos.
Our experiments showed that \nxc's \repro\ and platform versatility properties are achieved without deployment performance overhead in comparison to the existing solutions \kam\ and \enos.

\nxc\ enables to build and deploy reproducible distributed environments.
This is crucial for conducting reproducible distributed experiments, but this is only a part of the bigger picture.
We plan to explore how \nxc\ can be coupled to other tools that solve other parts of this problem.
\enos\ is for example well-suited to control the dynamic part of complex distributed experiments but lacks \repro\ properties, which makes us think that a well-designed coupling may be beneficial for practitioners.

The experiments conducted in this article showed that build caches greatly improves \nxc's build times, and that properly using the file system is important for its performance.
From a cluster administration perspective, providing a shared \store\ between users would be very interesting to avoid data duplication and to prevent different \nxc\ users to build the same packages over and over.
There are many ways to implement a distributed shared \store\ and we think that exploring their trade-offs would provide valuable insights, as \repro\ improvements should not be done at the cost of a higher resource waste on clusters.

User experience is a crucial factor that must be considered for reproducible experimental practices becoming the standard.
With this in mind, we think that the notion of \transpo\ we have defined in this article and implemented in \nxc\ is very beneficial.
\transpo\ reduces the development time of distributed environments, as it enables users to do most of the trial-and-error parts of this iterative process with fast cycles, without any \repro\ penalty on real-scale deployments.
However, practitioners that adopt \nxc\ are likely to experience a paradigm shift if they are not already accustomed to \nix's approach.
We strongly believe that the \repro\ and serenity gains it brings are worth it.
To help with the adoption of \nxc\ a tutorial has been created and presented at two occasions\ \cite{tuto_nxc}. 

<!---
### Perspectives
--->

\nxc\ currently only provides first-class support for \grid.
We would like to support bare-metal and virtualized deployments on other experimental testbeds such as CloudLab \cite{cloudlab} and Chameleon \cite{chameleon}.
Moreover, the hand off from \grid\ to SLICES(-FR) might require to also support deployments technologies such as OpenStack \cite{openstack2013openstack}.

<!---
As for now, \nxc\ does not provide its own engine to conduct experiments, and this is not our desire.
We developed an integration of \nxc\ into the Execo engine \cite{imbert_execo}.
Discussions have been started to support deployments with \nxc\ in \enos.
This integration would benefit \nxc\ with the large collection of tools from \enos, and would also benefit \enos\ with the integration of fully reproducible environments.
--->
<!---
More recently, we added support in \nxc\ to deploy \nixos\ images with `kexec` which `/nix/store` are located on the shared NFS of \grid.
This reduces the size of the ramdisk image and increases deployment speed.
But it also raised interesting new questions.
As the systems are booting up, the different systemd services are starting, and as the `/nix/store` is located on the NFS, a **lot** of syscalls are being made to resolve the dynamic libraries of the services.
This problem has been called the *stat storm* (because of the `stat` syscall).
The blog post \cite{stat_storm} gives a very nice presentation of the issue.
The hierarchy of the `/nix/store` catalyzes this problem by the explosion of the combination exploration required to resolve the dependencies.
Variations of this problem have starting to be addressed in the literature \cite{zakaria2022mapping, shaffer2017taming, frings2013massively}, but are mostly focused on the loading of user code and often require to modify it. 
During the Master 1 internship of Samuel Brun \cite{brun}, we started investigating a solution for \nxc\ based on FUSE \cite{szeredi2010fuse}, where the nodes loading the services will cache the result of the `openat` and `stat` syscalls from the NFS and share the results by multicast to the other nodes of the deployment.
--->
<!---
When deploying for \cigri\ experiments for example, we do not deploy a 1-to-1 scale cluster, but instead, we define several computing resources (from the point-of-view of \oar) on a single machine.
This allows us to reduce the experimental cost while keeping the same production software stack.
To increase the realism of this deployment, \nxc\ could deploy several virtual machines containing the environment of a computing node onto one or several physical nodes.
This would allow for isolation between computing resources while reducing the number of physical nodes to perform an experiment.
--->
