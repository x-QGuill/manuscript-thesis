## Introduction

In this chapter, we tackle **RQ1**.
We aim to make the **entire** software stack involved in a experiment of distributed systems reproducible.
This implies making the compilation and the deployment of this stack reproducible, allowing one to rerun the experiment on an identical environment in one week or ten years.

To reach this goal, we exploit the declarative approach for system configuration provided by the \nixos\ \cite{nixos_2008} Linux distribution.
\nixos\ makes use of a configuration that describes the environment of the entire system, from user-space to the kernel.
The distribution is itself based on the purely functional \nix\ package manager \cite{dolstra_nix_2004}.
The definition of packages (or system configuration in the case of \nixos) are functions without side effects,
which enables \nix\ and \nixos\ to reproduce the exact same software when the same inputs are given.

We extend this notion of system configuration for distributed systems in a new tool named \nxc.
\nxc\ enables to define distributed environments and to deploy them on various platforms that can either be physical (\eg on the \grid\ testbed \cite{grid5000}) or virtualized (Docker or QEMU \cite{qemu}).
\nxc\ exposes the **exact same user interface** to define and deploy environments regardless of the targeted platform.
We think that this functionality paired with fast rebuild time of environments improves user experience,
and we hope that it will help the adoption of experimental practices that foster \repro.

Projects such as \emph{NixOps} \cite{nixops}, \emph{deploy-rs} \cite{deploy-rs} or \emph{Disnix(OS)} \cite{disnix, disnixos} enable to activate new \nixos\ configuration on target machines, but they are limited to machines that already run \nixos.
These technologies only change the running configuration on the machines without rebooting them, which keeps a state on the machines and is detrimental for \repro.

\begin{figure}
    \centering
    \includegraphics[width = 0.9\textwidth]{./figs/state.pdf}
    \caption{Motivation of \nxc. Currently, to produce a reproducible environment for each platform, users must maintain a description file for each target platform. We want \nxc\ to only use a \textbf{single description} file (called a composition) that can build reproducible distributed environments and deploy them to \textbf{several platforms}.}\label{fig:nxc_motiv}
\end{figure}

\begin{lesson}{}{}
State-of-the-art solutions to deploy software environements are either not focused on their reproducibility, or require long development cycles, which encourages bad reproducibility practices. 
\end{lesson}

We believe that there is a necessity to propose a solution to \textbf{deploy reproducible environments} for a distributed system with \textbf{fast development cycles}.
Figure \ref{fig:nxc_motiv} summarizes the motivation that has led us to create \nxc.
To generate reproducible environments with the current solutions, users most often need a configuration file for every platform they target.
\nxc\ aims at having a \textbf{single description} of the distributed environment that can be deployed to \textbf{several platforms}.
\nxc\ relies on \nix\ and \nixos\ and thus inherits their properties to make the environment \textbf{completely reproducible}.
We fully embraced the \emph{descriptive} approach for \repro reasons and decided to do most of the configuration at the image build time.
Some part of the configuration must however still be done at runtime via a provisioning phase, typically to synchronize services between different machines.

The chapter is structured as follows.
Section \ref{sec:pres_nxc} presents \nxc, its main concepts, the external concepts it relies on, and the users' utilization workflow for setting up a complete reproducible system and software stack.
Section \ref{sec:how_it_works} gives technical details on how \nxc\ works.
Section \ref{sec:melissa} presents how \nxc\ can be used on a complex example that combines several distributed middlewares.
Section \ref{sec:nxc:eval} offers experimental performance results of \nxc\ against standard state-of-the-art tools using multiple metrics.
Finally, Section \ref{sec:conclu} concludes the section with final remarks and perspectives on reproducible works and environments.
