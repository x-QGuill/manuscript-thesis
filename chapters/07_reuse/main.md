# Reusability of Autonomic Controllers for HPC Systems {#chap:reuse}

This chapter is based on work submitted in ACM TAAS with Raphaël Bleuse, Sophie Cerf, Bogdan Robu, Rosa Pagano, and Eric Rutten.

<!---
## The need for reusability
--->

The controller defined in the previous chapter is tightly coupled with its underlying system.
This is due to the identification phase that creates a model of the system.
Implementing such a controller requires a control-theory background.
However, HPC system administrators are not control theory experts.
And they cannot develop *on their own* a new controller for every new cluster, file-systems, disks, network, in their platform.
To help with the adoption of control-theory based solutions, and with software engineering concerns in mind, we explore in this Chapter, the reusability of autonomic controllers using control-theory techniques with the example of \cigri\ as a study case.
In the following sections, we present the implementation of controllers of different nature (PI, adaptive PI, and Model-Free Control) to regulate the same system presented in Chapter \ref{chap:harvest_fs}, and propose a comparative framework to conclude on their *reusability*.
We further discuss their interdependence and known trade-offs, from a theoretical point-of-view. 

<!----
We select different controllers from the literature and implement them in \cigri.

##### Proportional-Integral Controller \cite{astrom}

The design of this controller has been described in the previous Chapter.
PI controllers are model-based, and thus require an identification phase, but yields runtime guarentees.
There exist methods to tune a PI controller without using a model of the system, but such approach fail to provide the same runtime guarentees than the academic protocoles and methodologies.

##### Model-Free Controller \cite{fliess2013model}

As its name suggests, the Model-Free Controller (MFC) does not require any model, and thus does not require an heavy identification phase.
The MFC is able to control a system by just having a knowledge of the orders of magnitudes of the inputs and the outputs.
This last point is important point as it allows HPC systems administrators to implement, or to at least define a MFC with their knowledge of their platform.
The drawback of not relying on a model is the lack of guarentees on the closed-loop system.
The MFC also offers an adaptive component which allows the controller to perform "better" than more rigid controllers (such as the PI), in some extreme cases.
\todo{berk...}

##### Adaptive Proportional-Integral Controller

\todo{}

Several aspects should be considered when comparing controllers reusability: from the design phase and setup, to usage and possibly portability.
When reusing a controller, one may consider reuse without modification, reuse with different parameter values, or a more-involved re-design of all or part of the controller.

--->



## Criteria for comparison {#sec:criteria}

The controllers are compared based on five criteria, each answering a specific design or evaluation problematic:

- **Nominal Performance**: To what extent, and how, does the controlled system meet its objectives, *in the experimental conditions in which the controller has been designed*?

- **Portability**: How will the controller behave *if the system varies from the nominal version considered for design*? What set of systems can the controller manage?

- **Setup complexity**: To what extent is the controller "plug and play"? How easy is it to set up the controller?

- **Support Guarantees**: Is the controller design backed by a methodology bringing behavioral guarantees? Under which hypothesis do the guarantees hold?

- **Competence required**: How knowledgeable one needs to be to soundly design the controller?

\begin{lesson}{}{}
We propose 5 criteria to compare controllers: their \emph{nominal performance}, \emph{portability}, \emph{setup complexity}, as well as their \emph{guarantees} and the \emph{competence} they require.
\end{lesson}

The next sections define in details each criterion, discuss their relevance, and propose metrics to characterize them.


### Nominal Performance



##### Definition

Nominal performance characterizes the ability of the system to perform its adaptation task.
Data- and model-based controllers are inherently dependent on the configuration and experimental conditions in which the design was done.
Usually, performance of an autonomous system is evaluated in similar conditions, what we call \emph{nominal} system.
Nominal performance covers both aspects of transient, dynamical behavior, and converged, stabilized behavior.

<!---
Quantitative metrics of nominal performance are mostly used when controllers (or controllers' tuning) need to be compared.
Note that some thresholds can exist, beyond which the adaptation is no longer considered valid.
This is the case, for example, during a reconfiguration phase.
--->

##### Metrics

Classical control theory provide three metrics to characterize the dynamical behavior of a controlled system.
Note that stability is eluded here, as we suppose all controllers are functional.
\textbf{Precision} reflects the ability to reach the desired target.
It is measured using the \emph{static error} metric, giving the distance between the measured performance and its reference value, once the system converged.
0 is the best value: the smaller, the better.
\textbf{Rapidity} characterizes the speed of convergence of the controlled system.
It is measured using the \emph{response time} metric, computed as the time needed from the performance signal to reach 95\% of its final value.
The smaller, the better.
\textbf{Quality} of the signal is also considered in cases there are oscillations in the performance signal.
It can be measured via the \emph{overshoot} metric, the relative value of the highest peak of performance signal.
0 is the best value: the smaller, the better.


### Portability

##### Definition

When used in real conditions, the controller may have to manage a system that does not correspond to the nominal situation.
Portability captures the ability of a controller to be applied,
without or with limited modification,
to a system different from the nominal design one, and to keep adequate performance.
Two cases can be distinguished for the portability conditions.
First, the variation occurs at runtime, \ie\ the system deviates from the nominal version, possibly due to some disturbances or external events.
The ability of a controller to keep good performance in case of runtime variations of the system is to be linked with the well studied property in Control Theory of robustness.
Robust controllers are able to reject disturbances, \ie\ ensuring that the effect of the disturbance on the system remains limited and mastered.
Second, the controller may be applied to a system with different configurations, \ie\ variations occur offline, on different runs.
Such software or hardware configuration changes can be significant, for instance when different clusters are considered.
This property is rarely considered in Control Theory for physical system, where the notion of different \emph{runs} does not exist: controllers continuously monitor the same physical system.

<!---
Note that the nominal system may be different from the expected system on which the adaptation is to be performed.
Indeed, it is not always possible or desirable to precisely characterize or model the real systems' conditions.
For instance, the analysis on the duration of Best Effort jobs of a campaign is a difficult task (Chapter \ref{chap:cigri_jobs} and \cite{guilloteau2022etude}), it is not realistic to suppose that it should be done for each campaign.
--->

##### Metrics

Performance metrics of precision, rapidity, and quality can be used to analyze the controlled system.
Different systems can be tested, by varying the configurations and injecting disturbances, when possible.
The distance between the current system and the nominal system can be characterized, to create a set of systems that the controller can handle.
Portability is then the performance achieved by a controller for several configurations and disturbances.


### Setup complexity

##### Definition

Controllers are implemented and used by system experts, that are not a priori knowledgeable in Control Theory.
A controller with low design complexity is more likely to be understood, correctly implemented, and tuned.
If performances are acceptable, the simplest controller may be the best option.
Note that design complexity is different from the mathematical complexity of the controller algorithm, whereas rather addresses the user experience aspect.

##### Metrics

There is no commonly agreed and general metric for design complexity.
We propose to use as a metric the number of parameters, or initial conditions, that have to be tuned in the controller algorithm.
While this metric only covers part of the difficulties a user can experience in developing a controller,
we believe that it gives a meaningful quantitative metric that can be applied to any algorithm.


### Support Guarantees

##### Definition

One advantage of Control Theory-based adaptation is regarding the mathematical guarantees one can have that the controlled system will perform as desired.
Guarantees are not a binary property: there are nuances beyond having or not having guarantees.
First, a methodology is needed for the controller tuning.
Then, the performance properties that are ensured come with some conditions, depending on the system's variations or on appropriate system excitation.
There exist a gradation of guarantees: from safety-critical system requiring strong guarantees, system subject to SLAs, to best-effort system without the need of guarantees.

##### Metrics

Measuring guarantees level to compare controllers is not an easy task, there is no agreed-upon metric to characterize this criterion.
We opt out for a qualitative evaluation of guarantees: the availability of a mathematically-backed methodology for controller tuning, and the hypothesis and conditions that need to be fulfilled to have those guarantees.


### Competence required

##### Definition

Designing and tuning a controller is not straightforward.
PID controllers are known for being fairly accessible, resulting in their predominance in the industrial applications.
However, the proper design of a PID controller may require control expertise: as it is the case to perform sound identification experiments and tune accordingly the PID parameters.
Advanced techniques such as optimal control, model predictive control, or robust control require even more domain-specific knowledge.
The need for a control expert leads to additional costs and delays, that may orient systems' experts toward simpler solutions.

##### Metrics

Evaluating the competence required for designing and using a controller will also be done qualitatively.
It can be by evaluating the level of education/knowledge required to design the controller, or the amount of available bibliography on the controller.
Such metric reflects the complexity of the design process, where the need for complementary actions such as feedforward, anti-windup, preventing bursting, etc., have to be investigated.
The metric also covers the domain-specific knowledge needed for fine-tuning and to properly understand the achieved guarantees.


### Theoretical trade-offs between criteria

No controller is expected to outperform the others on all criteria.
Control Theory highlights the inherent discrepancies between nominal performances and reusability.
Design complexity and guarantees are also hardly compatible.
Only a trade-off can be achieved.
Then, strategies for concealing multi-criteria have to be applied, such as setting priorities, weighting the criteria, or finding Pareto-optimal sets.
Such choice of priorities or weights is not straightforward, and can largely differ depending on the application context and objectives.
This work focuses on this challenge by evaluating various controllers on complementary criteria.
The objective is to highlight which controller to chose, given one's priorities.

In the following, we discuss two important trade-offs between pairs of criteria.


##### Trading nominal performance for reusability

Conciliating optimality and robustness is a primary concern in the control field.
Having highly performing controllers on the nominal system may come at the cost of limited performances, or even instability, in case the actual system differs significantly. 
Relaxing constraints on precision, rapidity, and quality can allow for the reusability of the controller on a large class of systems.


##### Trading support guarantees for low setup complexity and limited control competences

Strong guarantees on the behavior of the controlled system over a wide variety of conditions can be achieved, however it often requires using advanced control techniques.
Properly designing such controllers assumed significant control engineering knowledge, and its implementation and usage are consequently more complex. 
This discussion on the simplicity of the controller usage, even at the cost of loss of performance guarantees, is very common in the control field.
However, it reflects the reality of systems' experts when using controllers in practice. 




## Considered Controllers

This Section presents the design of the controllers considered in this study of reusability: a PI controller (Section \ref{sec:pi}), an adaptive PI (Section \ref{sec:api}), and a Model-Free controller (Section \ref{sec:mfc}).
Sections \ref{sec:api} and \ref{sec:mfc} summarize, respectively, the work done during the Master 2 internship of Rosa Pagano \cite{pagano}, and \cite{guilloteau:mfc} published at CCTA 2022 with Bogdan Robu, Cédric Join, Michel Fliess, Eric Rutten, and Olivier Richard.


### Proportional-Integral Controller {#sec:pi}

For the PI controller, we reuse the controller designed in Chapter \ref{chap:harvest_fs}.
To summarize, we have the following model of our system $y(k+1) = a y(k) + b u(k)$

##### Tuning

We have two parameters to choose the closed-loop behavior of our system: $k_s$ and $M_p$.
From those parameters and the model ($a$ and $b$), we find the gains $K_p$ and $K_i$ of the PI controller.

### Adaptive PI {#sec:api}

The PI controller defined above is coupled to one kind of jobs (execution time and \io\ load).
An adaptive controller, however, would be able to *adapt* its configuration at runtime to fit the jobs running even if they are different from the one used for the design of the controller.
In our case, the parameter $b$ of our model depends on the \io\ load of the jobs ($f$), see Figure \ref{fig:pi:identification:patch} and Equation \ref{eq:model_gains}, which thus motivates the use of adaptive control.

<!---
Adaptive control encompasses a set of techniques that dynamically adjust controller parameters in real-time to maintain a desired level of control system performance \cite{LandauID2011Adaptive}.
--->
We build adaptation on top of the previously defined PI controller, and the adaptive algorithm adjusts its parameters to account for the unmodeled dynamics beyond the scope of the linear model \cite{shevtsov2017control}.
<!---
In the case of a Single Input Single Output (SISO) system, an adaptive Proportional-Integral (PI) controller is frequently employed.
The PI controller is initially designed based on the model of \cref{eq:linear-model}, 
--->

There are two primary approaches for the adaptation: updating the parameters of the controller (*direct*), or updating the parameters of the model (*indirect*).
In our case, the indirect approach appears more suitable because of the dependence of the previous PI controller on the $b$ parameter of the model.
We will thus, at each iteration of the control loop, estimate the value of $b(k)$ of our model, noted $\hat{b}(k)$, and recompute the value of the gains ($K_p$ and $K_i$) accordingly.

#### Controller design

Our objective is to estimate $\hat{b}(k)$ using an online algorithm.
We define the prediction error $\varepsilon$ representing how far the estimated model is from the reality measured:

\begin{equation}
\label{eq:residual}
	\varepsilon(k)=y(k)-a y(k-1)-\hat{b}(k-1)u(k-1),
\end{equation}

Due to the presence of significant noise in the measured data, see for instance \Cref{fig:pi:identification:patch}, the algorithm should be capable to mitigate noise.
Therefore, we use a modified Recursive Least Square (RLS) \cite{AstromKJ2008Adaptive} algorithm to improve the robustness against noise.
That is, we substitute $\varepsilon$ with its filtered version $\Phi(\varepsilon(k))$, given by:

\begin{equation}
	\Phi(\varepsilon(k))=\frac{\varepsilon(k)}{1+\phi |\varepsilon(k)|}
\end{equation}

where $\phi \geq 0$ represents a smoothing parameter to be chosen.
By doing so, we preserve a linear structure when the error is small, while introducing a slower-than-linear behavior for larger errors. 
Eventually, we define our robust estimator as:

\begin{equation}
	\hat{b}(k+1)=\hat{b}(k) + V(k+1)\times{u(k)\times{\Phi(\varepsilon(k+1))}}
\end{equation}

With $V(k+1)$:

\begin{equation}
	V(k+1)=\frac{V(k)}{\mu}-\frac{V(k)u(k)^{2}V(k)}{1+u(k)V(k)u(k)}\cdot\frac{1}{\mu^2}
\end{equation}

where $\mu$ represents the forgetting factor.
The presence of changing parameters over time, such as $b$ varying with different campaigns, requires the use of forgetting effect of $\mu \in (0,1]$.
However, the $\mu$ parameter introduces a significant drawback known as the "bursting phenomena"\ \cite{AndersonB1985Adaptive,FortescueT1981Implementation}: when the system lacks sufficient excitation, the algorithm may approach a singularity, resulting in large spikes in the estimated parameter.
<!---
To mitigate this behavior, we employ the parallel estimation technique that consists in using a vector of forgetting factors $\mu$, rather than a unique value.
Thus, the algorithm can estimate N distinct values of parameter b.
The estimation errors (\cref{eq:residual}) are evaluated for each prediction, and the value of $\hat{b}$ achieving the smallest error is selected.
Then, at each time step, once $\hat{b}$ is selected, we use \cref{eq:kpki} to compute the new controllers parameters and \cref{eq:PI} to choose the control action.
--->

#### Tuning

The algorithm relies on different parameters: the forgetting factor $\mu$, the smoothing parameter $\phi$, and the initial conditions $\hat{b}(0)$ and $V(0)$.
Note also that the adaptive PI is built on top of the PI, which requires the computation of the model parameter $a$ and the definition of $k_s$ and $M_p$, two quantities that allow choosing for the close-loop behavior (see \cref{fig:ksMp}).

##### Forgetting factor $\mu$

A small $\mu$ results in a fast, but noise-sensitive, response.
Using a Parallel Estimation \cite{AstromKJ2008Adaptive,KowalczukZ1992Competitive} allows us to try various values of $\mu$ and pick the one returning the less error.

##### Smoothing parameter $\phi$

Strong smoothing leads to slow estimation, but also reduces overshoots and oscillations.
There are no methodology to define $\phi$, and we chose a satisfactory value via an experimental approach.

##### Initial conditions

There are two distinct initial conditions: $\hat{b}(0)$ and $V(0)$.
For $\hat{b}(0)$, a value of 0.5 was chosen.
Small values, \ie\ underestimations, of $\hat{b}(0)$ might lead to overshooting due the relation between $u$ and $b$.
The significance of $V(0)$ lies in its impact on the algorithm's initial speed.
Both initial conditions were chosen from experiments results.


### Model-Free Controller {#sec:mfc}

The third type of controller we will compare is one that does not rely on a model.
Many choices exist such as Active Disturbance Rejection Control\ \cite{HanJ2009From}, Model-Free Control\ \cite{fliess2013model}, or the ones using learning techniques (\eg\ \cite{PongV2020Temporal}), each of them with several variants and improvements.


Model-Free control (MFC) is an approach that has the advantage of not necessitating the possibly tedious phase of modeling.
The main ideas and formulations are summarized in the following of this section.

The MFC formulation relies on an \emph{ultra-local model}, but does not require building a reliable global model.
In that sense, it justifies its name of Model-Free Control.
The considered ultra-local model is of the form:

\begin{equation}
\label{eq:F_initial}
	\dot{y}(k) = F(k) + \alpha u(k)
\end{equation}

with $\dot{y}(k)$ the derivative of $y(k)$, $\alpha \in \mathbb{R}$ a constant, and $F(k)$ a term to be estimated (see \cref{eq:F_est}), reflecting the unknown structure of the system and its disturbances.

The Model-Free approach allows plugging PID controllers, defining the notion of \emph{intelligent} PIDs.
For an intelligent Proportional (\emph{iP}), which is linked to a classical Proportional-Integral controller, the control action is computed as:

\begin{equation}\label{ip}
u(k) = - \frac{\hat{F}(k) - \dot{y}^\star(k) + K_p \times e(k)}{\alpha}
\end{equation}

where $\hat{F}(k)$ is the estimated value of $F(k)$, and $K_p \in \mathbb{R}$ the proportional gain.
\cite{fliess2013model} present different techniques to compute $\hat{F}$.
Yet, as a first approach, we use past values of $y$ and $u$ to estimate it from \eqref{eq:F_initial}: 

\begin{equation}
    \label{eq:F_est}
    \hat{F}(k) = {\dot{y}}(k) - \alpha u(k-1) 
\end{equation}


##### Tuning

There are two parameters to tune: $\alpha$ and $K_p$.
In \cite{fliess2013model}, the authors recommend taking $\alpha$ such that $\dot{y}$ and $\alpha \times u$ have the same order of magnitude.
Thus, as in \cite{guilloteau:mfc}, we rely on experiments to set the parameter $\alpha$.
The value of the gain $K_p$ accounts for the closed loop behavior of the system.
Small values of $K_p$ yield conservative and slow controllers, whereas greater values yield more aggressive controllers prone to overshooting and oscillations.
Therefore, the $K_p$ value should be chosen by trial and error methods or by realizing a Pareto type analysis.


<!---
##### Limitations

\todo{somewhere else}

Designing a Model-Free controller requires less knowledge of Control Theory than a classic PID.
It removes the need for complex identification (Section \ref{sec:identification}) and modelling of the system (Section \ref{sec:model_system}).
It also has an online estimation of the underlying dynamics of the system.
Yet, this simplicity comes with a cost.
The Model-Free control does not provide the same guarantees as the classical PID in convergence, robustness, etc.
We cannot choose the closed-loop behavior of our system (Section \ref{sec:closed_loop}).
--->


## Evaluation and Comparison

### Experimental setup

The experiments were carried out on the nodes from the Grisou Cluster of Grid'5000 \cite{grid5000} which is a shared French testbed for experimental research in distributed and parallel computing.
Each node of this cluster has two Intel Xeon E5-2630 v3 CPU with eight cores per CPU and \qty{128}{\gibi\byte} of memory.
Each server of our system is being deployed onto a single Grid'5000 node from a Kameleon system image\ \cite{ruiz_reconstructable_2015}.
We used four nodes for the deployment setup: one for the \cigri\ server, one for the RJMS (\oar\ version 3 \cite{capit_batch_2005,oar3}), one for the fileserver (implemented with NFSv3), and one node emulating a cluster of 100 \oar\ resources.
The job model used in this work (job = \texttt{sleep} + \io) allows us to efficiently and realistically emulate several computing resources on a single physical node, and thus reduce the number of machines required to perform experiments at full scale.
The impact on the realism of such a \emph{folded} deployment is studied in \cite{guilloteau:hal-04038000} and presented in Chapter \ref{chap:folding}.

### Controller configurations

The PI is tuned based on the model of Equation \ref{eq:fit}.
Based on identification from experimental data (Figure \ref{fig:pi:identification:patch}), we have the following values: $\alpha_1 = -0.5071484$, $\beta_1 = 0.0086335$, $\beta_2 = 0.0451394$, and $\gamma = 0.001633$.
The gains of the PI controller are then defined using \cref{eq:kpki}, with the design parameters set to $k_s=12$ and $M_p=0$ in order to avoid any overshoot, but still have a fast response.
The adaptive PI relies on the PI tuning, with additional parameters set as follows: $\mu = [0.5, 0.6, 0.7, 0.8, 0.9, 1]$, $\phi = 2$, $\hat{b}(0) = 0.5$, and $V(0)=10^4$.
For the MFC, refer to \cite{guilloteau:mfc} for the numerical details of its configuration.

### Experimental protocol

For the three controllers considered in this study, we will evaluate their performance by varying the characteristics of the \cigri\ jobs from the identified system.
Controllers were designed for jobs lasting 30 seconds and writing 100 MBytes to the fileserver.
This configuration represents our nominal system.
For testing, we consider variations on two dimensions: the amount of \io\ written by the jobs, and the execution time of the jobs. 
For the \io, we consider the following file sizes: 50, 100, 200, and 400MBytes and for the execution times, we consider: 10, 30, 60, and 120 seconds.

For each triplet (\io\ load, execution time, controller), we repeat 5 times the following scenario:

1. we start with an empty cluster

2. we define the reference value for the controller at 3

3. we submit a campaign of jobs to \cigri\ with the desired characteristics (\io\ load, execution time)

4. we start the controller

5. at $t = \qty{2000}{\second}$, we introduce a disturbance in \io\ which represents a step of 2 for the load of the fileserver

6. at $t = \qty{4100}{\second}$, we stop the disturbance.


### Performance-related comparison

#### Nominal performance

\begin{figure}
    \centering
    \includegraphics[width = \textwidth]{figs/taas/nominal}
    \caption{%
Nominal performance of the different controllers through time.
The solid line represents the aggregated behavior from 5 different experiments (light dots).
The bottom plots compare the controllers on the performance metrics defined in Section \ref{sec:criteria}.
}
    \label{fig:expe:time:nominal}
\end{figure}

We first compare the performance of controllers on the nominal system, \ie\ the system's configuration that was used for design and tuning. 
The three controllers are executed, and the measure of the fileserver load through time is reported in Figure \ref{fig:expe:time:nominal}.
The top plots represent the first \qty{2000}{\second} of the scenario (before the introduction of the disturbance) for each controller.
The solid lines aggregate the behavior of 5 repetitions of each experiment (light dots), and the dashed line represents the reference value that the controllers follow.

All controllers are tracking well the reference imposed,
however with varying performances.
The PI and the MFC have similar behaviors.
The MFC is slightly faster which can lead to greater overshoot, but the PI seems more stable and precise.
The nominal performance of the adaptive PI (aPI) appears quite poor, as the response is slow and imprecise.

In order to compare quantitatively the controllers, we use the performance metrics defined in Section \ref{sec:criteria}: overshoot, response time, and precision.
We evaluate the controllers on the first 2000 seconds, \ie\ before the disturbance, to allow for fair comparison.
The results are presented in the bottom plots of Figure \ref{fig:expe:time:nominal}.
The PI has no overshoot, as defined during the design phase.
Similarly, the MFC mostly never overshoots, but a single experiment lead to an overshoot for the MFC.
Despite the slow response of the adaptive PI, some experiments lead to small overshoots.
The precision of the three controllers is similar, with the one of the adaptive PI being more volatile.
The slow response time of the adaptive PI can be observed on the \emph{Rapidity} plot of Figure \ref{fig:expe:time:nominal}.


\begin{lesson}{}{}
All controllers successfully track the reference value, with overall similar performances.
The adaptive PI is slightly worst in terms of rapidity.
The MFC is fast and precise, but can lead to an overshoot.
The PI is fairly performant on all criteria.
\end{lesson}


#### Portability {#sec:portability}

\begin{figure}
    \centering
    \begin{subfigure}{0.75\textwidth}
        \centering
        \includegraphics[width = \textwidth]{figs/taas/compa_io}
        \caption{Performance with varying I/O loads of the jobs.}
        \label{fig:expe:io}
    \end{subfigure}
    \begin{subfigure}{0.75\textwidth}
        \centering
        \includegraphics[width = \textwidth]{figs/taas/compa_exec}
        \caption{Performance with varying jobs execution time.}
        \label{fig:expe:exec}
    \end{subfigure}
    \caption{%
Global comparison of the controllers' performance for variations in \io\ loads (\cref{fig:expe:io}) and jobs execution times (\cref{fig:expe:exec}). 
The solid lines represent the aggregated behavior from 5 different experiments (light dots).
}
    \label{fig:expe:global}
\end{figure}

\begin{figure}
    \centering
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width = \textwidth]{figs/taas/metrics_io}
        \caption{Performance with varying I/O of the jobs.}
        \label{fig:compa:io}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width = \textwidth]{figs/taas/metrics_exec}
        \caption{Performance with varying jobs execution time.}
        \label{fig:compa:exec}
    \end{subfigure}
    \caption{
        Comparison of controllers' portability.
        The plots represent the performance metrics computed on various systems between 2000 seconds.
        The solid lines link the means over varying configurations for each controller.
    }
    \label{fig:expe:compa}
\end{figure}

<!---
Two variations are considered: one on the \io\ load, where the size of the file that the jobs write is varied, and one where the duration of jobs' execution time is varied.
--->

To evaluate the portability of controllers, we look at their performance when used on a different system than the nominal one. 
\Cref{fig:expe:global} depicts the dynamic behavior of the different controllers for the different scenarios: variations in \io\ and in execution times (\Cref{fig:expe:io,fig:expe:exec} respectively). 
Note that the nominal performance, \ie\ \Cref{fig:expe:time:nominal}, are repeated (second line of the plots) to allow for comparison. 
Similarly to the previous analysis, aggregated metrics of performance on the first 2000 seconds are computed in \Cref{fig:expe:compa}.

##### Variations of \io\ Load

All controllers manage to track the reference value, however some configurations lead to large oscillations in the fileserver load.
The disturbance does not seem to have a detrimental impact, it even reduces the oscillations in some cases.

For the MFC, the trend is similar than in nominal conditions: fast, but can lead to large overshoots.
The PI is slower but has fewer oscillations when the system is highly different from the nominal one.
The adaptive PI seems to have worse performance on the nominal system (100MBytes) than the other two controllers, but then it does not seem to be much impacted by the variations in \io\ load.

The quantitative analysis of \Cref{fig:compa:io} clarifies those analyses:

1. overshoot in the first 2000 seconds is larger for the adaptive PI, except with 400 MBytes (when we are far away from the nominal system), where both PI and MFC present huge values

2. precision is similar for all controllers, except with 400 MBytes where the adaptive PI is significantly better rapidity of the adaptive PI is better than for the other controllers on non-nominal systems

3. rapidity of the adaptive PI is better than for the other controllers on non-nominal systems (note that for the experiments with large oscillatory behavior, the rapidity criteria should be mitigated as convergence is not reached)

\begin{lesson}{}{}
The MFC and the PI perform very well close to the nominal system, but have difficulties not overshooting and oscillating when the system is too far from the nominal system, \ie\ 400 MBytes in our scenario. 
On the other hand, the adaptive PI has a similar behavior whatever the system configuration.
\end{lesson}


##### Variations of Jobs Execution Time

When variation in the jobs' execution time is introduced, controllers successfully track the reference, with a better behavior than in the case of \io\ variations, see \Cref{fig:expe:exec,fig:compa:exec}.

For the PI and MFC controllers, reducing the job duration to 10 seconds even improves the performances by reducing the overshoot.
When increasing the duration, the PI seems to better control the system, with few oscillations and small overshoot. 

The relative poor performance of the adaptive PI for job duration variations can be theoretically explained.
The increase of jobs duration leads to the addition of a delay in the system, \ie\ it takes more time to see the effect of an action on the system.
The adaptive PI was built to adapt the model parameter $b$ of the model, not to identify a delay.
Thus, the adaptive PI is not designed to deal with such type of system's variation, and its adaptation can be detrimental in some cases.

\begin{lesson}{}{}
Overall, concerning the variations in execution times, the controllers appear less sensible than with \io\ variations.
There is overshoot, especially with the MFC and the adaptive PI, but not as many oscillations.
\end{lesson}

### Methodology and Implementation-related comparison

This section complements the comparison of controllers on their performance, by focusing on aspects linked to their design, tuning, theoretic soundness, and implementation. 
Such considerations are not classic for the point-of-view of control-theory oriented evaluation, that focuses mainly on performance and robustness to unpredicted external events.
On the contrary, from the practical engineering point-of-view, simplicity in design, and usage can be a valuable asset. 
Some performance can even be traded to allow lower complexity,
so that system administrators can master solutions without the help from an external control expert, or then only in a limited and temporary fashion. 
In the following, we evaluate the three controllers on the methodology- and implementation-related criteria of Section \ref{sec:criteria} set-up complexity, guarantees, and control-theoretical competence required.
<!---
A discussion of the trade-off between those criteria are provided in the next section.
--->

#### Set-up complexity

In a first attempt to evaluate the setup complexity, we count the number of parameters to be tuned for each controller.

The MFC requires two parameters: $K_p$ and $\alpha$.
$\alpha$ is a non-physical constant parameter.
It is chosen by the practitioner such that $\dot{y}$ and $\alpha \times u$ are of the same magnitude.
It should be therefore clear that its numerical value, which is obtained by trials and errors, is not a priori precisely defined \cite{fliess2013model}.
$K_p$ is chosen through experiments.

The PI also requires two parameters: $K_p$ and $K_i$.
They are computed as in \Cref{eq:kpki} based on the model of the system (two parameters in the case of the linear first order model: $a$ and $b$), that can be derived from the identification experiments.
Moreover, the tuning allows translating the problem of finding $K_p$ and $K_i$ into the choice of the design quantities $k_s$ and $M_p$.
Thus, two parameters are needed for setting up the PI.

Finally, the adaptive PI is based on the PI: thus it requires the same two parameters.
It additionally requires parameters specific to the adaptive part: $\mu$, $\phi$, $\hat{b}(0)$, $V(0)$, summing up to a total of six parameters.

Besides the raw count of parameters, we can point out that some parameters are more sensitive than others in terms of precision needed.
We therefore see three cases:

1. the very sensitive parameters for which an ill-tuned value can even make the system unstable (\eg\ $K_p$)

2. the parameters for which an ill-tuned value makes the system slower (\eg\ $\phi$)

3. the parameters which are continuously updated and therefore an approximate initial value has little impact (\eg\ $V(0)$)


#### Guarantees

The controllers studied in this chapter have different guarantees from the point-of-view of their behavior.

As seen in Chapter \ref{chap:harvest_fs}, the design of a PI controller requires following a well-defined methodology.
This has the benefits of allowing users to choose the closed-loop behavior (choice of $k_s$ and $M_p$ as illustrated in \Cref{fig:ksMp}), and to have guarantees that the controller will behave as intended. 

Designing and using a Model-Free controller requires less knowledge of Control Theory than a classical PI.
It removes the need for complex identification and modeling of the system.
It also has an online estimation of the underlying dynamics of the system.
Yet, this simplicity comes at a cost: the MFC does not have a fixed methodology to choose all the parameters.
It hence does not provide the same guarantees as the classical PI with respect to convergence or robustness.
<!---
The MFC does not have a methodology to choose the parameter $K_p$, and as a result, there is no guarantee that the system won't overshoot or oscillate.
--->

The adaptive PI provides additional guarantees to the PI, by broadening the conditions in which the controller is operational.
That is to say that the guarantees that one has on the nominal system, also extend to systems close to the nominal one, as seen in Section \ref{sec:portability}. 
However, knowing in advance if a system falls into the region of stability of a controller is not straightforward for a computing system, especially without performing a preliminary modeling identification phase.
Additionally, the adaptive PI has the drawback of requiring specific input signals -- known as persistent excitation -- to avoid its divergence, namely the bursting phenomena.

#### Competence in Control required

On most computer science curriculums, there are no classes on control-theory.
Most cloud/grid platforms usually do not hire control scientists or engineers in their system administration team.
Therefore, the choice of a control method should be weighted by its difficulty to be mastered by the system administrators.

In this perspective, MFC has the advantage of very low requirements, as it does not require specific engineering education.
On the other hand, it is a novel, emerging method, with only little literature and experience from which to draw practical guidelines.

In the case of the classical PI, there is a need to perform open-loop identification experiments.
It has a practical cost, but there is only relative low mathematical difficulty in the equations at play.
Therefore, it is accessible and taught to undergraduate students in control engineering curriculums.
As it is very widely used in industry (around \qty{90}{\percent} of the industrial processes use a form of PI/PID controller) there is a huge amount of available literature \cite{PID-Astrom,1580153,dorf2021modern}.

The adaptive PI builds up on PI: it hence requires a greater set of skills to use\ \cite{LandauID2011Adaptive}.
Moreover, over the years, many upgrading elements and variants have been proposed in order to correct and improve certain aspects in its design (\eg\ \cite{aastrom2013adaptive,ioannou1996robust,middleton1988design}).
In terms of skills, this usually is taught in Master curriculums dedicated to control engineering or even at the PhD level for the more advanced variants.


## Conclusion

This chapter addressed the need for the reusability of autonomic controllers in the context of HPC with the study case of \cigri.
Controllers from control-theory can have a bound too tight to the nominal system, which can make them lose performance dramatically when they are used 
in different contexts, with variations in time or in space.
<!---
(\eg\ campaigns with different types of best-effort jobs)
or in space 
(\eg\ several clusters with different configurations).
--->
We proposed a comparison framework to evaluate the reusability of autonomic controllers, defined criteria of interest, and exposed tradeoffs between criteria.
Using the study case of \cigri, we implemented three controllers of different nature (PI, adaptive PI, and Model-Free control), and compared them with respect to our criteria.

\begin{table}[h]
\centering
\begin{tabular}{p{5cm} c c c}
\toprule
Criteria                                       & PI & Adaptive PI & MFC \\
\midrule
Nominal Performance                            & +  & +           & +   \\
Portability                                    & -  & +           & -   \\
Design Complexity                              & +  & -           & +   \\
Guarantees                                     & +  & +           & -   \\
Competence required                            & +  & -           & +   \\
\bottomrule
\end{tabular}
\caption{
	Comparison of controllers on all criteria.
	\enquote{+} indicates a positive evaluation of the criteria -- \eg\ high portability, low complexity.
	\enquote{-} that the criteria is poorly fulfilled -- \eg\ few guarantees, high competence required.
}
\label{table:criteria-summary}
\end{table}

\Cref{table:criteria-summary} summarizes the comparison of the different controllers on the criteria of the framework of Section \ref{sec:criteria}.
More general comparative results are still an open question due to the great variety of cases according to the control problems.
As expected, there is no clear \enquote{best controller} from a computer engineer point-of-view, but the table exhibits several tradeoffs.

The recommendations and guidelines that can be given to system administrators depend on different contexts: whether a control engineer is available or not, whether guarantees on the behavior are crucial or not, etc.
For example, if the system on which the controller will be deployed only varies \enquote{close} to the nominal system, then both the PI and the MFC are good solutions from the reusability point-of-view.
However, if the system can vary a lot, then an adaptive PI appears to be a more judicious choice concerning portability.
Note that it will be adaptive only with respect to the variables for which it has been designed.

In the case where one wants to give away some design complexity and required competence, but still want acceptable nominal performance, the MFC appears as an excellent candidate as it has a low setup complexity.

In the case where a well documented and explainable solution is required, then the PI controller has the advantage of being a very well-known control algorithm.






<!---
Our contributions are :
\begin{enumerate}
    \item 
    The identification of the relationship between
    the problem of reusability of autonomic controllers
    and 
    the potentials of the techniques of adaptive control
    (firstly here in the sense that controllers are able to readjust w.r.t. changes).
    
    
    \item 
    the proposal of a comparison framework to evaluate the reusability of autonomic controllers,
    with a general approach posing criteria of interest and their tradeoffs,
    and a more specific comparison in the case of HPC resource harvesting.
    
    
    \item 
    the experimental validation of the comparison on the use case,
    measuring the behavior of alternate controllers in the presence of changes in the defined criteria,
    and exhibiting comparatively better reusability of the MFC over the PID.
    
\end{enumerate}

The expected advantage of the MFC 
in terms of performance
is here assessed experimentally and quantitatively.
--->


<!---
\subsection{Perspectives}

Perspectives are in the directions of 

\begin{itemize}
    \item 
concerning the design of controllers :

    adaptive control

in the case of MFC,
designing a mechanism to readjust the value of \(K_p\)

\item 
concerning the treatment of criteria :

quantifying the uptil now 
qualitatively treated aspects,
e.g.
differences in performance (to quantify how satisfactory they are)
and 
modifications


\end{itemize}
--->

