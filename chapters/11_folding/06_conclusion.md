## Conclusion

In this Chapter, we investigated the use of *folding* techniques to reduce the number of deployed machines for the large scale evaluation of applications such as grid and cluster middlewares like \cigri.
We have seen that *folding* requires a change in the job model 
We focused on the impact on the performances of the file-system. 
To do so, we took a distributed I/O benchmark, IOR, and ran it for several cluster sizes, I/O loads and distributed file-systems. 
We analyzed the results of the benchmark and reached to the following conclusions:

- Write operations on NFS are not subject to folding (Take away \ref{th:nfs}).

- The performance of read operations on NFS can be modeled with a quadratic relation, and this model can be used to determine the maximum folding for an accepted degradation (Take away \ref{th:nfs_model}).

- The performance of read operations in a fully folded cluster with OrangeFS do not depend on the number of I/O nodes (Take away \ref{th:orangefs:folded}). 

- There are breaking points in reading and writing performance for OrangeFS when folding the cluster. Equation \ref{eq:orangefs:models} gives *rules of thumb* to estimate these breakpoints (Take away \ref{th:orangefs:model}).

This study presents some limitations.
The studied file-systems are not among the most popular in large HPC centers.
It would be interesting to consider file-systems such as Lustre\ \cite{lustre}, BeeGFS\ \cite{beegfs}, GlusterFS\ \cite{glusterfs}, and Ceph\ \cite{ceph}. 
During his Master internship, Alexandre Lithaud helped setting up distributed environments in \nxc\ containing such PFS, which will allow to extend this study \cite{lithaud}.
The reasons of this loss of performance due to folding are still unclear.
The main suspect is the network.
We did observe a speed similar (23 Gbps) to the one advertised on the NIC (25 Gbps).
We think that the overhead can be due to the TCP protocol.
Investigating different network protocol like InfiniBand or OmniPath would be interesting.

Recent discussions with Francieli Boito\footnote{\url{https://www.labri.fr/perso/fzanonboito/}} highlighted potential imperfections of our protocol concerning the potential impact of caching on read requests on NFS. 
