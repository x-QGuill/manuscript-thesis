## Methodology {#sec:methodo}

We aim at evaluating the variations in performance of a distributed application using a distributed file-system for different value of folding factors ($f_{fold}$).
As explained previously, the job model that we are using is a `sleep` time to represent the CPU bound phase, and a `dd` operation to represent the I/O phase.
The sleep operation allows us to fold the CPU bound phase on the same machine easily without noise.
We thus focus on the performance of the I/O operations in a folded deployment.

### Experimental Setup

The following experiments were carried on the `gros` cluster, located in Nancy, of the \grid\ \cite{grid5000} French test bed.
The machines of this cluster have an Intel Xeon Gold 5220 CPU with 18 cores, 96 GiB of memory, a 2 x 25 Gbps (SR-IOV) network and a 480 GB SSD SATA Micron MTFDDAK480TDN disk.
The reproducibility of the deployed environment was ensured by \nxc\ \cite{nxc}.
The environment definitions are available at \cite{nxc_nfs, nxc_orangefs}, the analysis scripts at \cite{folding_sw}, and the data on Zenodo\ \cite{zenodo_folding}.

### Benchmark application

To evaluate the performance of the cluster file system, we chose to use the IOR \cite{ior} benchmark.
IOR is a MPI application to benchmark I/O performances.
It is the most popular benchmark among research on I/O in HPC \cite{boito2018checkpoint}, and is also used in the context of the IO500 list \cite{iolist}.

IOR has a multitude of parameters, but give the main ones here.
We used the POSIX protocol (`api=POSIX`), a transfer size (`transferSize`) of 1Mbytes and a segment count of 1 (`segmentCount`).
We assigned a single file per IOR process (`filePerProc`), and checked the correct writing and reading afterwards (`checkWrite` and `checkRead`).
The number of tasks (`numTasks`), which represent the number of IOR processes, and the block size (`blockSize`), which is the total size of the file to read/write in this case, are parameters of the following experiments.
We used OpenMPI with the TCP backend.

### Distributed File-Systems

For the chosen distributed file-systems, we used NFS and OrangeFS.

###### NFS (v4) {#sec:fs:nfs}

NFS \cite{pawlowski2000nfs} is a popular **distributed** file-system for small clusters.
There is only one server.
Clients mount the file-system and can perform POSIX operations.
Figure \ref{fig:nfs} depicts the simplified architecture of a distributed file system like NFS.
All the clients query the same I/O node for their files. 
NFS servers do have several workers that can manage the requests concurrently. 
The NFS export options used are: `*(rw,no_subtree_check,fsid=0,no_root_squash)`.
The NFS server runs under the default configuration (8 workers).


###### OrangeFS {#sec:fs:pvfs}


\begin{figure}
\begin{subfigure}[b]{0.45\textwidth}
  \centering
  \includegraphics[width=0.7\textwidth]{./figs/folding/schemas/nfs.pdf}
  \caption{Distributed File system like NFS. Multiple clients query a single I/O node that process all the requests.}
  \label{fig:nfs}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.45\textwidth}
  \centering
  \includegraphics[width=0.7\textwidth]{./figs/folding/schemas/pfs.pdf}
  \caption{Parallel File system like OrangeFS. Multiple clients query in parallel all the I/O nodes.}
  \label{fig:pfs}
\end{subfigure}
\caption{Architectures for a distributed file system (Figure \ref{fig:nfs}), and parallel file system (Figure \ref{fig:pfs}).}
\end{figure}

OrangeFS \cite{bonnie2011orangefs} (or PVFS2) is a **parallel** file-system.
This means that there are several servers (also called I/O nodes) to manage the requests of the clients.
Figure \ref{fig:pfs} depicts the simplified architecture of a parallel file-system like OrangeFS.
The clients query a I/O node of the file system.
If one stripe asked by the client is not present on the query I/O nodes, the file system indicates on which I/O node to find it.
We used the default configuration recommended by the OrangeFS installer (the I/O nodes host both the metadata and the storage).
<!---
Moreover, in inour system (Section \ref{sec:cigri}), we do not consider the problem of metadata.
--->


##### Number of I/O nodes

In the case of PFS, like OrangeFS, we did not find any methodology nor "rule of thumb" to define the number of I/O nodes for a computing cluster.
In the following, we will consider OrangeFS file-systems with 1, 2, or 4 I/O nodes. 
Note that in the case of NFS there is only one I/O node.

##### I/O load

We consider 5 different sizes of I/O operations to perform, both in writing and reading: 1Mbytes, 10Mbytes, 100Mbytes, 500Mbytes, and 1Gbytes.
These file sizes will be the values of the IOR `blockSize` option.

##### Number of CPU nodes

As a first step, and because we aim at emulating clusters from regional meso-centers, we will thus consider small clusters of 8, 16, 24, and 32 nodes.
These number of nodes will be the values of the IOR `numTasks` option: one tasks per node of the cluster.

##### Protocol

Let us consider a system with $N$ CPU nodes.
We first deploy the system at full scale, \ie\ $N$ machines and the I/O nodes of the file system.
As IOR is an MPI application, we compute the `hostfile` with one `slots` per compute node.
We then start the IOR benchmark, that we repeat 5 times (the IOR `repetitions` option), and gather the performance reports.
We remove one compute node from the `hostfile` and recompute the `slots` for the remaining nodes to keep the number of processes (`numTasks`) constant.
This protocol is then repeated for all the different variations: number of CPU, size of I/O operations, type of file system, number of I/O nodes.

\begin{figure*}
   \begin{subfigure}[b]{0.45\textwidth}
       \centering
       \includegraphics[width=0.9\textwidth]{figs/folding/schemas/proto_1.pdf}
       \caption{Initial deployment at full scale: one process per node.}
       \label{fig:proto_1}
   \end{subfigure}
   \hfill
   \begin{subfigure}[b]{0.45\textwidth}
       \centering
       \includegraphics[width=0.9\textwidth]{figs/folding/schemas/proto_2.pdf}
       \caption{We remove \texttt{node4} and add one extra \texttt{slot} to \texttt{node3}.}
       \label{fig:proto_2}
   \end{subfigure}
   
   \begin{subfigure}[b]{0.45\textwidth}
       \centering
       \includegraphics[width=0.9\textwidth]{figs/folding/schemas/proto_3.pdf}
       \caption{We remove \texttt{node2} and add one extra \texttt{slot} to \texttt{node1}.}
       \label{fig:proto_3}
   \end{subfigure}
   \hfill
   \begin{subfigure}[b]{0.45\textwidth}
       \centering
       \includegraphics[width=0.9\textwidth]{figs/folding/schemas/proto_4.pdf}
       \caption{We remove \texttt{node3} and reach a fully folded deployment.}
       \label{fig:proto_4}
   \end{subfigure}
  \caption{Graphical representation of the experimental protocol presented in Section \ref{sec:methodo}. We start with a full scale deployment, \ie\ one MPI process (viewed as a virtual resource) per physical node (Figure \ref{fig:proto_1}), and remove from the \texttt{hostfile} the physical nodes one by one while keeping the number of MPI processes (\ie\ the number of virtual resources) constant. We recompute the number of \texttt{slots} per node to balance the processes (Figures \ref{fig:proto_2} and \ref{fig:proto_3}). The experiment stops when there is no more node to remove (\ie\ after Figure \ref{fig:proto_4}).}
  \label{fig:proto}
\end{figure*}

Figure \ref{fig:proto} shows a visual representation of the experimental protocol for an experiment with 4 CPU nodes.


