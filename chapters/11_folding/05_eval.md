## Evaluation {#sec:folding:eval}

In this Section, we present the results of the experiments presented in the previous section.
We first consider the file-system of the cluster to be NFS in Section \ref{sec:eval:nfs}, and then OrangeFS in Section \ref{sec:eval:pvfs}. 

### Evaluation of NFS {#sec:eval:nfs}

\begin{figure}
  \centering
  \includegraphics[width = \textwidth]{./figs/folding/graphs/nfs/ratio_folding.pdf}
  \caption{
Evolution of the reading (top row) and writing (bottom row) times based on the folding factor (x-axis) for experiments with different cluster size (\ie\ number of CPU nodes) and different sizes of file to read and write (point shape).
We observe that the writing performances are not affected by the folding, but that the reading ones are, and that the degradation has quadratic growth with respect to the folding factor.
  }
  \label{fig:nfs_all}
\end{figure}

Figure \ref{fig:nfs_all} shows the results of the experiments when using NFS.
We also plot the 95\% confidence intervals.
Note that we **did not** remove the outliers.
We notice that the writing performances (bottom row) does not seem to be affected by the folding of the deployment as it remains flat.
However, for the reading performances (top row), we can see that the reading times increasing for higher folding factor.
This means that the more we fold, the more we degrade the reading performances.

\begin{lesson}{}{nfs}
Write operations on NFS do not seem to be affected by the folding of the deployment.
On the other hand, read operation performances degrade the more the cluster is folded with a quadratic behavior.
\end{lesson}

\begin{figure}
  \centering
  \includegraphics[width = \textwidth]{./figs/folding/graphs/nfs/model.pdf}
  \caption{%
  Linear regression modeling the reading times (y-axis) and the folding factor (x-axis), file size (point shape).
  The top row shows the fitting of the model on the data, and the bottom row the same data but in \textit{log} scale.
  We can see that the model fits correctly the data for file sizes greater than 10M.
  1M files does not seem to be affected by the folding, and their variation in performance seem to be due to noise.}
  \label{fig:nfs_model}
\end{figure}

We modeled the reading performances based on the size of the file to read and the number of CPU nodes involved. 
Figure \ref{fig:nfs_model} shows the results of a linear regression between the reading time and the folding ratio, file size and number of CPU nodes involved.
We fitted a model with the following form:

\begin{equation}\label{eq:model_nfs}
  t_{read}(f_{fold}, f_{size}) \simeq \alpha + \beta_1 {f_{fold}}^2 + \beta_2 f_{size} + \gamma {f_{fold}}^2 f_{size}
\end{equation}

with $\alpha, \beta_1, \beta_2, \gamma \in \mathbb{R}$ the coefficients of the model.
The $R^2$ of the fitting is 0.9982.

Figure \ref{fig:nfs_model} shows the fitting of the model on the NFS data.
The bottom row represents the same information as the top one, but in log scale.
We can see that the model fits all the file sizes but for 1M.
We believe that the variations in performance for the 1M files are due to noise.

We are interested in knowing the maximum folding factor ($f_{fold}$) possible for a desired file size ($f_{size}$).
Let $p > 1$ be the percentage of increased reading time compared to the full scale deployment containing $nb_{cpu}$ CPU nodes.
By using the definition of the model for $t_{read}$ in Equation \ref{eq:model_nfs}, we get:

\begin{equation}
  \frac{t_{read}(f, f_{size})}{t_{read}(nb_{cpu}, f_{size})} < p \implies f < \sqrt{\frac{p \times t_{read}(nb_{cpu}, f_{size}) - \left(\alpha + \beta_2 f_{size}\right)}{\beta_1 + \gamma f_{size}}}
\end{equation}

\begin{figure}
  \centering
  \begin{subfigure}[b]{0.49\textwidth}
    \centering
    \includegraphics[width = \textwidth]{./figs/folding/graphs/nfs/model_percentage.pdf}
    \caption{Maximum folding factor ($f_{fold}$) based on the accepted degradation of the reading time compared to the full scale deployment and to the size of the file to read.}
    \label{fig:nfs:model_percentage}
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.49\textwidth}
    \centering
    \includegraphics[width = \textwidth]{./figs/folding/graphs/darshan/analysis_read.pdf}
    \caption{Proportion of the size of the read operations on ANL-Theta between 2019 and 2022. The majority is smaller than 100K. These values are extracted from Darshan \cite{darshan1, darshan2} logs.}
    \label{fig:darshan}
  \end{subfigure}
  \caption{Figure \ref{fig:nfs:model_percentage} shows the maximum folding factor to use to have a desired overhead on the reading times on NFS base on the file size. Figure \ref{fig:darshan} shows the distribution of the number of read requests per size on ANL-Theta.}
\end{figure}

\begin{lesson}{}{nfs_model}
For files of size 10M, folding 10 resources onto a single physical resource leads to a degradation of 5\%.
To reach the same degradation for file size of 100M or 1G, the maximum folding factor would be 5. (Figure \ref{fig:nfs:model_percentage})
\end{lesson}

From the model defined previously, Figure \ref{fig:nfs:model_percentage}, and the Darshan \cite{darshan1, darshan2} logs from ANL-Theta between 2019 and 2022 (Figure \ref{fig:darshan})\footnote{This data was generated from resources of the Argonne Leadership Computing Facility, which is a DOE Office of Science User Facility supported under Contract DE-AC02-06CH11357.},
we can have an estimation of the overhead if we decided to rerun these Darshan logs (on NFS and with the job model considered in Chapter \ref{chap:cigri_jobs}) with different folding factors.
For example, a folding factor of 10 would lead to an overhead of 64 hours over 4 years (\ie\ an increase of 0.2\%), while requiring 10 times fewer machines.


### Evaluation of OrangeFS {#sec:eval:pvfs}

In the case of OrangeFS, there is an extra dimension to explore: the number of I/O nodes in the file-system.


\begin{figure}
  \centering
  \includegraphics[width = \textwidth]{./figs/folding/graphs/orangefs/ratio_write.pdf}
  \caption{
Evolution of the writing times with OrangeFS based on the folding factor ($f_{fold}$) for experiments with different number of CPU and I/O nodes and different sizes of file to write.
}
  \label{fig:orangefs:write}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width = \textwidth]{./figs/folding/graphs/orangefs/ratio_read.pdf}
  \caption{
Evolution of the reading times with OrangeFS based on the folding factor ($f_{fold}$) for experiments with different number of CPU and IO nodes and different sizes of file to read.
}
  \label{fig:orangefs:read}
\end{figure}

Figures \ref{fig:orangefs:write} and \ref{fig:orangefs:read} show respectively the evolution of the performance in reading and writing time of the IOR benchmark for different number of CPU nodes in the cluster and I/O nodes in the file-system, as well as different sizes of file to read/write.

We can see on Figure \ref{fig:orangefs:write} that contrary to NFS, there is a significant loss of performance for the write operations when increasing the folding factor.
This loss of performance appears more significant when there are more I/O nodes in the file-system.

Concerning the reading performances (Figure \ref{fig:orangefs:read}), we observe the same behavior as for NFS.
High folding factors lead to an increase of reading time.
The increase appears greater when there are more I/O nodes in the file-system.
The start of this increase seems to depend on the number of CPU nodes.
The more CPU nodes, the later the increase starts.
It is interesting to note that the variation when measuring the reading time during the experiments is smaller (thus more stable) than for NFS.

\begin{lesson}{}{orangefs:folded}
The reading performance of a fully folded deployment \textbf{does not depend} on the number of I/O nodes in OrangeFS (Figure \ref{fig:orangefs:read}).
\end{lesson}


For both reading and writing performances there seem to be two behaviors.
First a phase where the folding does not affect the performances, and then, from a given folding factor, the reading/writing times grow linearly.
As we want to know the maximum folding factor until which the folded system behaves like a full scale system, we are now interested in finding this breakpoint where the behavior changes.
Using *segmented regression* techniques \cite{muggeo2003estimating}, we found a model that we simplified to make it a *rule of thumb*:

\begin{equation}\label{eq:orangefs:models}
\begin{aligned}
  f_{break,r} & \simeq 1 + 0.3 \times nb_{cpu} - 0.5 \times nb_{io}\\
  f_{break,w} & \simeq 2 + 0.3 \times nb_{cpu} - 0.5 \times nb_{io}
\end{aligned}
\end{equation}

where, $f_{break,r}$ and $f_{break,w}$ are respectively the folding factor of the breakpoint for the reading and writing performances, $nb_{cpu}$ and $nb_{io}$ are the number of CPU and I/O nodes in the system.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{./figs/folding/graphs/orangefs/segmented_read.pdf}
  \caption{Model of the breaking point in behavior of performance in reading (left) and writing (right) for 32 CPU nodes ($nb_{cpu}$) and 4 I/O nodes ($nb_{io}$).
  The model (dashed line) comes from Equation \ref{eq:orangefs:models}.}
  \label{fig:orangefs:model}
\end{figure}

\begin{lesson}{}{orangefs:model}
For writing and reading times on OrangeFS, there is a breakpoint in performance before which there is no degradation.
We modeled \textit{rules of thumb} to them in Equation \ref{eq:orangefs:models}. 
These \textit{rules of thumb} are depicted in Figure \ref{fig:orangefs:model}.
\end{lesson}

As $f_{break,w}$ (Equation \ref{eq:orangefs:models}) will always be greater than $f_{break,r}$, \textbf{the overall breaking point in performance for OrangeFS is $f_{break,r}$}.
