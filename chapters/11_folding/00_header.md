# Reducing the Cost of Experimenting with Distributed File-System {#chap:folding}

This chapter is based on \cite{guilloteau:hal-04038000} presented at the 15th JLESC workshop with Olivier Richard, Raphaël Bleuse, and Eric Rutten.
