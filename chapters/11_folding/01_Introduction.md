## Introduction

The increase of computing demands from scientists from all fields led to the development of computing cluster and grid architectures.
And with these new architectures came new challenges.
Due to their high prices, clusters are often shared among several research laboratories and teams.
This sharing motivates the development of middlewares such as batch schedulers that are responsible to assign the user jobs to physical machines, manage the state of the resources, deal with reservation, etc.
Such cluster, or grid, middlewares are complex applications of great research interest, and they must be tested before reaching production.
However, they are usually destined to operate in an environment of hundreds or thousands of machines. 
Deploying a full scale environment is too costly to perform simple tests and very specific evaluations.

We thus want to answer the following question: **Can we reduce the number of physical machines needed to perform a full scale experiment while keeping a similar behavior as the full scale system?**

One solution to reduce the number of machines used for experiments would be to use simulation techniques.
The system, the applications, and the middlewares are modeled and can then be executed on a single node.
Beside reducing the number of machines used, simulators also reduce the execution time of the experiments. 
In the context of distributed systems and applications, projects such as Simgrid \cite{casanova:hal-01017319} and Batsim \cite{dutot:hal-01333471} are leading the way.
One drawback of simulation is that the real middleware is not being executed, or not fully executed, but instead, a partial or modeled version is executed in a modeled environment.
In the case of cluster and grid middlewares, the applications are often far too complex to model them fully correctly.

Another approach is to *fold* the experiment by deploying more "virtual" resources on physical machines.
In the case of a Resources and Jobs Management Systems (RJMS), like Slurm \cite{goos_slurm_2003} or OAR \cite{capit_batch_2005}, it is possible to define several resources, from the point of view of the RJMS, on a single machine, which would be completely transparent for the users.
For example, one can deploy a 1000-node virtual cluster on 10 physical nodes by defining 100 virtual resources on each node.
The advantage is that the experiment takes place on the real system (CPU, network, disk, etc.) and with the real middleware code.
The drawback is that this folding can introduce noise in the experiment and degrade performance. 

In this chapter, we evaluate the performance of a distributed I/O benchmark when we fold a computing cluster onto itself, and quantify the impact of folding a computing cluster containing a file system.
We then give a *rule of thumb* for choosing the amount of folding for distributed experiences containing a distributed file-system. 

Section \ref{sec:defs} defines notions and concepts.
We present the experimental protocol in Section \ref{sec:methodo} and perform the evaluation in Section \ref{sec:folding:eval}.
