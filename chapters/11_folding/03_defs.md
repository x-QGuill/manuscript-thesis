## Definitions \& Concepts {#sec:defs}

We define the **folding** of a deployment as the action of defining several "virtual resources" on a physical resource.
A physical resource is a node from a cluster, and a virtual resource represents a resource on the full scale system from the point of view of the RJMS.

We also define the **folding factor** ($f_{fold}$) as the division of the number of virtual resources in the deployment divided by the number of physical resources.
Intuitively, it represents the number of virtual resources for each physical resource:

\begin{equation}
  f_{fold} = \frac{\#resource_{virtual}}{\#resource_{physical}} \in [1, +\infty[
\end{equation}

Figure \ref{fig:fold_example} depicts an example of folding a deployment.

\begin{figure*}
     \centering
     \begin{subfigure}[b]{0.3\textwidth}
         \centering
         \includegraphics[width=0.9\textwidth]{figs/folding/schemas/folding_r_1.pdf}
         \caption{Folding with a factor of 1 (4 physical resources and 4 virtual resources).}
         \label{fig:r_1}
     \end{subfigure}
     \hfill
     \begin{subfigure}[b]{0.3\textwidth}
         \centering
         \includegraphics[width=0.9\textwidth]{figs/folding/schemas/folding_r_05.pdf}
         \caption{Folding with a factor of 2 (2 physical resources and 4 virtual resources).}
         \label{fig:r_05}
     \end{subfigure}
     \hfill
     \begin{subfigure}[b]{0.3\textwidth}
         \centering
         \includegraphics[width=0.9\textwidth]{figs/folding/schemas/folding_r_025.pdf}
         \caption{Folding with a factor of 4 (1 physical resource and 4 virtual resources).}
         \label{fig:r_025}
     \end{subfigure}
        \caption{Example of folding a deployment for a system with 4 resources. Figure \ref{fig:r_1} depicts the system deployed at full scale. Figure \ref{fig:r_025} represent the system completely folded. And Figure \ref{fig:r_05} shows an intermediate folded deployment.}
        \label{fig:fold_example}
\end{figure*}
