This problematic arises in the context of experiments with \cigri\ where we must deploy and perform experimentation evaluation of a modified version of \cigri\ on a realistic environment.
It is unreasonable to deploy on the *entire* *Gricad* meso-center, or using simulation due to the complex software stack (\cigri\ + \oar\ + users jobs).
We are thus interested in folding strategies to perform the evaluation of our \cigri\ modifications.

The statistical description of the execution times of the \cigri\ jobs presented in Chapter \ref{chap:cigri_jobs} allows us to use a `sleep` model to represent the execution times.
As `sleep` calls are extremely lightweight for a CPU to deal with, we are able to fold several virtual resources onto one physical resource. 
However, no realistic job only does computation without reading and/or writing data.
We can extend the previous job model by adding I/O operations before or after the `sleep` with the `dd` command.
This addition makes it less obvious how much we can afford to fold.

The questions that we want to answer are thus the following:

1. **What is the minimum number of machines we need to deploy to emulate a full scale cluster with this job model while keeping the same performance in I/O?**

2. **Is there a trade-off between the number of physical resources used and the overhead of performances due to the folding?**

