# General Conclusion {#chap:meta_conclu}

This thesis presented solutions to regulation problems in the harvesting of idle computing resources in HPC cluster.
We used tools from Autonomic Computing coupled with Control Theory to design and implement controllers with proven guarantees in the \cigri\ middleware (Part I).
The experimental setup of \cigri\ led us to investigate more experiment-oriented research questions (Part II), \ie\ how to reduce the number of machines required to deploy an experiment, and in a reproducible fashion.

We gave some detailed perspectives of this thesis Chapters \ref{chap:conclu_ctrl} and \ref{chap:conclu_expe}.
In this Chapter we give an higher-level conclusion. 

Regulation problems are frequent in Computer Science, but especially in fields where there are runtime components or Service-Level Agreements.
Scheduling or Machine learning-based strategies are very popular but have limitations.
Scheduling usually make assumption on the system, information given by the users, etc.
This information is then processed by complex (cognitively and computationally) algorithms that return a schedule of the jobs.
It is possible to prove performance bounds of the scheduling algorithm.
Machine learning strategies are black boxes.
The cost of training the model is high, and the resulting model does not have any proven guarantees other than the precision resulting of the training.
Control Theory is a powerful, yet underused, technique for the runtime management of computing systems.
It really excels at regulating trade-offs, degradation levels, SLAs, QoS, etc.
Moreover, using Control Theory tools on computing systems can foster interaction between research fields, as it requires a knowledge and expertise very rarely seen among computer scientists and system administrators.
We tried to tackle this issue at our modest scale by creating and presenting a tutorial to introduce Control Theory to computer scientists\ \cite{tuto_control}.


The experiments required to design controllers need to be robust to be able to design a correct controller.
The system on which we worked on during this thesis is complex, with deep software stacks (\cigri, \oar, file-system, jobs, etc.).
Deploying this complete stack in a reproducible fashion is tricky.
Usual tools to deploy software environments are not focused on reproducibility.
Nix and Guix are, as of today, the best tools to manage reproducible software environments.
We hope that \nxc\ will help researchers in distributed systems to transition from non-reproducible tools.
In general, there is the need to educate the communities (not only computer scientists) to these reproducibility questions (\eg\ \cite{tuto_nxc, tuto_nix, mooc_rr1, mooc_rr2}), and develop reproducibility not as a afterthought, but as a fundamental skill.
The cost of experiments in distributed systems, and especially when evaluation grid or cluster middlewares, is significant.
Having realistic experiments implies to perform large scale deployment, which is expensive.
Simulations are a good alternative, but are limited by the fact that the real software is not executed.
Moreover, the state of simulation of parallel file-system is not mature enough to perform the \cigri\ experiments in simulation.
We believe that an interesting alternative is the use of intermediate deployment techniques such as the one presented in Chapter \ref{chap:folding}.

Three concerns of scientific research are: the *explainability* and the *energy efficiency* of the solutions, and the *reproducibility* of research.
This thesis attempted to take into account these concerns, and to raise awareness about them. 

