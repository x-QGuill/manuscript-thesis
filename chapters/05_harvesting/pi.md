# A Proportional-Integral Controller to harvest idle resources {#chap:harvest_fs}

In this Chapter we implement a Proportional-Integral (PI) controller in \cigri\ to regulate the load of a distributed file-system.
As the target audience of this thesis is mainly computer scientists with limited knowledge of Control Theory, this Chapter aims to present the methodology in a pedagogical fashion.
The reader can also go through a more hands-on introduction to Control Theory with our tutorial to introduce Control Theory to computer scientists: \cite{tuto_control}.

<!---
## Hypothesis

\todo{maybe this should be at the end of sota}

In this Chapter we make the following hypothesis.

##### There is only one cluster in the grid

This hypothesis allows us to focus on a single cluster.
The generalization to several clusters can be done by considering one autonomic controller per cluster.
An interesting path to consider would be the affinity between \cigri\ campaigns and the different clusters of the grid, with potentially similar techniques as in \cite{casanova2000heuristics}.

##### \cigri\ cannot kill one of its jobs

We suppose that once a job is submitted by \cigri, \cigri\ cannot kill it.
The opposite case could lead to strange behaviors in the considered signal dynamics. 

##### There are no other \be\ jobs in the system besides \cigri's

Regular users of the cluster can in practice also submit \be\ jobs.
Having both \be\ jobs from \cigri\ and regular users could lead to some hidden competition for the idle resources, and introduce noise in the signals.
As a first step, we consider that regular users cannot submit \be\ jobs.

##### The total number of resources in the cluster is constant

We do not take into account the variation of the availability of the nodes.
Meaning that no node are removed or added to the set of available nodes.
Note that if we can have a dynamic sensor of the number of available resources in the cluster, the remaining of the paper holds.

--->

## Goals, Control Objectives, and Sensors

In this Chapter, we are focusing on the research question *RQ2* seen in Section \ref{sec:rq_ctrl}.
We want to reduce the overhead on the \io\ operations of the regular users of the cluster due to the \cigri\ jobs.
In other words, we want to regulate the impact of the \cigri\ jobs on the shared file-system of a cluster.

### Sensor on the File-System

The overhead on the \io\ operations cannot directly be observed easily.
One way would be to submit a specific job periodically and measure its performance to have an idea of the load of the file-system.
But this would mean submitting an extra job, which might stay in the waiting queue and introduce some delay in the measurement.
Moreover, such a job will also use nodes on the cluster.
We instead will measure indirectly this overhead on \io\ operations by using a sensor on the load of the file-server (\ie\ the load of the machine hosting the file-system).
We decided to use the `loadavg` metric \cite{ferrari1987empirical}, present on every UNIX system.
This metric has some interesting properties.
First, it is already well known by system administrators.
This means that they know what values of this metric are acceptable on their system.
It is also available on every Unix machine under `/proc/loadavg`.
The value of this sensor is updated every 5 seconds which is fast enough compared to the duration of most HPC jobs, see Chapter \ref{chap:cigri_jobs}.
This metric also carries some inertia due to its definition with an exponential filter:

\begin{equation}\label{eq:loadavg}
  Q_i = Q_{i-1} \times \left(1 - e^{-T}\right) + q_{i} \times e^{-T}, Q_0 = 0
\end{equation} 

where, $Q_i$ is the value of the `loadavg` at iteration $i$ and $q_i$ is the number of processes running or waiting for the disk at iteration $i$.
There exists several variations of this metric with different factor of filtering ($T$).
Those factors correspond to the period of consideration for the metric.

```
$  cat /proc/loadavg                                                                      
0.62 0.63 0.58 1/1805 49884
```

The first three values returned correspond respectively to the load averaged over one minute, five minutes, and fifteen minutes.  
The longer the period, the smoother the value of the `loadavg`, but also the slower the response of the sensor to a variation.
In the following, we consider only the first value of `/proc/loadavg`, \ie\ the load averaged on the last minute.


This variation of the `loadavg` metric can however be noisy.
Indeed, as this is a system-wide metric, it might capture behaviors that do not belong to the file-system.
Moreover, this metric works nicely for a distributed file-system such as NFS, where there is a single machine hosting the file-system on the server side.
In the case of parallel file-system, where there are several \io\ nodes, meta-data nodes, etc., it might be more difficult to use this exact metric.
One can average the different `loadavg` values of the different nodes belonging to the file-system, but the aggregated metric would probably lose most of its meaning.
In our case, we are interested in small to medium computing centers, where using NFS as a shared file-system is a perfectly acceptable option. 
The implementation of this sensor is done by opening a `ssh` connection from the machine hosting \cigri\ to the file-system server and periodically reading the content of `/proc/loadavg`:

```
ssh fileserver 'while true; do L=$(cat /proc/loadavg);\
                D=$(date +%s);\
                echo -e $D $L;\
                sleep 5;\
                done' >> /tmp/loadavg_storage_server &
```


In the case of NFS, the configuration defines a number of *workers* that can manage \io\ requests in parallel (8 by default).
Each worker is in its own process, which makes it easy to observe with `loadavg`.
Hence, the maximum acceptable load on the file-server is a value of `loadavg` equal to the number of NFS workers.
Note that the metric can reach an even greater value, but it will not be because of the file-system load.
The minimum load is zero, by definition of the metric.

### Sensors on the Cluster

Only looking at the load of the file-system might lead to some degenerate cases for the closed-loop system.
Imagine the situation where the \cigri\ jobs perform a very small quantity of \io, and that even if all the resources are being used, the load of the file-server is below the reference value.
Then the controller will perceive that it is possible to keep increasing the number of jobs submitted to reach the reference value.
However, as the cluster is already full, the jobs will go in the waiting queue, and the only action of the controller is to fill faster and faster the waiting queue of \oar.
Hence, we also need a sensor on the state of the cluster to avoid such cases.
Fortunately, \oar\ offers an API where we can extract, directly or indirectly, the number of jobs in waiting queue or currently running. 

Note that in order to be reactive to potential overload of the file-system, \cigri\ cannot afford to have a lot of jobs in the waiting queue of \oar.
Indeed, as \oar\ has no notion of file-system load, if there are idle resources, and (fitting) waiting jobs, those jobs will be executed.
The desired value for this sensor would be close to zero.

\begin{lesson}{\cigri\ sensors}{}
To sense the load of the distributed file-system, we use the \texttt{loadavg} metric of the machine hosting the file-system.
For information about the state of the cluster, we use the \oar\ API.
\end{lesson}

## Actuators

At first glance, the only actuator, or knob, at \cigri's disposal to impact the load of the file-system is the amount of jobs it submits at every iteration (every 30 seconds).
However, we can also consider which jobs are in the submission.
Mixing jobs from different campaigns in a single submission could lead to a better control, where the additional knob is the proportion of jobs from each campaign.
In this thesis, we consider only the number of jobs submitted by \cigri\ at each iteration.
The work done in my Master Thesis\ \cite{master_guilloteau, guilloteau:icstcc} explores the proportion of jobs from two different campaigns to improve the control of the load of the file-server, and will be summarized in Section \ref{sec:biphasic}.
Figure \ref{fig:cigri_hor} summarizes the definition of the loop in \cigri\ with the sensors, and the actuator.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{figs/pi/cigri-hor.pdf}
\caption{Graphical representation of the feedback loop in \cigri.}
\label{fig:cigri_hor}
\end{figure}

## Model of the system



<!---
In this Section we present the classical method to design a 
PI
controller.
There exists several types of controllers for several types of applications.
Control Theory practitioners use the Proportional-Integral-Derivative Controller (PID) most of the time.
Three parts constitute the PID Controller.
The Proportional part reacts proportionally to the error.
The Integral part corrects the input to reach the desired value.
The Derivative part foresee variations of the system.

The following equation governs the controller:

\begin{equation}
\begin{aligned}
Input_{k}=K_p& \times Error_k + \\
K_i & \times \sum_k Error_k +\\
K_d &\times \frac{Error_k - Error_{k-1}}{\Delta t}
\end{aligned}
\end{equation}


Where:


\begin{itemize}
\item $Input_k$: the input of the system at iteration $k$
\item $Error_k = Reference - Output_k$: the control error at iteration $k$
\item $K_p, K_i, K_d \in \mathbb{R}$: the proportional, integral and derivative gains
\item $\Delta t$: time between two iterations
\end{itemize}


For a noisy signal, computing the derivative can yield even noisier values.
Filtering the signal smooths the values of the derivative, but also slows down the response of the controller.
This opposes to the goal of the derivative term to foresee the variations.
Thus, practitioners often set the derivative term to zero, yielding a Proportional-Integral controller.

In the following, we design a Proportional-Integral controller for our problem.





### Open-Loop Experiments and Identification {#sec:identification}

--->

As we do not have any a priori knowledge of what would be a model of the system, we perform identification experiments.
The identification aims at finding the relation governing our system.
We intend to determine an expression of the following form:

\begin{equation}\label{eq:model_general}
  y(k + 1) = \sum_{i = 0}^k a_i y(k - i) + \sum_{j = 0}^k b_j u(k - j)
\end{equation}

Where:

\begin{itemize}

\item $y(k)$: output of the system at step $k$. In our case, the load of the file-system
\item $u(k)$: input to the system at step $k$. In our case, the number of jobs sent from \cigri\ to \oar
\item $a_i, b_j \in \mathbb{R}$: coefficients of the model
\end{itemize}

Equation \ref{eq:model_general} represents a *linear model*.
Such a model is simple, but does not necessary fit all the systems.
In some cases, a change of variable is needed to fall back on a linear model.
The model does not need to be *perfect* to have a good control.
An approximated model which captures the behavior of the system might be enough in some cases.


### Identification Experiments

To find the coefficients ($a_i, b_j$), we will study the system in open loop.
This means without feedback from the system.
We change the input and observe the variation in the output.
We submit steps of number of jobs and see the impact on the load of the file-server.
By step of the number of jobs we mean that at time $k, \forall k < k_{step}, u(k) = u_0$ and $\forall k \geq k_{step}, u(k) = u_{step}$, with $u_{step} \gg u_0$.
We then look at the behavior of the file-server load during these steps to extract the coefficients of the model.

In the following, we consider 4 different sizes of files: 25, 50, 75 and 100 MBytes.
For each size of file, we have 6 steps of values: 1, 10, 20, 30, 40 and 50 concurrent jobs.
For each step, we execute 120 consecutive identical submissions of the step.
Note that we will only consider the writing \io\ operation as it is the most costly.

\begin{figure*}
  \centering
  \begin{subfigure}[b]{0.9\textwidth}
       \centering
       \includegraphics[width=\textwidth]{./figs/pi/identification2.png}
       \caption{Time for the NFS file-system to process concurrent write requests (colors) of different sizes (facets).}
       \label{fig:pi:identification:time}
   \end{subfigure}
  \begin{subfigure}[b]{0.9\textwidth}
       \centering
       \includegraphics[width=\textwidth]{./figs/pi/identification_load.png}
       \caption{Load of the machine hosting the NFS file-system through the \texttt{loadavg} metric.}
       \label{fig:pi:identification:load}
   \end{subfigure}
\caption{%
Identification experiments.
For the different file sizes, we write n concurrent files onto the NFS file-system and record the time to process each request (Figure \ref{fig:pi:identification:time}) and the load of the machine hosting the file-system (Figure \ref{fig:pi:identification:load}).
The dashed line on Figure \ref{fig:pi:identification:load} represents the theoretical maximum load of that the NFS server can manage based on its number of workers.
We can see that writing 50 concurrent 100Mbytes files overloads the file-system, the processing time explodes and the load is at the theoretical maximum.
}
\label{fig:pi:identification}
\end{figure*}


Figure\ \ref{fig:pi:identification} represents the results the identification phase.
Figure \ref{fig:pi:identification:time} corresponds to the time to write the files, and Figure \ref{fig:pi:identification:load} depicts the file-server load, with the black dashed line representing its total number of NFS workers.
We observe that increasing the number of simultaneous write requests increases the time to process these requests.
The load of the file-server follows the same pattern.

\begin{lesson}{}{}
The \texttt{loadavg} metric seems to adequately represent the perturbations of the file-server and the overhead on the \io\ operations.
\end{lesson}

Note that for a submission of 50 write requests of 100 MBytes, the processing time skyrockets, and the approximation of the system having a linear behavior is no longer valid.
We see that for such a submission, the load of the file-server reached the dashed line, representing the number of workers in the system.
In this situation, the file-system is overloaded and cannot deal with all the requests.
**This motivates our goal to regulate the load in order not to reach this limit.**
Thus, the `loadavg` metric also provides a way to detect such an overload.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{./figs/pi/identification_patchwork.pdf}
    \caption{Processing time (top) and the file-server load (bottom) for different submissions in number of jobs and \io\ loads. It represents the identification phase. We vary the quantity of \io\ (columns) and the number of simultaneous write requests/jobs in x-axis. We observe that the \texttt{loadavg} sensor captures the (over)load of the file-system.}\label{fig:pi:identification:patch}
\end{figure}


Figure\ \ref{fig:pi:identification:patch} is another representation of Figure \ref{fig:pi:identification} without the temporal dynamic of the system, which will be helpful for modelling the steady-state components of the model of our system.


### Modelling {#sec:model_system}

From the data gathered during the identification experiments in Figure \ref{fig:pi:identification}, we model the relation between the value of the `loadavg` ($y$), the \io\ load ($f$), and the number of jobs ($u$).
We consider the upper bound of the `loadavg` in the modelling to be conservative.
By fitting a linear regression on the open-loop data, we get the following relation:

\begin{equation}\label{eq:fit}
  y =  \alpha + \beta_1  f + \beta_2  u +  \gamma f \times u
\end{equation}

<!---
With:
\begin{itemize}
\item $\alpha = -0.5071484$
\item $\beta_1 = 0.0086335$
\item $\beta_2 = 0.0451394$
\item $\gamma = 0.001633$
\end{itemize}
--->


To design the controller for our system, we need a relation as showed in Equation \ref{eq:model_general}.
As a first approach, we suppose that we are looking for a *first order system*.
The order of the system corresponds to the degree of dependence of $y(k+1)$ to previous values of $y$ (first order: $y(k)$, second order: $y(k)$ and $y(k-1)$, etc.).
First order systems have a limited set of behaviors, which makes their study easier.
Higher order systems have more than one degree of dependence, which increases their complexity but also their realism.
But they can be approximated to a first order system with some hypothesis on their poles\ \cite{hellerstein2004feedback}.

In our case, this means that $\forall i,j > 0, a_i = 0, b_j = 0$ in Equation \ref{eq:model_general}.
We are thus looking for the following relation between the input ($u$) and the output ($y$) at step $k$:

\begin{equation}\label{eq:model_1st_order}
  y(k + 1) = a y(k) + b u(k)
\end{equation}

<!---

We express the z-transform transfer function of this system as:

\begin{equation}
G(z) = \frac{b}{z - a}
\end{equation}

---->

By definition of the `loadavg` metric (Equation \ref{eq:loadavg} and \cite{ferrari1987empirical}), we have $a = \exp \left(-\frac{5}{60}\right)$.
The `loadavg` value updates itself every 5 seconds, faster than a period of \cigri\ ($\Delta t = 30s$).
Thus,

\begin{equation}
a = \left(\exp \left(-\frac{5}{60}\right)\right)^{\frac{\Delta t}{5}}
\end{equation}

In order to find the value of $b$, we must look at the behavior of our system in steady state, meaning when the system has converged.
In a steady state ($ss$), we have the following relation:

\begin{equation}\label{eq:b}
y_{ss} = a \times y_{ss} + b \times u_{ss} \implies b = \frac{y_{ss}\left(1 - a\right)}{u_{ss}}
\end{equation}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{./figs/pi/estimate_b.pdf}
    \caption{Estimation of the parameter $b$ for the model of the system. The bars correspond to the estimation for a number of jobs ($u$) and a \io\ load ($f$). The points represent the value of the estimation when considering $u = +\infty$. We observe that the estimations converge towards these limits values. We will take these limits as values for $b$.}\label{fig:estimation_b}
\end{figure}

Figure \ref{fig:estimation_b} depicts the estimation of $b$ for the values of $u_{ss}$ and $y_{ss}$ from the identification in Figure \ref{fig:pi:identification}.

From Equation \ref{eq:b} and Equation \ref{eq:fit} we get:

\begin{equation}
\begin{split}
\lim_{u \rightarrow + \infty} b & = \lim_{u \rightarrow + \infty} \frac{\left(\alpha + \beta_1  f + \beta_2 u + \gamma f \times u\right) \left(1 - a\right)}{u} \\
                                & = \left(\beta_2 + \gamma f \right) \times \left(1 - a \right)
\end{split}
\end{equation}

This means that the value of $b$ depends on the file size of the current jobs ($f$).
We also plot this limit value of $b$ on Figure \ref{fig:estimation_b} as points.
We can see that the estimations of $b$ converge to this limit value.

To sum up, we pick the model parameters to be:

\begin{equation}\label{eq:model_gains}
\begin{cases}
\begin{aligned}
a & = \left(\exp \left(-\frac{5}{60}\right)\right)^{\frac{\Delta t}{5}} \\
  &= 0.6065307
\end{aligned}\\
\begin{aligned}
b & = \left(\beta_2 + \gamma f\right) \times \left(1 - a \right) \\
  & = 0.017761 + 6.4273488 \times 10^{-4} \times f
\end{aligned}
\end{cases}
\end{equation}


\begin{figure}
     \centering
     \includegraphics[width=\textwidth]{./figs/pi/identification_load_random.png}
      \caption{
Load of the NFS file-system for random steps in the number of concurrent writes.
We observe that for 100Mbytes and 49 concurrent writes, the file-system collapses and fails to continue, which shows the importance of not overloading the distributed file-system of a cluster. 
}
    \label{fig:pi:random}
\end{figure}

We evaluate our model by executing random steps of concurrent writes on the NFS file-system, and log the `loadavg` of the NFS server.
Results can be seen on Figure \ref{fig:pi:random}.
We observe that for 100Mbytes and 49 concurrent writes, the file-system collapses and fails to continue, which shows the importance of not overloading the distributed file-system of a cluster. 


\begin{lesson}{}{}
We performed identification experiments to find a linear model of the first order, linking the jobs submitted by \cigri\ to the load of the file-system (Equations \ref{eq:model_1st_order} and \ref{eq:model_gains}).
\end{lesson}


## Design of the Controller {#sec:design_controller}

We assumed in Section \ref{sec:model_system} that our system is a first order.
We want to design a Proportional-Integral (PI) controller to be able to regulate the load of the file-system with *precision* and *robustness*.
For a PI, there are two gains to set up:

\begin{itemize}
\item $K_p$ is the proportional gain of the controller
\item $K_i$ is the integral gain of the controller
\end{itemize}

These gains are functions of the model of the system (Equations \ref{eq:model_1st_order} and \ref{eq:model_gains}) and two parameters that allow to choose the closed loop behavior of the system:

\begin{itemize}
\item $k_s$ which represents the maximum time for the closed-loop system to get to steady state, and which gives the rapidity of the system, \ie\ the time to react to a variation
\item $M_p$ which represents the maximum overshoot allowed
\end{itemize}

From these value given by the administrators of the system, we can derive the gains for the PI controller\ \cite{hellerstein2004feedback}.

\begin{equation}\label{eq:kpki}
\begin{cases}
  K_p = \frac{a - r^2}{b} \\
  K_i = \frac{1 - 2 r \cos \theta + r^2}{b}
\end{cases}
\end{equation}

Where:

\begin{itemize}
\item $r = \exp \left(- \frac{4}{k_s}\right)$
\item $\theta = \pi \frac{\log r}{\log M_p}$
\end{itemize}



## Choice of the Closed-Loop Behavior {#sec:closed_loop}

\begin{figure}
    \centering
    \includegraphics[width=0.75\textwidth]{./figs/pi/closed_loop_behaviour.pdf}
    \caption{Impact of the $k_s$ and $M_p$ parameters on the Closed Loop Behavior of the system. The smaller $k_s$, the fastest the closed-loop system will converge to the reference value. But too small values of $k_s$ can lead to some overshooting. $M_p$ controls the allowed overshoot. The greater $M_p$, the greater the allowed overshoot.}\label{fig:ksMp}
\end{figure}

Figure \ref{fig:ksMp} shows the influence of the $k_s$ and $M_p$ parameters on the closed loop system.
We now pick the values of $k_s$ and $M_p$ to meet with the desired behavior and extract the gains of the controller from Equation \ref{eq:kpki}.
In our case, we want to avoid any overshoot, and thus avoid overloading the file-system, but still have a fast response.
Hence, we will take $k_s = 12$ and $M_p = 0$.

\begin{lesson}{}{}
We designed a Proportional-Integral Controller from the model identified in the Section \ref{sec:model_system}.
Control Theory allows us to choose the closed-loop behavior of our system (Figure \ref{fig:ksMp}).
\end{lesson}


## Example of Perturbation

\begin{figure}
    \centering
    \includegraphics[width=0.95\textwidth]{./figs/pi/step_pi_single.pdf}
    \caption{Response of the closed-loop system to a step perturbation. The Proportional-Integral controller increases the number of jobs \cigri\ submits to \oar\ (bottom) to get the load to the reference value (top). At $t=2000s$ we introduce a step-shape perturbation resulting in an increase of the load. The controller detects it and decreases the size of the submission to get the load back to the reference value.}\label{fig:step_pi_single}
\end{figure}

Figure \ref{fig:step_pi_single} represents the response of the closed-loop system to a step perturbation.
We ask the controller to regulate the load of the fileserver around the value 6 (dashed line).
At time $t = 2000s$, we simulate a premium user job that would produce a disturbance on the load (dotted and dashed line).
We took a step-shaped disturbance to observe the reaction to an very abrupt change in load and then observe the convergence to a new stable state.
If the disturbance was ramp-based, it will be easier for the controller to cope with it.
We repeat this experiment several times.
Each color on Figure \ref{fig:step_pi_single} represents a single experiment.
We also plot the aggregated mean behavior on these experiments with a continuous line.

We can see that the controller manages to get to the reference value.
When the disturbances start, the controllers detects the rise in load due to the step.
It then decreases the number of jobs sent to \oar\ to get the load back to the reference value.

\begin{figure}
  \centering
  \includegraphics[width = \textwidth]{./figs/pi/mb2.pdf}
  \caption{Overhead on the MADBench2 \io\ benchmark\ \cite{mb2} based on the chosen reference value for our PI controller.}
  \label{fig:pi:mb2}
\end{figure}

In Figure \ref{fig:pi:mb2}, we run the MADBench2 \io\ benchmark\ \cite{mb2} as a priority job while running \cigri\ with the PI controller presented above.
We replayed this scenario with different reference values for the controller.
We confirm that the reference value is the key to the trade-off between the amount of resources harvested (y-axis) and the perturbation (x-axis) on the priority applications (MADBench2 here).

## Limitations and Improvements


Proportional-Integral controllers have mathematically proven properties in particular for their convergence, robustness to disturbances, etc.
But they do also have limits.
Indeed, they rely on the system model, defined in Equation \ref{eq:model_1st_order}.
In our case, the model depends on the jobs sent by \cigri.
Hence, when a new campaign starts, the model needs to change.
In practice, we do not know the quantity of \io\ per job and thus the model.
There exists ways to deal with this issue.
First, we could measure this quantity of \io.
But all HPC systems do not provide such metrics as they require metrics collectors such as Colmet\ \cite{colmet} or DCDB\ \cite{netti2019facility}.
As \cigri\ campaigns gather lots of jobs and as they have similar behaviors, we could submit a subset of these jobs and compute an estimation of their \io s ($f$ in Equation \ref{eq:model_gains}).
Then we would adapt the model with this estimation and start the controller with the newly computed gains.
Or, we could perform an online estimation of the model parameters, in particular $b$.

The remaining of this chapter presents some modifications of the feedback loop to improve the behavior of the closed loop system.

### Dynamic Reference Value

In this chapter, we showed that a PI controller was able to track correctly a reference value for the load of the file-system.
As we can see at t = 2500s on Figure \ref{fig:step_pi_single}, the response time could lead to an overload of the file-system if the reference value is too high.
The choice of the reference value is thus crucial.
A too low reference value would lead to a poor harvesting of idle resources, and a high reference value would increase the overhead on priority jobs while increasing the chances of a collapse of the file-system due to an overload.

\begin{figure}
  \centering
  \begin{tikzpicture}[scale=0.6]
    \draw [->] (-5, 5) -- (-2.9, 5) node[pos=.2, above] {$y_{\max}$};
    \draw [->] (-2.1, 5) -- (-0.4, 5) node[pos=.5, above] {$y_{ref,k}$};
    \draw [->] (0.4, 5) -- (2, 5) node[pos=.5, above] {$e_k$};
    \draw (2, 6) rectangle (6, 4) node[pos=.5] {Controller};
    \draw [->] (6, 5) -- (8, 5) node[pos=.5, above] {$u_{k}$};
    \draw (8, 6) rectangle (12, 4) node[pos=.5] {System};
    \draw [->] (12, 5) -- (16, 5);
    \draw [->] (14, 5) -- (14, 3) -- node[pos=0.5, above]{$y_k$} (0, 3) -- (0, 4.6);

    \draw (0, 5) circle (.4);
    \draw [-] (0.28, 5.28) -- (-0.28, 4.72);
    \draw [-] (-0.28, 5.28) -- (0.28, 4.72);
    \draw (.5, 4.5) node {-};
    \draw (-.3, 5.5) node {+};

    \draw (-2.5, 5) circle (.4);
    \draw [-] (-2.22, 5.28) -- (-2.78, 4.72);
    \draw [-] (-2.78, 5.28) -- (-2.28, 4.72);
    \draw (-3.2, 5.3) node {+};
    \draw (-2.8, 5.5) node {-};

    \draw [->] (10, 8.5) -- node[above,pos=0.01] {Disturbances} (10, 6);
    \draw [->] (10, 7) -- node[pos=0.5, above]{$d_k$}(4, 7);
    \draw [->] (1, 7) -- node[pos=0.5, above]{$y_{prio,k}$}(-2.5, 7) -- (-2.5, 5.4);
    \draw [->] (2.5, 8.5) -- node[above,pos=0.01] {$\bar{f}$} (2.5, 7.5);
    \draw (1, 6.5) rectangle (4, 7.5) node[pos=.5] {$f_{model}$};



    \end{tikzpicture}
  \caption{
Feedback loop representing the control scheme with the dynamic reference value.
The current number of premium jobs ($d_k$) is fed into a model returning the maximum load that the premium jobs could put on the file-system.
This load is then subtracted to the maximum load for the file-system, and then is defined as the reference value for the \cigri\ controller.
The value of $\bar{f}$ represents a representative file size for the cluster and is chosen by the administrators of the cluster.
This information can be retrieved with Darshan \cite{darshan1} for example.
It could be the mean or median file size, or the 95\% percentile to be more conservative.
}
  \label{fig:pi:ref_loop}
\end{figure}

One approach would be to dynamically change the reference value based on the number of premium jobs currently running on the cluster.
If there are no premium jobs, then the reference value is high to allow \cigri\ to harvest.
If there are some premium jobs, then the reference value is decreased to leave more room for the controller to react.
The amount to reduce the reference value depends on how many nodes are used by priority jobs, and prior global knowledge of the \io\ load of premium jobs.

Figure \ref{fig:pi:ref_loop} gives an idea of the control loop.
The quantity $d_k$ represents the number of resources used by premium jobs at iteration $k$.
$f_{model}$ is the estimated maximum load that can be produced by all the premium jobs simultaneously.
It can be found by performing an identification experiment where, instead of having steps inputs as previously, we have Diracs.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{./figs/pi/dirac.png}
  \caption{
Identification experiment to model the maximum load (y-axis) that N concurrent writes (color) of a given size (facets) on the distributed file-system.
After performing concurrent write requests, we wait for the load of the machine hosting the file-system to reach a near zero value before executing the next write requests.
}
  \label{fig:pi:dirac}
\end{figure}

Figure \ref{fig:pi:dirac} shows the results of this new identification experiment.
For different file sizes, we write concurrently to the distributed file-system and observe the load of the machine hosting the file-system.
We then wait for the load to reach (near) zero and write again.
From this experiment we extract a model of the maximum load ($f_{model}$) based on the file size and the number of concurrent writes using a linear regression.
Using this model, we can define the reference value for the load of the file-system:

\begin{equation}
y_{ref,k} = y_{max} - y_{prio_k} = y_{max} - \max(0, y_{max} - f_{model}(d_k, \bar{f}))
\end{equation}

The value of $\bar{f}$ represents a representative file size for the cluster.
This information can be retrieved with Darshan \cite{darshan1} for example.
It could be the mean or median file size, or the 95\% percentile to be more conservative.
The administrators of the cluster would set the value of $\bar{f}$.

\begin{lesson}{}{}
With extra information from the premium jobs, we can adapt the reference value dynamically to reduce the risks of a collapse of the file-system.
\end{lesson}


### Tagging \io\ intensive campaigns {#sec:biphasic}

This section summarizes the work done during my master thesis\ \cite{master_guilloteau} and published in \cite{guilloteau:icstcc}.
Originally, \cigri\ submits batches of jobs from the same campaign to \oar.
If the quantity of \io\ done in each job of the same campaign is similar, it differs between campaigns of different projects.
Suppose that there are two campaigns submitted to \cigri, one with high \io\ load jobs and the other with light \io\ load jobs.

\begin{figure}
\centering
\begin{tikzpicture}[scale=1.3]
    \draw (0, 1.4) -- node[above] {Load Reference} (7, 1.4);
    \draw [->] (0, -.1) -- node[below] {Time} (8, -.1);
    \draw [->] (-.1, 0) -- node[left] {Load} (-.1, 1.5);
    % HEAVY
    \filldraw[pattern=north west lines, pattern color=red](0, 0.0) rectangle (3, 0.3);
    \filldraw[pattern=north west lines, pattern color=red](0, 0.3) rectangle (3, 0.6);
    \filldraw[pattern=north west lines, pattern color=red](0, 0.6) rectangle (3, 0.9);
    \filldraw[pattern=north west lines, pattern color=red](0, 0.9) rectangle (3, 1.2);
    % LIGHT[pattern=north east lines, pattern color=blue]
    \filldraw[pattern=north east lines, pattern color=blue](3, 0.0) rectangle (6, 0.1);
    \filldraw[pattern=north east lines, pattern color=blue](3, 0.1) rectangle (6, 0.2);
    \filldraw[pattern=north east lines, pattern color=blue](3, 0.2) rectangle (6, 0.3);
    \filldraw[pattern=north east lines, pattern color=blue](3, 0.3) rectangle (6, 0.4);
    \filldraw[pattern=north east lines, pattern color=blue](3, 0.4) rectangle (6, 0.5);
    \filldraw[pattern=north east lines, pattern color=blue](3, 0.5) rectangle (6, 0.6);
    \filldraw[pattern=north east lines, pattern color=blue](3, 0.6) rectangle (6, 0.7);
    \filldraw[pattern=north east lines, pattern color=blue](3, 0.7) rectangle (6, 0.8);
    \filldraw[pattern=north east lines, pattern color=blue](3, 0.8) rectangle (6, 0.9);
    \filldraw[pattern=north east lines, pattern color=blue](3, 0.9) rectangle (6, 1.0);
    \filldraw[pattern=north east lines, pattern color=blue](3, 1.0) rectangle (6, 1.1);
    \filldraw[pattern=north east lines, pattern color=blue](3, 1.1) rectangle (6, 1.2);
    \filldraw[pattern=north east lines, pattern color=blue](3, 1.2) rectangle (6, 1.3);
    \filldraw[pattern=north east lines, pattern color=blue](3, 1.3) rectangle (6, 1.4);
    % Submission
    \draw [->, dashed] (0, -1) -- node[below, pos=-0.2] {Submission} (0, -.3);
    \draw [->, dashed] (3, -1) -- node[below, pos=-0.2] {Submission} (3, -.3);
\end{tikzpicture}
\caption{Submissions with jobs from single campaign}
\label{fig:heavy}
\end{figure}

Figure \ref{fig:heavy} represents a situation using submissions composed of jobs from the same campaign.
If we suppose that we are able to regulate perfectly the number of jobs submitted for the red campaign
to keep the load of the cluster under the reference load, there will be a "gap" between the actual load
of the cluster and the reference load.
We could exploit this "gap" by submitting jobs that have a smaller impact on the load (blue jobs).
Figure \ref{fig:mixed_example} shows a situation where we submit a set of jobs coming from two different campaigns
to improve the cluster usage, the number of resources used while keeping the load under the reference value.

\begin{figure}
\centering
\begin{tikzpicture}[scale=1.3]
    \draw (0, 1.4) -- node[above] {Load Reference} (4, 1.4);
    \draw [->] (0, -.1) -- node[below] {Time} (5, -.1);
    \draw [->] (-.1, 0) -- node[left] {Load} (-.1, 1.5);
    % HEAVY[pattern=north west lines, pattern color=blue]
    % \filldraw[color=red, draw=black] (0, 0.0) rectangle (3, 0.3);
    % \filldraw[color=red, draw=black] (0, 0.3) rectangle (3, 0.6);
    % \filldraw[color=red, draw=black] (0, 0.6) rectangle (3, 0.9);
    % \filldraw[color=red, draw=black] (0, 0.9) rectangle (3, 1.2);
    \filldraw[pattern=north west lines, pattern color=red](0, 0.0) rectangle (3, 0.3);
    \filldraw[pattern=north west lines, pattern color=red](0, 0.3) rectangle (3, 0.6);
    \filldraw[pattern=north west lines, pattern color=red](0, 0.6) rectangle (3, 0.9);
    \filldraw[pattern=north west lines, pattern color=red](0, 0.9) rectangle (3, 1.2);
    % LIGHT[pattern=north west lines, pattern color=blue]
    \filldraw[pattern=north east lines, pattern color=blue](0, 1.2) rectangle (3, 1.3);
    \filldraw[pattern=north east lines, pattern color=blue](0, 1.3) rectangle (3, 1.4);
    % Submission
    \draw [->, dashed] (0, -1) -- node[below, pos=-0.2] {Submission} (0, -.3);
\end{tikzpicture}
\caption{Submission with jobs from several campaigns}
\label{fig:mixed_example}
\end{figure}

A \cigri\ campaign is represented by a JSON file.
We extended the definition of this JSON file to include a boolean field to the \io\ "heaviness" of the jobs of the submitted campaign. 
We suppose that at any moment, there are at least one \io\ heavy campaign and one \io\ light campaign.
This consideration gives us one additional knob of action on the system: the proportion of jobs from each type of campaign in the batch of jobs to submit to \oar.

Changing the number of jobs in the submission has a bigger impact than changing the percentage of \io\ heavy jobs in the submission.
Thus, to regulate the load of the file-system precisely, we should
do these actions in the following order:

\begin{enumerate}
\item Change the number of jobs submitted to \oar
\item Change the percentage of \io\ heavy jobs in the submission
\end{enumerate}

The controller will have two modes running in exclusion.
The idea could be summed up as "big step, small step".
The controller will firstly regulate the number of jobs sent to \oar\ (big step).
Then, when the load of the file-system is "close" to the reference value, the controller will regulate the proportion of \io\ heavy jobs in the submission (small step).
Figure \ref{fig:feedback:biphasic} gives a graphical representation of the controller.


\begin{figure}[!h]
\centering
\begin{tikzpicture}[scale=0.5]
\draw [->] (-6, 5) -- (-1.4, 5) node[pos=.2, above] {Load Reference};
\draw [->] (-0.6, 5) -- (2, 5) node[pos=.5, above] {e};
\draw (2, 6) rectangle (4, 4) node[pos=.5] {$e$ ?};
\draw (5, 3) rectangle (9, 1) node[pos=.5] {Nb jobs};
\draw [->] (3, 6) -- (3, 8) -- node[pos=.5, above] {$<T$} (5, 8);
\draw (5, 9) rectangle (9, 7) node[pos=.5] {\% IO heavy};
\draw [->] (3, 4) -- (3, 2) -- node[pos=.5, above] {$\geq T$} (5, 2);
%\draw [->] (6, 5) -- (8, 5) node[pos=.5, above] {u};
\draw (12, 6) rectangle (16, 4) node[pos=.5] {System};
\draw [->] (9, 2) -- (11, 2) -- (11, 5) -- (12, 5);
\draw [->] (9, 8) -- (11, 8) -- (11, 5) -- (12, 5);
\draw [->] (16, 5) -- (20, 5) node[pos=.5, above] {Output};
\draw (5, 0) rectangle (9, -2) node[pos=.5] {Load Sensor};
\draw [->] (18, 5) -- (18, -1) -- (9, -1);
\draw [->] (5, -1) -- node[pos=.5, above] {Load} (-1, -1) -- (-1, 4.6);
\draw (-1, 5) circle (.4);
% \draw [-] (0.28, 5.28) -- (-0.28, 4.72);
\draw [-] (-0.72, 5.28) -- (-1.28, 4.72);
\draw [-] (-1.28, 5.28) -- (-0.72, 4.72);
\draw (-0.5, 4.5) node {-};
\draw (-.3, 5.7) node {+};
\end{tikzpicture}
\caption{Representation of the feedback loop for a submission with different campaigns with different \io\ loads.}
\label{fig:feedback:biphasic}
\end{figure}



The choice of the threshold value ($T$ in Figure \ref{fig:feedback:biphasic}) between the two modes is not easy, but we give some guidelines below.

If $T$ is too small, then it will be more difficult to get into the mode regulating the percentage of \io\ heavy jobs, and there might be an unavoidable static error plus some oscillations.
Figure \ref{fig:threshold:small} gives a visual representation of this situation.

If $T$ is too large, the controller might get "stuck" in the mode regulating the percentage of \io\ heavy jobs.
This could lead to reaching a non-optimal stationary state.
Figure \ref{fig:threshold:big} gives a visual representation of this situation, where the percentage of \io\ heavy jobs reached 100\% and the controller cannot get closer to the reference value.
In\ \cite{guilloteau:icstcc}, we choose a threshold value of 1 ($T = 1$) as experiments have shown not to be too small nor too big.


\begin{figure*}
    \centering
    \begin{subfigure}{0.49\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figs/m2report/mixed_campaign_threshold_too_small.pdf}
    \caption{Situation where a too small threshold value could lead to oscillations in the system and an imprecision.}
    \label{fig:threshold:small}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figs/m2report/mixed_campaign_threshold_too_big.pdf}
    \caption{Situation where a too large threshold value could lead to an unavoidable static error.}
    \label{fig:threshold:big}
    \end{subfigure}
    \caption{Graphical representation of the potential behavior of the load if the threshold is too small (Figure \ref{fig:threshold:small}) or too big (Figure \ref{fig:threshold:big}).}
    \label{fig:threshold}
\end{figure*}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{./figs/m2report/biphasic.pdf}
  \caption{
Example of regulation of the load of the NFS file-system by considering \io\ heavy jobs and \io\ light jobs.
The dashed lines on the bottom plot represent the threshold between the two modes.
The solid line is the reference value.
We can see that the controller first enters the threshold zone around 700 seconds, and then starts regulating the percentage of \io\ heavy jobs in the submission (top plot).
}
  \label{fig:biphasic}
\end{figure}

Figure \ref{fig:biphasic} depicts and example of regulation of the load of the NFS file-system by considering \io\ heavy jobs and \io\ light jobs.
The dashed lines on the bottom plot represent the threshold between the two modes.
The solid line is the reference value.
We can see that the controller first enters the threshold zone around 700 seconds, and then starts regulating the percentage of \io\ heavy jobs in the submission (top plot).

Experiments showed that considering \io\ heavy and \io\ light campaigns allows to improve the usage of the cluster, while having slightly better control on the load of the file-system\ \cite{guilloteau:icstcc}.

\begin{lesson}{}{}
By considering jobs from campaigns with different \io\ loads, it is possible to improve the usage of the machines.
\end{lesson}
