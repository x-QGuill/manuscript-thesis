## The \cigri\ Middleware \& its need for regulation {#sec:presentation_cigri}

This section gives more details about \cigri, the middleware of interest in this thesis.

### The \gricad\ Computing Center

The \gricad\ computing center\footnote{\url{https://gricad.univ-grenoble-alpes.fr}} provides computing and storing infrastructures to the researchers of the region of Grenoble, France.
The center is composed of several computing clusters, each with a focus (\eg\ `luke` for data processing, `froggy` for HPC, or `dahu` for HPCDA\footnote{\emph{High Performance Computing and Data Analysis}}).
These clusters are linked together to form a computing grid.
This grid is also impacted by the issue of unused resources explained above. 

### The \cigri\ middleware

\cigri\ \cite{cigri, cigri_sw} is a computing grid middleware set up in the \gricad\ computing center.
It interacts with the \oar\ schedulers \cite{capit_batch_2005, oar3} of each cluster.
The goal of \cigri\ is to use the idle resources of the entire computing grid.
Originally, \cigri\ was designed to reduce the stress on the different RJMSs of the grid when users had to execute large campaigns (tens of thousand of jobs or more).

Users of \cigri\ submit \bag\ applications to the middleware.
These applications are composed of numerous small, identical, and independent tasks making them perfect candidates for the harvesting the idle resources.
An example of such application is Monte-Carlo simulations, where the user will execute a large batch of random independent experiments to conclude on the aggregated results.
 
Once the application submitted to the middleware, \cigri\ will submit batches of jobs to the clusters of the grid.
The jobs are submitted to the schedulers with the lowest priority (\be), which allows \oar\ to kill those \be\ job if a normal/premium user needs the resources.
Figure \ref{fig:cigri} summarizes the interaction between \cigri\ and the different schedulers of the computing grid.

\begin{figure}
  \centering
  \begin{subfigure}{0.49\textwidth}
    \centering
    \includegraphics[width=\textwidth]{./figs/cigri_oar.pdf}
    \caption{Graphical representation of the system with \cigri\ and the different \oar\ schedulers of a computing grid.}
    \label{fig:cigri}
  \end{subfigure}
  \begin{subfigure}{0.49\textwidth}
    \centering
    \includegraphics[width=\textwidth]{./figs/cigri-oar.pdf}
    \caption{Graphical representation of the interaction between \cigri\ and a \oar\ scheduler.}
    \label{fig:cigri-oar}
  \end{subfigure}
  \caption{Interactions between \cigri\ and the different RJSMs of the grid.}
  \label{fig:cigri-figures}
\end{figure}



### \cigri\ jobs

The hierarchy of \cigri\ jobs is as follows: a *job* belongs to a *campaign*, and a *campaign* belongs to a *project*.
From the point-of-view of \cigri, a *campaign* is a \bag\ application.
One example of project is the processing of massive GPS data for the deformation of the Earth surface \cite{deprez2018toward}.
There are several geographic stations, with a lot of GPS data.
The processing of each station can be grouped as a *campaign*, where the processing of a subregion is a *job*. 
Chapter \ref{chap:cigri_jobs} will present in more details the statistical characteristics \cigri\ jobs.

### The Need for Regulation

The current submission algorithm of \cigri\ works as a "tap".
\cigri\ first submits a batch of \be\ jobs to the cluster scheduler, and then waits for all the submitted jobs to terminate before submitting again.
Figure \ref{fig:cigri-oar} represents the sequence diagram between \cigri\ and \oar.
The size of the batch is defined by the simple ad-hoc Algorithm \ref{algo:original}.

\begin{algorithm}
\SetAlgoLined
\SetKwInOut{Input}{Input}
\Input{$rate$ (init. 3),\\ $increase\_factor$ (constant 1.5)
}
\eIf{no running jobs}{
 $rate$ = min($rate$ $\times$ $increase\_factor$, 100)\;
 submit $rate$ jobs\;
} {
  submit 0 job\;
}
 \caption{%
\cigri\ current job submission.
It submits jobs like a tap: opens the tap and submits jobs to \oar.
Then closes the tap and waits for all the jobs to be executed.
Then it opens the tap again and submits jobs.
The values of $rate$ and $increase\_factor$ have been empirically chosen by the administrators of \cigri.
}\label{algo:original}
\end{algorithm}


One can think of scenarios where such a submission algorithm can lead to both the idling of resources or the unnecessary killing of \be\ jobs.
For example, we can think of a situation where there are a lot of idle resources on the cluster, but a single job from the previous submission is still running and thus \cigri\ will not submit new jobs, as it must wait for this last job to terminate.

\begin{lesson}{}{}
The harvesting capabilities of \cigri\ can be improved by integrating feedback mechanism in its decision process.
\end{lesson}

We believe that the utilization of the system can be improved by introducing feedback mechanism to the submission decision process.
In particular, we think that using an Autonomic Computing \cite{kephart2003vision} point-of-view coupled with tools from \CT\ can yield an improvement in the total utilization of the platform.
In this Thesis, we will focus on two regulation problems of \cigri.


#### Overload of the file-system

One regulation problem arises when considering the file-system of the cluster.
As \cigri\ jobs are mostly short, and that no useful job does not either read or write to the file-system, then having too many \cigri\ jobs running can lead to an overload and a potential collapse of the distributed file-system.
Such a problem is not taken into account in \oar\ for the scheduling of job, but could be with solutions such as \cite{jeannot2023io}.
However, the performance of I/O-aware scheduling techniques are not yet satisfactory, and are not being implemented in production.
They usually need either instrumentation with Darshan\ \cite{darshan1, darshan2} for example, or guesses from the users. 
Previous work has been done in \cite{yabo2019control} by implementing a Model-Predictive controller, but the complexity of the solution, errors and scale of the experimental setup (10 nodes) yield a model that did not scale correctly.
Hence, there is the need to keep exploring different control-based approaches. 

<!---
In Chapter \ref{chap:harverst_fs}, we will design two different controllers from Control Theory (\todo sections) and compare their performance.
We will also take a look the reusability aspect of the controllers implemented, and the trade-off between reusability and performance.
--->

<!---
### Unnecessary killing of \cigri\ jobs
--->
#### Avoid loss of computing power from killing low priority jobs

In the case where we do not consider the distributed file-system of the cluster, one can think that using the all the resources of the cluster is easy and that \cigri\ "just" needs to keep enough jobs in the waiting queue of \oar\ to fill the entire cluster if available.
Even if this represents a regulation problem of its own (addressed in \cite{stahl2018towards}), it only considered idle resources to be wasted computing power.
However, killed \cigri\ jobs also represent wasted computed power as \cigri\ jobs do not use check pointing techniques.
The campaigns submitted by the users contain a given work to be executed, and will not be completed until all the work has been done.
Thus, getting \cigri\ jobs killed is also counterproductive.

<!---
In Chapter \ref{chap:ff}, we will regulate the submission of \cigri\ jobs with the objective to minimize the computing power lost to the idle resources and to the killing of \cigri\ jobs. 
--->

\begin{lesson}{Limitations of \cigri}{}
\cigri\ is able to harvest idle resources of a set of clusters, but does not take into account the state/load of the clusters.
Applying Autonomic Computing with Control Theory tools could improve the resources' utilization.
\end{lesson}

## Hypotheses of this thesis

In this work, we make the following hypotheses:

##### There is only one cluster in the grid

This hypothesis allows us to focus on a single cluster.
The generalization to several clusters can be done by considering one autonomic controller per cluster.
An interesting path to consider would be the affinity between \cigri\ campaigns and the different clusters of the grid, with potentially similar techniques as in \cite{casanova2000heuristics}.


##### \cigri\ cannot kill one of its jobs

We suppose that once a job is submitted by \cigri, \cigri\ cannot kill it.
The opposite case could lead to strange behaviors in the considered signal dynamics. 

##### There are no other \be\ jobs in the system besides \cigri's

Regular users of the cluster can in practice also submit \be\ jobs.
Having both \be\ jobs from \cigri\ and regular users could lead to some hidden competition for the idle resources, and introduce noise in the signals.
As a first step, we consider that regular users cannot submit \be\ jobs.

##### The total number of resources in the cluster is constant

We do not take into account the variation of the availability of the nodes.
Meaning that no node are removed or added to the set of available nodes.
Note that if we can have a dynamic sensor of the number of available resources in the cluster, the remaining of this thesis should be easily adaptable.

<!---
#### The walltime of the submitted jobs is exactly their processing time
--->
