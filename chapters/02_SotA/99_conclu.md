## Conclusion \& Research Questions {#sec:rq_ctrl}

HPC systems are not making use of all their resources.
This is due to the rigid nature of the jobs.
The idle computing power can be harvested by executing smaller and interruptible jobs.
However, those jobs can introduce perturbations in the system and degrade the performances of the premium users jobs.
We believe that the harvesting of idle resources represents a regulation problem which can be tackled via an Autonomic Computing approach.
To provide a runtime behavior with guarantees, we think that using tools and methods from Control Theory will help in terms of performance and adoption.
The \cigri\ middleware represents a good framework to investigate these ideas and methods.

The remaining of this Part presents several autonomic feedback loops in the \cigri\ middleware to harvest the idle resources of an HPC cluster while considering the degradation of premium users' jobs performance.
Chapters of this Part are articulated by the following *research questions* (RQ):

- **RQ1**: Can we characterize the \cigri\ jobs? How many jobs are in a campaign? How long is their execution time?

- **RQ2**: Can we design a controller with the methodology of Control Theory to harvest the idle computing resources of a cluster while regulating the load of a distributed file-system despite disturbances and unpredictability?

- **RQ3**: Controllers are usually tightly tuned for a specific system. To what extent can we plug a controller designed on a system A, on a system B?

- **RQ4**: Can a controller in \cigri\ reduce the computing power lost due to both idle resources and the killing of low priority jobs with some internal decision-making information from the scheduler?

A statistical study of the \cigri\ jobs from the last 10 years exploring **RQ1** is presented in Chapter \ref{chap:cigri_jobs}.
The **RQ2** will be addressed in Chapter \ref{chap:harvest_fs} with the implementation of a Proportional-Integral controller following the methodology of Control Theory.
Chapter \ref{chap:reuse} investigates **RQ3** by comparing the controller defined in Chapter \ref{chap:harvest_fs} and two others types of controllers on their design cost and performance.
We present a first step for answering **RQ4** in Chapter \ref{chap:ff}, and conclude Part I in Chapter \ref{chap:conclu_ctrl}.
