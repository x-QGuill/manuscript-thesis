## Harvesting {#sec:sota:harvesting}

The motivation for harvesting idle computing resources is two-fold.
First, as computers are expensive and are powered on, it would be a waste of energy and money to leave them idle.
Second, sparse idle machines represent a cheap way to expand the computing power available to users. 

In this Section, we present various techniques from the literature to harvest idle resources of a set of machines in different contexts. 

### Personal Computers

<!---
Personal computers represent a great source of computing power.
--->

Volunteer Computing\ \cite{mengistu2019survey} frameworks such as BOINC\ \cite{anderson_boinc_2004} provide a way for anyone to lend their idle CPU times to community scientific projects.
These scientific project range from extraterrestrial intelligence search with SETI\@HOME \cite{anderson_setihome_2002}, climate prediction\ \cite{stainforth2004climateprediction}, or simulating protein dynamics\ \cite{larson2009folding} which was helpful in the fight against Covid-19\ \cite{forti2021fight}.
The computation done by these projects are *embarrassingly parallel*, meaning that it is easy to separate the problem into smaller and independent sub-problems which can then be sent to the users.
This characteristic make them a good candidate to exploit idle computing resources.
Users install the framework on their machine and decide how many idle CPU time to donate to which project.
When the machine is about to go idle, the volunteer computing framework pulls some work (\ie\ a piece of computation) from the server of the community project.
The user machine then executes this work.
If it finishes the previous task, and is still idle (\ie\ not used by the user), it requests a new task.
In the case where the computer is no more idle during the execution of a task, the task is killed and the CPU resources given back to the user.

A different approach is to push computation onto idle personal machines as with Condor\ \cite{litzkow1987condor} for example.
Users register their machines onto the Condor network, making their idle time available.
Users can submit their computations to Condor, which will then find available machines on the network to execute these computations.
The goal is to create a single computing cluster out of idle machines and make the machines available thought a scheduling mechanism.
If a machine is no longer available while a Condor job is executing, the job is killed and put back in the Condor queue.
The difference between Condor and Volunteer Computing is the nature of the workloads executed on the machines.
Condor's workload is not necessarily embarrassingly parallel, but might be a big single job that is simply too resource demanding to be executed on a single machine.


<!---
\todo{NIMROD ?}
--->

### In the Cloud


The cloud is also interested in using idle resources as every idle CPU tick is money lost.
Amazon Web Services (AWS) introduced a 90\% cheaper deal to use the idle resources of its EC2 cloud\ \cite{aws_ec2, aws_ec2_spot}.
Users submit their computations, and AWS will schedule them on appropriated available idle resources.
Those computations are seen as second class citizens and will be interrupted when EC2 reclaims the resources.
In \cite{shastri2016transient}, the authors propose a pricing for idle cloud resources based on a probabilistic model of the potential revocation of the resources.
The goal is to encourage the usage of the idle resources thanks to a fair pricing based on the offered guarantees.
\cite{liu2020game} approaches the problem with a game-theory strategy to dynamically define the price of cloud resources.
The model the problem as a non-cooperative game between the cloud provider and the customers.
The authors of \cite{wang2021smartharvest} use online learning to predict the demand of users and compute the amount of resource that second class virtual machines can safely harvest.


### On High-Performance Computing Clusters


HPC systems are also victims of idle resources.
One solution to reduce the idle time would be to have finer, more precise, scheduling algorithm.
But as smart as the scheduling algorithm could be, humans are still submitting the jobs.
In particular, humans are giving the estimation of the duration of their computation (\ie\ the job walltime), and we are notoriously bad at it\ \cite{cirne2001comprehensive, mu2001utilization}.
Work has been done to improve the quality of the walltime estimations for the schedulers, but they forget other sources of variations (\eg\ network contention, file-system load, etc.).
HPC systems are too dynamic and variable for scheduling to be the only solution.
This section presents different approaches to harvest the idle resources HPC clusters.

OurGrid \cite{cirne2006labs} aims to connect several geographically distributed clusters to a common grid interface.
Users then have more available machines, and can make use during the day of currently idle machines in an opposing time-zone.
This approach relies heavily on trust, as people from different laboratories can access any clusters, which raises some security questions.
The harvesting of the idle resources is thus done manually by scientists that need more machines.

The work on the HPC/Big-Data convergence led to some solutions using Big-Data workloads to make use of the idle HPC resources.

In \cite{souza2019hybrid}, the authors propose a co-scheduling approach based on the Slurm \cite{goos_slurm_2003} and Mesos resources managers\ \cite{hindman2011mesos}.
The solution profiles the running application, and adapts the Linux control groups (`cgroup`) limits, \ie\ the amount of given resources at the operating system level, to fit the available resources to the actually needed resources.
The remaining resources (from the cgroup point-of-view) are then available to the scheduler.
This solution managed to reduce the overall resource usage and makespan, but does require a change of architecture for systems administrators, as well as setting up profiling.

Bebida \cite{mercier2017big} is another approach coupling HPC jobs and Big-Data workflows.
The authors slightly modified the prologue and epilogue scripts of the \oar\ batch scheduler\ \cite{capit_batch_2005} to start and stop Hadoop\ \cite{shvachko2010hadoop} workers on the HPC resources when an HPC job finishes or starts. 
The idle HPC resources are then seen as a dynamic resource pool from the point-of-view of the Big-Data resource manager.
Authors showed that Bebida improved the total usage of the machines, but degraded the mean waiting time of HPC jobs (due to the longer prologue and epilogue scripts).

Similarly to Bebida, the authors of \cite{przybylski2022using} propose a solution to use the idle resource of a cluster using FaaS (Function as a Service), or Serverless, workloads.
The jobs from such workloads last a few seconds, which make them good candidates to exploit the "holes" in the schedule.
The solution couples the resources managers Slurm and OpenWhisk\ \cite{openwhisk}.
One limitation of the approach is the actual usage of FaaS for scientific workflows.
The HPC community is conservative and the adoption of such complementary tools might be long\ \cite{spillner2018faaster}.

\cigri\ \cite{cigri} is the approach that we will focus on in this thesis.
\cigri\ is a grid middleware that runs on top of several computing clusters managed by the \oar\ resources manager.
It harvests the idle resources of the grid's clusters by periodically submitting jobs to the different resources managers with the lowest priority (\be).
Those jobs come from Bag-of-Tasks applications, which are composed of numerous, small, independent, and similar jobs.
Contrary to the previous solutions, the jobs submitted to \cigri\ are HPC jobs (just with different "shapes"), and there is a single scheduler decision at the cluster level.
The limitation of \cigri\ is its submission algorithm which does not take into account the state of the cluster.
This lack of feedback can lead to suboptimal resource usage and contention on shared resources of the clusters.
A more detailed presentation of \cigri\ is done in Section\ \ref{sec:presentation_cigri}.

\begin{lesson}{Harvesting}{}
HPC systems are victim to idle resources.
Those resources are usually harvested by executing smaller, interruptible jobs.
Solutions from the literature usually involve two levels of scheduling and jobs from two different natures.
% \todo{There is thus the need for a ...} 
\end{lesson}
