# Discussion \& State-of-the-Art {#chap:sota}

The first part of this Thesis is related to three areas: the harvesting of idle computing resources in High Performance Computing systems (Section \ref{sec:sota:harvesting}), Autonomic Computing (Section \ref{sec:sota:ac}), and Control Theory (Section \ref{sec:sota:ct}).
Section \ref{sec:presentation_cigri} presents \cigri, the case-study of this thesis.
