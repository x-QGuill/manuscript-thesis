## Control Theory {#sec:sota:ct}


<!---
### Methodology
--->

This Section summaries the classical methodology of applying tools from Control Theory to computing systems \cite{filieri2017control, litoiu2017can, rutten2017feedback}.

For the sake of clarity, we will use the following example.
Imagine you have a brand-new CPU which is quite powerful, but can become very hot.
High temperatures can damage the CPU, and you do not want to buy a new one (it was very expensive).  
In this example, we will go through the methodology to set up a controller on your CPU to avoid breaking it.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{figs/control_methodo.pdf}
  \caption{Workflow of the Control Theory methodology \cite{cerf2021sustaining}.}
  \label{fig:control_methodo}
\end{figure}

Figure \ref{fig:control_methodo} presents the workflow for design a controller.
Note that there can be cycles in the workflow where the objectives, knobs, sensors, etc., can be updated.


### Identify the goals

The first step is to define what are the objectives of the closed-loop system.
What are the metrics, available sensors on the system, states what we want to regulate, to control?
What are the desired guarantees on the closed-loop system?
Should the system be fast?
Should it be allowed to oscillate, or to go over a certain limit?
What about disturbances?
What are the nature of the disturbances?
Can they be modelled?
Can they be anticipated?
In Control Theory jargon, the desired state of the system is called *setpoint* or *reference value*.

In our simple example, the objective is to regulate the temperature.
Lucky you, you have a thermometer on your board!
So your sensor will be the value returned by this thermometer.
There do not seem to be a lot of disturbances.
You can imagine that your fan could break at some point and thus the temperature would increase. 


### Identify the knobs

Once the goals are described, one needs to identify the knobs of action, also called *actuators*.
This means finding parts of the system whose variations impact the output of the system, positively or negatively. 
There might be several knobs of actions, or even some indirect knobs.
Knowing the range of action of your knob is quite important in order not to ask for impossible inputs.

In our example, we can think of several knobs of action.
The first one would be the amount of work given to the CPU.
Another knob would be the frequency of the CPU itself.
And this is much easier because you have a DVFS\footnote{Dynamic Voltage Frequency Scaling} mechanism on your CPU!
Your knob will thus be the frequency of the CPU.
The range of this knob is between the minimum and the maximum frequencies given on the CPU box.


### Devise the model

This phase consists in identifying the relation between the change in the knobs and the next value of the sensors.
In Control Theory terms, a *model* is a mathematical relation between the current state of the system (sensors) and the current input (knobs) to the next state of the system.
If you already have knowledge of the system you can come up with such a relation.
In physical systems, such knowledge could be physical laws (\eg\ Maxwell's equations, heat equation, etc.).
Otherwise, *identification* experiments are required.
During those experiments, the system is put under different types of inputs, and the output is observed.
One kind of input is a *stair function* where the value of the knob is constant for some time, then changes to a new value (lesser or greater), stays constants for some time, etc.
Then, with classical tools, such as linear regression, one is able to extract a model from those identification experiments. 

For our CPU, we might be able to derive a model from thermodynamic equations, but we can also perform identification experiments.
We can perform stair-shaped inputs, where the frequency would increase and then decrease and log the value of the temperature.
What we might observe is that there might be a delay between the action taken on the system, inertia, and its visible reaction.
This behavior can be taken into account in the model.


### Design the controller 

Once the model has been established, one can use the tools from Control Theory to design the controller adapted to the problem.
There are plenty of controllers in the literature, all with their pros and cons.
They can usually be derived from the model found previously, either by inserting the model into equations, or by using toolboxes like Matlab. 
During this phase, and based on the chosen controller, it might be possible to choose the closed-loop behavior of the system.
The main properties, illustrated in Figure \ref{fig:properties}, are the following:

- *Stability*: the system reaches an equilibrium point regardless of the initial conditions.

- *Absence of overshooting*: the system does not exceed the desired state (setpoint/reference value).

- *Settling time*: the system reaches the equilibrium point in a guaranteed time.

- *Robustness*: the system converges to the setpoint despite the imprecision of the underlying model.

\begin{figure}
    \centering
    \includegraphics[width=0.75\textwidth]{./figs/maggio.pdf}
    \caption{Graphical explanation of the different properties of a controlled system (stability, settling time, overshoot) \cite{shevtsov2017control}.}
    \label{fig:properties}
\end{figure}
 
In the case of our CPU example, a "simple" Proportional-Integral controller seems more than good enough to be robust to potential disturbances and to converge to the desired state.
Concerning the closed loop behavior, we really want to avoid overshooting as it could greatly damage the CPU.
We would also like to have a decent settling time (maybe in the order of the second) and robustness.


In the following, we present the most common controllers of Control-Theory.
Their description will be given in *discrete time* as we are in the context of computing systems.

#### Proportional Controller

The Proportional controller, or P controller, is the simplest controller of Control Theory.
The idea of this controller is to react proportionally to the *current* error, \ie\ the different between the desired state (setpoint) and the current state (sensor).
Note that the sign of the error is important as it helps the controller to steer the system towards the desired state. 
It is defined by a single gain $K_p$.

\begin{equation}\label{eq:p_controller}
  u(k + 1) = K_p \times e(k) = K_p \times \left(y_{ref} - y(k)\right)
\end{equation}

where $u$ is the value to apply to the knob, $y$ the value of the sensor, and $y_{ref}$ the reference value.

The issue of the P controller is its imprecision.
When designing a P controller, the experimenter must deal with some trade-off between all the desired closed-loop properties (stability, overshoot, settling time, precision, etc.).
Increasing the precision might come at the cost to increase the maximum overshoot and increase settling time.
In practice, P controllers are not often used due to these trade-offs.


#### Proportional-Integral Controller

One solution to improve the imprecision of the P controller is to add an Integral term on the *past* errors.
The role of the integral term is to compensate the past errors and thus cancel out the steady state error.
The integral part of the controller is parametrized by a gain noted $K_i$.

\begin{equation}\label{eq:pi_controller}
  u(k + 1) = K_p \times e(k) + K_i \times \sum_{j=0}^{k} e(j)
\end{equation}

The PI controller is the most popular one as it remains simple but offers good guarantees.

#### Proportional-Integral-Derivative Controller

The P reacts on the current error, the PI also on the past error, and it is possible to add a component to react on the future.
By adding a *derivative* term on the error to a PI, we get a PID controller.
The idea of the derivative, is to detect changes of trajectory.
The derivative part of the controller is parametrized by a gain noted $K_d$.

\begin{equation}\label{eq:pid_controller}
  u(k + 1) = K_p \times e(k) + K_i \times \sum_{j=0}^{k} e(j) + K_d \times \frac{e(k) - e(k-1)}{\Delta t}
\end{equation}

When a system is noisy, the computation of the derivative can lead to undesired behavior.
A solution would be to add a filter after the sensor to smooth out the value and thus compute the derivative.
But this defeats the purpose of the derivative term as the filter will slow down the reaction to the changes in trajectory.
Thus, in practice, the derivative term is rarely used.



### Trustable properties of the closed-loop system

After implementing the controller on the system, one should validate experimentally its closed-loop behavior compared to the expected ones of the design phase.
This usually requires performing experiments where a set-point is given to the controller, and then observe its behavior.


\begin{lesson}{Control Theory}{}
Control Theory provides clear methodologies which yield controllers with proven guarantees for exact models, and trustable behavior for inexact ones.
These controllers can also be robust to disturbances, and converge to the desired state within a desired time.
\end{lesson}
