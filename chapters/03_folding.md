---
lang: fr
---

# Repliement de systèmes distribués

## Définitions

Nous définissons **le repliement** comme le fait de mettre plusieurs ressources "virtuelles" sur une ressource physique.
Une ressource physique est une machine appartenant à une grappe de calculs.
Une ressource virtuelle est \todo{}.


Nous définissons également **le ratio de repliement** ($r_{\fold}$) comme étant le nombre total de ressources virtuelles divisé par le nombre total de ressources physiques:

\begin{equation}
  r_{\fold} = \frac{\# ressource_{virtuelle}}{\# ressource_{physique}} \in \left]0, 1\right]
\end{equation}

Ainsi, si le ratio de \fold\ est proche de 1, alors le système est proche de l'échelle 1:1.
Plus, le ratio de \fold\ est proche de 0, plus le système est replié.

\begin{figure}
     \centering
     \begin{subfigure}[b]{0.3\textwidth}
         \centering
         \includegraphics[width=\textwidth]{figs/folding_r_1.pdf}
         \caption{Repliement avec un ratio de 1 (4 ressources physiques et 4 ressources virtuelles).}
         \label{fig:r_1}
     \end{subfigure}
     \hfill
     \begin{subfigure}[b]{0.3\textwidth}
         \centering
         \includegraphics[width=\textwidth]{figs/folding_r_05.pdf}
         \caption{Repliement avec un ratio de 0.5 (2 ressources physiques et 4 ressources virtuelles).}
         \label{fig:r_05}
     \end{subfigure}
     \hfill
     \begin{subfigure}[b]{0.3\textwidth}
         \centering
         \includegraphics[width=\textwidth]{figs/folding_r_025.pdf}
         \caption{Repliement avec un ratio de 0.25 (1 ressource physique et 4 ressources virtuelles).}
         \label{fig:r_025}
     \end{subfigure}
        \caption{Exemple de repliements d'un système avec 4 ressources. \ref{fig:r_1} montre le système à l'échelle, \ref{fig:r_025} montre le système complétement replié et \ref{fig:r_05} montre le système dans un repliement intermédiaire.}
        \label{fig:fold_example}
\end{figure}

Avec cette définition, un système replié avec 100 ressources virtuelles repliées sur 10 ressources physiques a le même ratio de repliement qu'un système avec 10 ressources virtuelles repliées sur une ressource physique.
La figure \ref{fig:fold_example} présente un exemple de système avec plusieurs ratios de repliement.

## \Fold\ vs. Simulation

L'avantage de la simulation comparé au \fold\ est que le système cible peut être simulé sur une seule machine, qui peut ne pas appartenir à une grappe de calculs.
De plus, la simulation permet d'accélérer le temps et peut ainsi exécuter plus en moins de temps. \todo{berk}

Cependant, le désavantage de la simulation est que la modélisation du système se base sur des modèles approximant le véritable comportement du système.
Ceci n'est pas un problème avec le \fold\ car le véritable système est déployé.
Dans l'exemple de \cigri, le système déployé comprend bien des instances de \cigri, \oar\ et un système de fichiers distribué.


\newpage