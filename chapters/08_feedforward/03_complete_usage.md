## Towards a better usage of the resources {#sec:better_usage}


<!-- comment
In this section we present a PI controller for regulating the injection of BE jobs to achieve
close to 100% usage of the cluster

We then present an adaptive P controller requiring less knowledge about the jobs to run


we should also present some of the jobs and plots of the compas paper




In this section, we present a solution based on \CT\ to regulate the submission of \bag\ tasks from \cigri\ to improve the usage of a computing cluster.
The method follows the one presented in Chapter \ref{chap:harvest_fs}.
-->

### Problem Definition {#sec:problem_def}

In this work, we want to improve the usage of a set of computing clusters.
We identify two ways of misusing the computing resources.
The first one is to leave computing nodes idle.
The second one is to submit a \be\ job while risking that it will be killed in a near future.

One challenge is to keep enough \be\ jobs in the waiting queue of \oar\ so they can be started immediately when some resources are freed, but not too many to not overload the waiting queue.
An overload of the waiting queue can lead to a longer response time for the controller, as \oar\ will schedule the jobs no matter if they are destined to be killed or not.
<!---
A long response time can degrade the Quality-of-Service for the users.
--->


### Control Formulation

\begin{table}
\centering
\caption{Summary of the notations used.}\label{tab:notations}
\resizebox{\textwidth}{!}{
\begin{tabular}[t]{l p{12cm}}
\toprule
Notation & Definition\\
\midrule


$u_k$ & Number of \be\ resources submitted by \cigri\ at iteration $k$\\

$w_k$ & Number of \be\ resources in the \oar\ waiting queue at iteration $k$\\

$r_k$ & Number of used resources used by \be\ jobs in the cluster at iteration $k$\\

$y_k$ & Output of the sensor at iteration $k$. ($y_k = r_k + w_k$)\\

$r_{\max}$ & Total number of resources in the cluster (supposed constant)\\

$\bar{p_j}$ & Mean processing time of jobs in a \cigri\ campaign\\ 

$y_{ref}$ & \emph{Reference value}, or desired state of the system to maintain. ($y_{ref} = (1 \pm \varepsilon)\times r_{\max}$ with $\varepsilon \geq 0$)\\

$\Delta t$ & Time between two \cigri\ submissions (constant, chosen by the system administrator). 30 seconds in this paper.\\

$e_k$ & Control error. The difference between the desired state and the current state ($e_k = y_{ref} - y_k$)\\

$K_p, K_i$ & Proportional and Integral gains of a PI Controller\\

$h$ & Horizon value, amount of time in the future to look at the predictive Gantt of \oar.\\ 


\bottomrule
\end{tabular}
}
\end{table}

Our knob of action is the number of \be\ jobs submitted by \cigri\ at each of its iterations.
To sense the current state of the clusters, we can query the API of the \oar\ scheduler to get the number of resources currently used, the number of \be\ jobs waiting.
<!---
, or the provisional schedule of the jobs.
--->

For the sensor, we will consider resources and not jobs as we want 100\% of usage of the nodes.
Considering jobs instead of resources could lead to some degenerate situations.
One example would be \be\ jobs using 2 resources each and having a single idle resource on the cluster.
By considering jobs instead of resources, the controller could continue the injection of \bag\ jobs thinking that there is always some idle resources, even though it cannot be used.


We note:

- $u_k$: number of \be\ resources submitted by \cigri\ to \oar\ at iteration $k$.

- $r_k$: number of used resources by \be\ jobs on the cluster at iteration $k$.

- $w_k$: number of resources from \be\ jobs in the \oar\ waiting queue at iteration $k$.

- $r_{\max}$: the total number of resources in the cluster.


We thus want to regulate the quantity "number of currently used resources + number of \be\ resources waiting" around the total number of resources available in the cluster.
Meaning that we want to regulate $r_k + w_k$ around $r_{\max}$ by varying the value of $u_k$.

We note $y_k = r_k + w_k$.
We also note the *reference value* $y_{ref} = r_{\max}$, which is the control theory term for the desired state of the system.
The reference value could also be $(1 \pm \varepsilon)\times r_{\max}$, with $\varepsilon \geq 0$, based on the cluster administration preferences.
A reference value greater than $r_{\max}$ could lead to longer response time for the controller and more killing of \be\ jobs, whereas a reference value smaller than $r_{\max}$ will leave some resources idle but kill less jobs.

### System Analysis {#sec:sys_anal}


<!---
Let us take a campaign where jobs have a constant execution time of $p_j$.
In steady state, the amount of jobs finishing at each iteration $k$ is $\frac{\Delta t}{p_j} \times r_k$
--->

\begin{figure*}
\centering
\begin{subfigure}{0.55\textwidth}
  \includegraphics[width = \textwidth]{./figs/feedforward/identification.pdf}
  \caption{Identification experiment. We vary the size of the batch that \cigri\ send to \oar\ as steps. \cigri\ first submits batches of 5 jobs for 20 iterations, then batches of 10 jobs for another 20 iterations, then 15 jobs and finally 20 jobs.}
  \label{fig:identification}
\end{subfigure}
\hfill
\begin{subfigure}{0.4\textwidth}
  \includegraphics[width = \textwidth]{./figs/feedforward/hist_exec_times.pdf}
  \caption{Histogram of the execution times for a \cigri\ campaign during the identification experiment. The campaign is composed of synthetic jobs of theoretical duration 60 seconds.}
  \label{fig:hist_exec_times}
\end{subfigure}
\caption{Results of the identification of the system. Figure \ref{fig:identification} depicts the link between the input and the output of our system.
Figure \ref{fig:hist_exec_times} shows that the distribution of execution times is impacted by the commission and decommission of the nodes by \oar, and thus must be taken into account in the modelling.}
\end{figure*}


In this section, we investigate the relationship between $u_k$, the number of \be\ jobs submitted by \cigri\ at every iteration, and $y_k$, the number of \be\ resources in the system either waiting or running.
We perform the identification phase of the \CT\ methodology.

Let us consider a campaign of jobs with 60 seconds execution time.
We modified \cigri\ to submit the jobs of the campaign as steps.
Meaning that for the first 20 \cigri\ iterations, jobs are submitted by batches of 5.
Then, for the next 20 iterations, by batches of 10 jobs, then 15 jobs and finally 20 jobs.
We record the output of our sensor during the entire experiment and plot the results in Figure \ref{fig:identification}.

We observe that around 1500 seconds, \cigri\ starts to overflow the waiting queue.
And as a result, the value of the sensor does not stabilize but keeps increasing, until 2600 seconds where there are no more submission from \cigri.
This can be explained by the fact that \cigri\ is submitting more jobs than the cluster can process in one iteration.
Let $p_j$ be the execution time of the jobs of the campaign.
Note that we supposed that the jobs have the same execution times.
Then, the number of resources freed at each iteration is:

\begin{equation}\label{eq:rate_1}
  \frac{\Delta t}{p_j}\times r_{\max}
\end{equation} 

In our experiment, $\Delta t$ would be 30 seconds, $p_j$ 60 seconds, and $r_{\max}$ 32 resources.
Meaning that the cluster should be able to free up to $16$ resources per \cigri\ iteration.

However, when we look at the actual execution times of the jobs, we remark that the execution times are not constant.
Figure \ref{fig:hist_exec_times} shows the histogram of the execution times of the \cigri\ campaign used for the identification experiment.
As we can see, no job actually lasted the theoretical 60 seconds.
This is due to the node setup and node cleaning mechanisms of \oar.
To improve the precision of the processing rate, we can replace $p_j$ in Equation \ref{eq:rate_1} by the mean of the execution times ($\bar{p_j}$).
This modification leads to a processing rate of about 14.8 jobs per iterations.
This corrected rate thus explains why the value of the sensor starts growing when \cigri\ starts to submit batches of 15 jobs.

We want to avoid the overflowing the waiting queue.
Indeed, if the queue is not empty, \oar\ will try to schedule the \be\ jobs on the machines no matter if they are going to be killed soon or not.
By regulating the number of jobs in the waiting queue, we reduce the undesired killing of \be\ jobs.
But the objective is to have a 100\% usage of the cluster.


### Model \& Control Design {#sec:model_design}

From the discussion in the previous section, we can model the system as:

\begin{equation}\label{eq:model_true}
r_{k+1} + w_{k+1} = \left(1 - \frac{\Delta t}{\bar{p_j}}\right) \times r_k + w_k + u_k
\end{equation}


Equation\ \ref{eq:model_true} describes perfectly the behavior of the system.
However, we cannot transform this equation into a *linear* model easily exploitable by control theory tools.
More complex models might be possible, but as a first step we considered linear models.
Linear models have the following form:

\begin{equation}
y_{k+1} = \sum_i a_i \times y_{k-i} + \sum_i b_i \times u_{k-i} 
\end{equation}

with $a_i, b_i \in \mathbb{R}$.
However, as in our case $y_k = r_k + w_k$, this required formulation is impossible. 
As we are designing the controller to regulate $y_k$ around the value $r_{\max}$, we can suppose that all the resources are being used and that the cluster is able to process $\frac{\Delta t}{\bar{p_j}}r_{\max}$ jobs per \cigri\ iteration.
In this case, we can rewrite Equation \ref{eq:model_true} as:

\begin{equation}\label{eq:model}
\begin{aligned}
  r_{k + 1} + w_{k + 1} & \simeq r_k + w_k - \frac{\Delta t}{\bar{p_j}}r_{\max} + u_k \\
  \Leftrightarrow y_{k + 1} & = y_k + (u_k - \frac{\Delta t}{\bar{p_j}}r_{\max})
\end{aligned}
\end{equation}

$\frac{\Delta t}{\bar{p_j}}r_{\max}$ is called the operating region of the system.

<!---
The model in Equation \ref{eq:model} is what \CT\ calls a *linear first order model*.
--->
Knowing the model of the open-loop system, we can design a controller to regulate the closed-loop system.
Similarly to the work in Chapter \ref{chap:harvest_fs}, we decide to use a Proportional-Integral controller for its precision and robustness.
In the following, we use $k_s = 10$ and $M_p = 0$.

<!---
Figure \ref{fig:closed_loop} shows various closed-loop behaviors for various couple $(k_s, M_p)$.

The value of gains $K_p$ and $K_i$ are then computed following the methodology presented in \cite{hellerstein2004feedback}.
--->
<!---
\begin{figure}
  \centering
  \includegraphics[width = 0.45\textwidth]{./figs/closed_loop_behavior.pdf}
  \caption{
    Closed-loop behavior of the system for different choices of parameters.
    \todo{Small values of $k_s$ lead to faster response, but also potential overshoot and oscillations.
    Large values of $M_p$ lead to ...}
  }
  \label{fig:closed_loop}
\end{figure}

--->


<!-- comment
In [77]: a = 1 - 30/60

In [78]: b = 1

In [79]: ks = 2

In [80]: r = exp(-4/ks)

In [81]: theta = 0

In [82]: kp = (a - r * r) /b

In [83]: ki = (1 - 2 * r * cos(theta) + r * r)/b

In [84]: (kp, ki)
Out[84]: (0.48168436111126584, 0.7476450724155088)
-->

### Taking the future into account with help from the scheduler {#sec:gantt_sensor}

\begin{figure}
  \centering
  \begin{tikzpicture}[scale=0.9]
        \draw[->] (0, 0) -- node[pos=.5, below] {Time} (10, 0);
        \draw[->] (0, 0) -- node[pos=.5, above, rotate=90] {Resources} (0, 6.5);
        % \draw[-,dashed] (0, 2) -- (10, 2);
        % \draw[-,dashed] (0, 4) -- (10, 4);
        \draw[-,dashed] (0, 6) -- (10, 6);

        % #1
        \draw [pattern=north west lines, pattern color=gray](1, 0) rectangle (6, 2) node[midway]{Job \#1};;

        % #2
        \draw [pattern=north west lines, pattern color=gray](1.5, 2) rectangle (3, 6) node[midway]{Job \#2};

        % #3
        \draw [pattern=north west lines, pattern color=gray](3, 4.5) rectangle (7, 6) node[midway]{Job \#4};

        % #4
        \draw [pattern=north west lines, pattern color=gray](6, 0) rectangle (8, 4.5) node[midway]{Job \#3};

        \draw[-] (3.5, 0) -- node[above,pos=1.01] {Now} (3.5, 6.5);
        \draw[-, dashed] (6.5, 0) -- node[above,pos=1.01] {} (6.5, 6.5);
        \draw[->] (3.6, 6.2) -- node[pos=.5, above] {Horizon} (6.4, 6.2);
  \end{tikzpicture}
  \caption{
Graphical explanation of the prediction Gantt sensor.
The value returned by the sensor is the number of resources that will be used by normal jobs in \textit{horizon} seconds.
}
  \label{fig:gantt_sensor}
\end{figure}

For now, our controller only reacts to the instant changes in the system (arrival or departure of normal jobs).
To reduce the wasted computing time (idle and killed), we need a way to predict those changes on the system and take them into account in the controller.
Doing this would allow the controller to proactively increase or decrease the number of \cigri\ jobs to submit to \oar\ to avoid the killing of \be\ jobs or the idling of some resources.
We thus need a way to query the provisional schedule of \oar\ to extract information.

In the current state of \oar, the available information through the API are not enough to know the provisional schedule.
We thus slightly modified \oar\ (about 30 lines of code) to implement a new software sensor by introducing a new route in its API that returns the number of *normal* jobs that are predicted to be running at a given time in the future.
We call this time the horizon ($h$), and its value is decided in \cigri.
Figure \ref{fig:gantt_sensor} depicts the idea of this sensor.

There are several ways to inject the information returned by this new sensor into the controller.
We took inspiration from the *feedforward* techniques of Control Theory, and decided to change dynamically the reference value.
If we note $d^{h}_{k}$ the number of resources that will be used by normal jobs in $h$ seconds (the horizon), then we can redefine the reference value for the controller as $y_{ref,k} = r_{\max} - d^h_k$. 
Roughly, the reference value is the number of available resources for the \cigri\ jobs in $h$ seconds.
We also adapt the operating point in Equation \ref{eq:model}.

The value of the horizon is a parameter of the controller, which carries some trade off.
A small horizon value makes the sensor highly sensitive to miss evaluation of the walltimes of the normal jobs, and requires a fast response from the controller to meet the desired state.
This would lead to an increase of the killing of \be\ jobs.
On the other hand, a large horizon value might be too conservative and lead to more idle machines.

\begin{figure*}
  \centering
  \begin{tikzpicture}[scale=0.6]
    \draw [->] (-5, 5) -- (-2.9, 5) node[pos=.2, above] {$r_{\max}$};
    \draw [->] (-2.1, 5) -- (-0.4, 5) node[pos=.5, above] {$y_{ref,k}$};
    \draw [->] (0.4, 5) -- (2, 5) node[pos=.5, above] {$e_k$};
    \draw (2, 6) rectangle (6, 4) node[pos=.5] {Controller};
    \draw [->] (6, 5) -- (8, 5) node[pos=.5, above] {$u_{k}$};
    \draw (8, 6) rectangle (12, 4) node[pos=.5] {System};
    \draw [->] (12, 5) -- (16, 5);
    \draw [->] (14, 5) -- (14, 3) -- node[pos=0.5, above]{$y_k$} (0, 3) -- (0, 4.6);

    \draw (0, 5) circle (.4);
    \draw [-] (0.28, 5.28) -- (-0.28, 4.72);
    \draw [-] (-0.28, 5.28) -- (0.28, 4.72);
    \draw (.5, 4.5) node {-};
    \draw (-.3, 5.5) node {+};

    \draw (-2.5, 5) circle (.4);
    \draw [-] (-2.22, 5.28) -- (-2.78, 4.72);
    \draw [-] (-2.78, 5.28) -- (-2.28, 4.72);
    \draw (-3.2, 5.3) node {+};
    \draw (-2.8, 5.5) node {-};

    \draw [->] (10, 8) -- node[above,pos=0.01] {Disturbances} (10, 6);
    \draw [->] (10, 7) -- node[pos=0.5, above]{$d^h_k$}(-2.5, 7) -- (-2.5, 5.4);

    \end{tikzpicture}
  \caption{
Feedback loop representing the control scheme.
The reference value ($y_{ref}$) is proactively changed to take into account the future availability of the resources ($d^h_k$).
}
  \label{fig:loop}
\end{figure*}

Figure\ \ref{fig:loop} summarizes the control scheme of our system with this new sensor.

\begin{lesson}{}{}
By slightly modifying \oar\ we can extract information about the provisional schedule.
From this information, we adapt the reference value proactively to avoid the killing of best-effort jobs.
The introduction of this new sensor constitutes a more advanced coordination between the controller and the scheduler.
\end{lesson}


## Evaluation {#sec:eval}

### Experimental Setup {#sec:eval:setup}

The experiments were carried on the `dahu` cluster of \grid\ \cite{grid5000} where the nodes have 2 Intel Xeon Gold 613 with 16 cores per CPU and 192 GiB of memory.
The reproducibility of the deployed environments is ensured by \nxc\ \cite{nxc}.
The environments are available at \cite{cigri-feedforward}.
For each experiment, we deploy 3 nodes: one for the \oar\ server, one for \cigri, and one for a \oar\ cluster of 32 resources.
Note that we do not deploy 32 nodes for the cluster, but instead deploy a single node and define 32 \oar\ resources.
This choice is made with energy concerns in mind, as deploying a full size cluster to perform the following experiments would be an aberration.
We do deploy the real software stack (\oar, \cigri), but no real job is executed, only `sleep`s (see discussion about the job model in Chapter \ref{chap:cigri_jobs}).
This representation of the jobs allows us to emulate several \oar\ resources on a single physical machine without introducing noise to the sharing of computing resources for computation, communication, storage, etc.

### Experimental Protocol

We want to evaluate the controller proposed in Section \ref{sec:better_usage}.
We consider the following workload from the normal users of the cluster:
at time 1000, a new job arrives and takes 8 resources, then at time 1005, a job requesting 25 resources is submitted.
The second job has to wait for the first one to finish in order to start as there are not enough available resources for it to start.
With this scenario, we want to observe (*i*) the behavior of the controller when there is an abrupt change in available resources (arrival of the first job), (*ii*) the reaction to the killing of \be\ jobs, and (*iii*) how the controller will proactively decrease and increase the number of jobs to submit to meet the start and end of the second job.

We will evaluate the controller on this scenario with different parameters: different execution times of the \cigri\ jobs, and different horizon values for the sensor described in Section\ \ref{sec:gantt_sensor}.
We consider 3 execution times ($p_j$) for the \cigri\ campaigns: 30s, 60s, and 240s.
Those execution times correspond respectively to the first quartile, median, and third quartile of the execution times of the \cigri\ jobs on the *Gricad* platform (see Figure \ref{fig:ecdf_global}).
For the horizon, we consider 9 different duration: no horizon (or 0 second), 30s, 60s, 90s, 120s, 150s, 180s, 210s, 240s.
The horizon values are multiples of $\Delta t = 30s$ as smaller values would miss some behaviors.
The maximum horizon that we consider is 4 minutes which is the third quartile of the execution times.
Each experiment will be repeated 10 times to reduce the noise due to the time \oar\ needs to set up and clean the resources before and after a job. 


### Constant Injection taking into account the future

As a first comparison point, we also implemented a constant submission algorithm.
At each \cigri\ iteration, we submit $\frac{\Delta t}{\bar{p_j}} \times r_{\max}$.
The value of $\bar{p_j}$ is updated during the execution of the campaigns jobs.
This constant injection is supposed to fill exactly the cluster if there are no normal jobs.

We also made a variation of this submission algorithm by using the sensor defined in Section \ref{sec:gantt_sensor}.
In this variation we change the constant submission by taking into account the number of resources that will be available.
The value of $r_{avail}$ represents the number of resources available for \cigri.
In brief: $r_{avail,k} = r_{\max} - d^h_k$.
Thus, at each \cigri\ iteration, we submit $\frac{\Delta t}{\bar{p_j}} \times r_{avail, k}$.

Figure \ref{fig:box_constant} depicts the distribution of the percentage of computing time lost due to idle resources (left column) and due to killed jobs (right column).
We can see that considering the horizon does not have a noticeable impact on the percentage of lost time due to idle resources for $p_j = 30s\text{ and } 240s$.
In the case of $p_j = 60s$, we see that longer horizons lead to more idle resources, which is expected.
Note that for $p_j = 240s$, we see a decrease in the percentage of killed jobs with the increase of the horizon.
This decrease is not noticeable for the other processing times considered.
In our scenario, a constant submission will fill the waiting queue of \be\ jobs of \oar, and the jobs accumulated in the waiting queue will be scheduled by \oar\ and get killed when the second job starts.

\begin{lesson}{}{}
Taking the future into account in a step-based submission strategy does not yield any noticeable improvement of the usage of the resources.
\end{lesson}


### PI Controller taking into account the future

\begin{figure*}
\centering
\begin{subfigure}{0.77\textwidth}
  \centering
  \includegraphics[width=\textwidth]{./figs/feedforward/constant_horizon_boxplots.pdf}
  \caption{
Distribution of the lost compute times for the constant submission with horizon.
}
  \label{fig:box_constant}
\end{subfigure}
\hfill
\begin{subfigure}{0.77\textwidth}
  \centering
  \includegraphics[width=\textwidth]{./figs/feedforward/pi_horizon_boxplots.pdf}
  \caption{
Distribution of the lost compute times for the PI Controller with horizon.
}
  \label{fig:box_pi}
\end{subfigure}
\caption{
Distribution of the lost compute times due to idle resources (left column) and because of killing \be\ jobs (right column).
The x-axis represents the horizon of the sensor described in Section \ref{sec:gantt_sensor}.
Figure \ref{fig:box_constant} presents the results for the constant submission with horizon, and Figure \ref{fig:box_pi} for the PI Controller with horizon.
The dashed line is the mean lost time for the solution without horizon.
}
\end{figure*}

Figure \ref{fig:box_pi} depicts the distribution of the lost compute time due to idle resources and killed \be\ jobs for a PI Controller with various horizon lengths.
We can see that for small horizons (30 or 60 seconds), having this mechanism allows to reduce the idle time of the system.
However, the longer the horizon, the more the idle time, but the less killing of the jobs. 
**There is thus a trade-off between killing less and harvesting more**.
<!---
We found that this trade-off is well-balanced for a horizon value similar to the processing time of the \be\ jobs: $h \simeq \bar{p_j}$ 
--->

Compared to the constant injection depicted in Figure \ref{fig:box_constant}, we can see that the percentages of lost computing powers are slightly different.
A PI controller without horizon will lose around 5\% of computing power due to idle resources, while losing one percent due to killed jobs.
On the other hand, the constant controller will lose less concerning idle resources (around 2.5\%), and lose a lot more by killing jobs (about 2\%).
This is because of the accumulation of jobs in the waiting queue, which are scheduled by \oar\ even if they are destined to be killed.
This highlights again the added value of the feedback regulation of the system. 

\begin{figure}
  \centering
  \includegraphics[width=0.9\textwidth]{./figs/feedforward/control_pj_60_h_60.pdf}
  \caption{
Control signals for a scenario with $p_j = 60s$ and a horizon of 60s.
The top plot represents that number of resources submitted by \cigri\ through time.
The bottom plot depicts the value of our sensor, as well as the number of available resources to \cigri\ in dashed red.
}
  \label{fig:control}
\end{figure}

\begin{figure*}
  \centering
  \includegraphics[width=0.99\textwidth]{./figs/feedforward/gantt.pdf}
\caption{
Gantt chart for a scenario with $p_j = 60s$ and a horizon of 60s.
The killed jobs are depicted with a thicker contour.
}
  \label{fig:gantt}
\end{figure*}

Figure \ref{fig:control} shows the temporal evolution of the control signals for a scenario with a campaign of one-minute long jobs and a horizon value of 60 seconds.
The number of available resources through time is depicted in dashed red line.
We can see that the controller is able to adapt the number of jobs it submits to \oar\ (top graph) to meet the reference value (bottom graph). 
We do notice some overshooting around 100s and 3000s.
Those are due to a large variation in the reference value, but the controller manages to stabilize the system in a couple of minutes.
The overshooting could be tamed by changing the gains of the controller, and especially the $M_p$ parameter presented in Section \ref{sec:model_design}.
This issue might also come from the imprecision of our model due to the estimation presented in Section \ref{sec:sys_anal}.
Even if we designed the controller to reach the reference value within 10 \cigri\ iterations, it seems able to reach it less for small variations of reference value.
<!---
 (around t=1000s and t=2000s).
--->

The Gantt chart of the previous scenario is presented in Figure \ref{fig:gantt}.
The killed jobs are depicted with a thicker border.
We can see that there are some \cigri\ jobs killed at $t=1000s$, which is unavoidable for this scenario as the priority job starts immediately and the sensor controller cannot anticipate it.
Only three jobs are killed when the normal job starts at $t=2000s$.
We observe that there are some idle time right after the start of the second normal job.
This is due to the reaction of the controller, and it also can be observed on the bottom plot of Figure \ref{fig:control} around 2000 seconds, where the output signal is under the reference value (red dashed line).

 
\begin{lesson}{}{}
When using the additional information about the future schedule with a PI controller, we are able to improve both the number of idle resources and the number of killed jobs.
For large values of horizon, the controller anticipates too much and it leads to an increase of the idle time.
\end{lesson}

### Global Comparison

\begin{figure}
  \centering
  \includegraphics[width=0.75\textwidth]{./figs/feedforward/compa_energy_pi.pdf}
  \caption{
Gain of using the PI controller compared to the constant submission algorithm.
The top plot represents the gain from the point-of-view of the total energy consumption loss.
The bottom plot considers the computing time lost.
Values above one indicate that the PI out-performs the constant submission algorithm.
}
  \label{fig:compa_pi}
\end{figure}

From the point-of-view of energy consumption, idle resources and killed jobs do not consume the same amount of energy.
Indeed, in practice, the power used by a machine when a job is running is about twice more than when the machine is idle \cite{heinrich2017predicting}.
In this work, we did not measure the energy consumption of the jobs during the experiments as we are using `sleep`s to represent the CPU time.
To estimate the gain in energy consumption of the cluster for each strategy of submission presented in this paper, we will simply multiply by two the weight of the computing time lost due to the killing of jobs. 
<!---
Figure \ref{fig:compa_energy} shows the gains of using a PI controller compared to a constant submission.
The top plot depicts the gains in energy, and the bottom plot the gain in compute time.
--->

<!---
Values above one indicate that the PI performs better than the constant submission.
\begin{figure}
  \centering
  \includegraphics[width=0.5\textwidth]{./figs/compa_energy.pdf}
  \caption{
Gain of using the PI controller compared to the constant submission algorithm.
The top plot represents the gain from the point-of-view of the total energy consumption loss.
The bottom plot considers the computing time lost.
Values above one indicate that the PI out-performs the constant submission algorithm.
}
  \label{fig:compa_energy}
\end{figure}

\todo{take the best constant for each pj and compare with all PI}

--->


Figure \ref{fig:compa_pi} shows the gain of considering the prediction sensor in the PI Controller.
We look at two gains: the one in lost computing time (top plot) and the energy loss (bottom plot).
We can see that for all the considered processing times ($p_j$) just having a horizon of 30 seconds improves the usage of the cluster.
The best improvement is reached for a processing time of 60 seconds and a horizon of 60 seconds with about 25\% gains in computing time and 40\% in energy.

We think that it is possible to get the same behavior of the best horizon by changing the sampling time of \cigri\ ($\Delta t$) to match the characteristics of the \cigri\ campaign:

\begin{equation}\label{eq:deltat}
\Delta t \simeq \frac{\bar{p_j}}{2} \text{ and } h \simeq \bar{p_j}
\end{equation}

This choice of $\Delta t$ could be explained by the Nyquist frequency which stays that a process should be sampled twice faster than its dynamics.
Concerning the choice of the horizon value, the intuition is that values smaller than $\bar{p_j}$ could lead to some killing, and values larger than $\bar{p_j}$ could lead to the idling of machines. 

\begin{lesson}{}{}
Using a PI with information about the future improves the global usage of resources of 25\%.
If we consider the energy point-of-view, with a rough approximation that an idle node consumes twice less than a working node, then our controller can improve the energy usage by 40\%.
\end{lesson}

<!---
### Conclusion

Experiments showed that considering a PI Controller for the regulation of the injection helps to improve the total usage of a computing cluster.
A simple PI Controller will only reduce the amount of idle resources.
This is why we coupled it with a sensor on the number of resources that will be used by normal jobs in the future (*horizon*).
This consideration helps decreasing the number of \be\ killed, and, for small horizon values, also decreases the number of idle resources.
--->
