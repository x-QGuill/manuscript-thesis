## Conclusion and Future Work {#sec:conclu}

This chapter has presented an approach to the harvesting of idle computing resources in an HPC system using a feedback loop approach coupled with \CT\ tools.
Contrary to the harvesting solutions of the literature, we also considered the killed jobs as a source of wasted computing time.
We have shown that our solution, a Proportional-Integral Controller, can reduce the total amount of wasted computing time compared to an ad-hoc solution.
We show that by also taking into account the predictive Gantt chart of the scheduler, we are able to reduce even more the wasted computing time.
The choice of the horizon value has an impact on the total usage of the cluster.
A horizon value of 30 seconds yields an improvement for all the considered campaigns.
It is however possible to reach better performance by adapting the sampling time of \cigri\ to the running \bag\ campaign (see Equation \ref{eq:deltat}).

Our work shows promises, but has some limitations.
We supposed that the normal user know exactly the duration of their jobs and set the walltime accordingly, but in practice such behavior rarely happens \cite{cirne2001comprehensive, mu2001utilization}.
We think that the controller presented in this chapter should adapt relatively well to imprecise walltimes due to the robust nature of the Proportional-Integral controller.
However, we believe that the robustness to these imprecisions of walltimes could be improved by considering a probabilistic approach to the durations of the jobs.
Control Theory also proposes *Feedforward controllers* which bare the same idea as the controller above, but are more complex and powerful because they also model the disturbances.
Future work include to implement such a controller with the help of Control Theory experts.
