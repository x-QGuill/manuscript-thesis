## Related Works {#sec:sota}

<!-- comment
INTRO
-->

This work is related to two research areas: (*i*) the harvesting of idle computing resources, and (*ii*) the use of feedback loop management.

### Harvesting of Idle Resources

The usual approach to use the idle resources of a set of machines is to run small, independent, and interruptible tasks.
The most famous implementation of this idea is probably the BOINC project \cite{anderson2004boinc}, where any person with a computer can register their machine idle time to perform scientific computations, like looking for extraterrestrial signs with SETI\@Home \cite{anderson2002seti}.
The Condor project \cite{thain2005distributed} uses a similar idea to create a computing grid of worstations where user can submit their computations.

In the context of HPC systems, there have been a couple of approaches.
In \cite{mercier2017big}, the authors coupled the Hadoop YARN big data scheduler \cite{vavilapalli2013apache} with the \oar\ batch scheduler use the idle HPC resources with big data tasks.
This combination resulted in a full usage of the platform but degraded the mean waiting time of the HPC jobs.
Similarly, the approach presented in \cite{przybylski2022using} is using HPC idle resources to host a FaaS (Function as a Service) infrastructure. 
<!---
In both cases, the workload exploiting the idle resources does not come from HPC, but rather from fields where the usage of such machine is debatable.
--->
Those solutions only consider their impact in terms of scheduling (\eg\ mean waiting time of priority jobs).
However, the jobs exploiting the idle resources are still being executed on the machines and are using the shared resources of the cluster (\eg\ file-system, network, etc.) which can impact significantly the performances of the priority jobs.

In \cite{guilloteau2021controlling, guilloteau2022model}, the authors regulate the submission of \bag\ application with respect to the load of the distributed file-system of the cluster, exhibiting a trade-off between the amount of harvested resources and the degradation of performances for the priority users jobs using the file-system. 

The approach presented in this paper shows that taking into account the killed jobs in the computation of usage is important.

### Feedback Loop Management \& \CT

The feedback loop management philosophy introduced by \cite{kephart2003vision} led to several strategies to implement the decision mechanism: AI, rules, ad-hoc, or \CT.
The application of \CT\ on computing systems is only quite recent \cite{filieri2015software, filieri2017control}, even though it has been used on physical systems for centuries.
Some example of application of \CT\ can be found in the Cloud \cite{cerf2016cost}, Web servers \cite{abdelzaher2002performance}\todo{more}.

In HPC, the usage of \CT\ techniques is quite limited.
In \cite{cerf2021sustaining}, the authors use \CT\ to adapt a processor frequency based on the progress of the running application and a degradation factor provided by the users.
This approach showed that allowing a degradation of 10\% leads to a reduction of 22\% of the energy consumption of a node.
In \cite{stahl2018towards}, the authors present a \CT\ approach to improve the usage of a computing cluster. 
\cite{park2010predictable} \todo{}.
