## Introduction

Researchers have been studying the problem of harvesting idle HPC resources \cite{mercier2017big, thain2005distributed, przybylski2022using} by submitting jobs that are more flexible (smaller, interruptible).
These jobs are viewed as second class citizens by the scheduler and can be killed if needed.
However, these jobs are still being executed on the compute nodes and impact the shared resources of the cluster (\eg\ file-system, communication network), thus inevitably disturbing the jobs of normal users.
Meaning, there is a trade-off to exploit between the amount of harvesting and the maximum perturbation that these premium users can accept.
Unfortunately, these solutions introduce a new source of computing power waste by killing jobs.
Indeed, most of these small jobs do not implement a check-pointing mechanism, thus, all the computations done before the jobs are killed will be lost and will need to be started again.

In this chapter, we tackle the problem of harvesting the idle resources of a cluster while reducing the total amount of wasted computing time (from either idle resources or killed jobs) in the context of \cigri. 
<!---
We apply the \CT\ methodology to our regulation problem in Section \ref{sec:better_usage}.
The performance of the proposed solution are presented in Section \ref{sec:ff:eval}.
We discuss related works in Section \ref{sec:sota}, and finally conclude and give perspectives in Section \ref{sec:conclu}. 
--->
