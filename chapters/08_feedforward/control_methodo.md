## \CT\ Methodology {#sec:methodo}

This Section summarizes a \CT\ workflow to design a controller for a software system \cite{filieri2017control, filieri2015software}.

### Problem definition {#sec:methodo:pb_def}

The first step is to define the objectives, characteristics and range of action of the problem.

In this work, we want to improve the usage of a set of computing clusters.
We cannot use less than no resource and use more than the full cluster.
Section \ref{sec:problem_def} gives more details about the problem.

### Control formulation

Then, we need to define the means of action and sensors at our disposal to act on the system and get information about the state of the system.

Our knob of action is the number of \be\ jobs submitted by \cigri\ at each of its iterations.
To sense the current state of the clusters, we can query the API of the \oar\ schedulers to get the number of resources currently used, the number of \be\ jobs waiting, etc.

### System Analysis {#sec:methodo:sys_anal}

Once the knobs and sensors have been defined, their relationship to each other needs to be analyzed.
Meaning how the knobs influence the values of the sensors.
This analysis requires a specific experiments phase called *identification*.
This phase might highlight some new characteristics or challenges of the problem, which will require to update the problem definition (Section \ref{sec:methodo:pb_def})
We present this phase in Section \ref{sec:sys_anal}.

### Model and control design

With the system analysis (Section \ref{sec:methodo:sys_anal}), the problem definition (Section \ref{sec:methodo:pb_def}), and the desired behavior of the closed-loop system, we can choose and define an adequate controller.
There exists several types of controllers with each their pros and cons.
Once chosen, the controller needs to be tuned to the system.
This tuning is usually done with \CT\ theorems or toolboxes.  
We present this phase in Section \ref{sec:model_design}.

### Evaluation

Finally, we can evaluate the performance of the closed-loop system with the controller.
We present this phase in Section\ \ref{sec:eval}.
