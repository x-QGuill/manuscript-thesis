---
lang: fr
---

# Introduction


Tous les scientifiques expérimentaux ont besoin d'exécuter des calculs.
Ces calculs, par leurs demandes en charge CPU, mémoire ou \es, sont généralement exécutés sur des grappes ou grilles de calculs (\ie\ un ensemble de machines interconnectées).
Ces grilles de calculs subissent une demande croissante de la part des scientifiques et l'augmentation de leur usage introduisent de nouvelles problématiques.

Les chercheurs en \hpc\ et systèmes distribués se sont penchés sur ces problématiques et proposent des solutions.
Avant d'être disponible dans les grilles de calculs, ces solutions doivent être implémentées et testées. 
Ces intergiciels sont destinés à gouverner des centaines ou milliers de machines.
Ainsi, lors du développement, les expérimentateurs aimeraient déployer leurs solutions à grande échelle pour vérifier leur bon comportement.
Cependant, déployer autant de machines pour de simples tests est une perte considérable de puissance de calculs. 

Il existe diverses méthodes pour réduire le nombre de machines physiques déployées.
La simulation \todo{}...
Il est également possible de considérer une plus petite instance du système cible (\eg\ 100 machines au lieu de 1000 machines). 

Une autre approche est de replier le système sur lui-même.
C'est-à-dire définir plusieurs ressources "virtuelles" sur une ressource "physique".
Cela a l'avantage de garder un nombre de ressources "virtuelles" du même ordre de grandeur que le système réel, tout en réduisant le nombre de machines déployées.
Cependant, un potentiel désavantage est l'introduction de bruit dans le système (\eg\ surcharge CPU, contention réseau, etc).

Dans ce papier, nous étudions l'impact d'un repliement sur les performances d'une expérience distribuée effectuant des opérations \es.

\newpage