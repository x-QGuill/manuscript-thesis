# Conclusion and Perspectives {#chap:conclu_ctrl}



The \cigri\ middleware exhibits several regulations problems.
In this thesis, we focused on two aspects: regulation based on the load of the distributed file-system (Chapter \ref{chap:harvest_fs}), and regulation to reduce the amount of lost computing power (idle machines and killed jobs) (Chapter \ref{chap:ff}).
We showed that such problems can be addressed with tools from the fields of the Autonomic Computing and Control Theory.
The main limitation of these works is the need for a "strict" model of the target system.
In Chapter \ref{chap:reuse}, we investigated the impact in performance of reusing a controller designed on a system on another system.
We defined comparison criteria and gave guidelines for choosing a controller based on tradeoffs involving performance, complexity, guarantees, and competence required.

<!---
We also created a tutorial\ \cite{tuto_control} to share the notions of Control Theory for computer scientist, that has already been presented at four different occasions.
--->

Even if we tackled the regulations problems independently, we can merge the controller defined in Chapter \ref{chap:harvest_fs} and Chapter \ref{chap:ff} into a single Autonomic Controller that can satisfy the different objectives.
Figure \ref{fig:conclu:feedback} depicts what could be the single feedback loop for the different controllers and objectives. 


\afterpage{%
    \clearpage% Flush earlier floats (otherwise order might not be correct)
    \thispagestyle{empty}% empty page style (?)
    \begin{landscape}% Landscape page
        \centering % Center table
\begin{figure}[!h]
\centering
\begin{tikzpicture}[scale=0.8]

% FF
\draw [->] (-5, 5) -- (-2.9, 5) node[pos=.2, above] {$r_{\max}$};
\draw [->] (-2.1, 5) -- (-0.4, 5) node[pos=.5, above] {$y_{ref,k}$};
\draw [->] (0.4, 5) -- (2, 5) node[pos=.5, above] {$e_k$};
\draw (2, 6) rectangle (6, 4) node[pos=.5] {Controller};
\draw [->] (6, 5) --  node[pos=.5, above] {$u_{k}$} (10, 5) -- (10, 3.5);
%\draw (8, 6) rectangle (12, 4) node[pos=.5] {System};
%\draw [->] (12, 5) -- (16, 5);
%\draw [->] (14, 5) -- (14, 9.5) -- node[pos=0.5, above]{$y_k$} (0, 9.5) -- (0, 4.6);
\draw [->] (18, 2.5) -- (22, 2.5);
\draw [->] (20, 2.5) -- (20, 9.5) -- node[pos=0.5, above]{$r_k + w_k$} (0, 9.5) -- (0, 4.6);

\draw (0, 5) circle (.4);
\draw [-] (0.28, 5.28) -- (-0.28, 4.72);
\draw [-] (-0.28, 5.28) -- (0.28, 4.72);
\draw (.6, 5.5) node {-};
\draw (-.3, 5.5) node {+};

\draw (-2.5, 5) circle (.4);
\draw [-] (-2.22, 5.28) -- (-2.78, 4.72);
\draw [-] (-2.78, 5.28) -- (-2.28, 4.72);
\draw (-3.2, 5.3) node {+};
\draw (-2.8, 5.5) node {-};

\draw [->] (16, 8) -- node[above,pos=0.01] {Disturbances} (16, 3.5);
\draw [->] (16, 7) -- node[pos=0.5, above]{$d^h_k$}(-2.5, 7) -- (-2.5, 5.4);


\draw [->] (-5, 0) -- (-0.4, 0) node[pos=.5, above] {$load_{ref}$};
\draw [->] (0.4, 0) -- (2, 0) node[pos=.5, above] {$e_k$};
\draw (2,1) rectangle (6, -1) node[pos=.5] {Controller};
\draw [->] (6, 0) --  node[pos=.5, above] {$u_{k}$} (10, 0) -- (10, 1.5);

\draw [->] (20, 2.5) -- (20, -2) -- node[pos=0.5, above]{$load_k$} (0, -2) -- (0, -0.28);

\draw (0, 0) circle (.4);
\draw [-] (0.28, 0.28) -- (-0.28, -0.28);
\draw [-] (-0.28, 0.28) -- (0.28, -0.28);
\draw (.3, -0.5) node {-};
\draw (.3, 0.5) node {+};


\draw (8, 3.5) rectangle (12, 1.5) node[pos=.5] {Min $u_k$};
\draw [->] (12, 2.5) -- (14, 2.5) node[pos=.5, above] {$u_k$};
\draw (14, 3.5) rectangle (18, 1.5) node[pos=.5] {System};

\end{tikzpicture}
\caption{
Feedback loop gathering the controllers and objectives from Chapter \ref{chap:harvest_fs} (bottom) and Chapter \ref{chap:ff} (top).
At every iteration, we ran both controllers and take the minimum number of jobs in the submission ($u_k$) to satisfy the objectives.
In the case of a PI controller, we would only add the error to the integral part only if the chosen submission size comes from this controller.
It would be also possible to add the feedback loop presented in Figure \ref{fig:feedback:biphasic}.
}
\label{fig:conclu:feedback}
\end{figure}
    \end{landscape}
    \clearpage% Flush page
}


## Perspectives on \cigri

Perspectives on \cigri\ include:

##### Considering several clusters

In this document we consider a single cluster, but \cigri\ actually can submit to several clusters.
The immediate solution would be to have a single controller per cluster and apply the work in this thesis.
This approach could miss the opportunity to exploit potential affinity between campaigns and clusters.
Determining this affinity could be done by a combinatorial bandit approach \cite{cesa2012combinatorial, saha2019combinatorial} with a process of *exploration* where \cigri\ would submit jobs from a campaign to different clusters, and observe the performance of the jobs or the impact on the shared resources, and then *exploit* a chosen coupling between campaign and cluster until the next phase of exploration. 

##### Sampling time of \cigri

By default, the \cigri's loop is executed every 30 seconds.
This value is fixed and ad-hoc, but does bear some importance.
Indeed, this *sampling time* must be small enough to capture the behavior of the system, but not too small to react on noise.
In Chapter \ref{chap:harvest_fs} we take the \cigri\ jobs to have an execution time of 30 seconds.
This helps the controller as longer lasting jobs would add some *delay* in the system, which cannot be capture by a first order model, as used in this thesis.
One immediate solution would be to adapt the sampling time based on the current campaign being executed.
In Chapter \ref{chap:reuse}, we showed that the controllers were quite resistant to variation of the execution times.
But in Chapter \ref{chap:ff}, we concluded that adapting the sampling time of \cigri\ to half of the execution time of the jobs of the campaigns could lead to significant improvement of the global usage of the machines.
<!---
The question of the adequate sampling time for \cigri\ remains open.
--->


##### \io\ characteristics of \cigri\ jobs

In Chapter \ref{chap:harvest_fs}, we consider known the amount of \io\ done by a \cigri\ job.
In practice this information might be tricky to obtain.
Users could give an estimation of the \io\ load of their job, but there will be no guarantee on the quality of this estimation.
A heavy instrumentation and monitoring of the jobs and/or the system would make this information available and more precise than given by users.
Tools such as Darshan\ \cite{darshan1} or Colmet\ \cite{emeras2014analysis}, could help to get this information respectively by instrumenting the jobs or the platform. 
Using Adaptive controllers can help to reduce the need of such a characterization, but require more Control Theory knowledge, as seen in Chapter \ref{chap:reuse}.

<!---
##### Adaptive Controllers

The Control Theory field also provides advanced techniques and controllers that can adapt themselves at runtime to the variations of the system.
One example in the case of the \cigri\ would be the start of another campaign.
If the parameters of the controller are fixed, then its performances could degrade if the characteristics of the new campaign differ too much from the campaign the controller has been designed for (Chapter \ref{chap:reuse})
Adaptive controllers can change their parameters at runtime to give global guarantees.
However, setting up such a controller is not trivial and requires a strong Control Theory background.
There are tricky questions about the initialization of those controllers that can lead to some degenerate behaviors if not careful.
The intuition to understand those controllers is the following: after applying the input on the system (submitting jobs in the case of \cigri), the controller looks at the value of the sensor and compares it to the expected output given by its internal model of the system.
The controller then performs a least-mean square optimization of its parameters to make its model more realistic to the system.
--->

##### Sensors for Parallel File-Systems

The work presented in this thesis supposes the use of a centralized distributed file-system like NFS.
This assumption makes it easier to implement a sensor on a file-system.
However, in large HPC centers, shared file-systems are usually parallel with Lustre, BeeGFS, GlusterFS, etc.  
Such Parallel File-Systems (PFS) are composed of several nodes to balance the load.
Implementing a sensor on a PFS might be tricky.
A simple solution would be to keep the `loadavg` and average it over all the nodes of the PFS.
But this solution might lose the meaning of the `loadavg`, and weird behaviors might happen.
Another solution might be to look at the bandwidth of the PFS.
But such metrics could be greatly varying through time, and would require some sort of filtering to be usable by a controller.  

It might also be interesting to investigate even lower level sensors with the help of the eBPF technology.
The development of such sensors would enable to have very specific, and less noisy information on the system.


##### Try using models of greater orders

In this thesis, we only considered models, in the Control Theory sense, of the first order.
Such models have the advantage of being simple, but might also lack realism.
For instance, in some cases, the \cigri\ system might be impacted by delay, which should be considered in the model.
This delay requires to use a greater model order.

##### Phase detection

HPC applications are usually iterative and thus have repeating phases where they do computation, then perform \io\ operations, then computation again, etc. 
Detecting such \io\ phases (\eg\ \cite{stoffel2021phase, tarraf2023ftio}) would be a great additional information for the \cigri\ controller.
This new sensor can then be coupled with a feedforward control strategy to proactively adapt the submission of \cigri\ to avoid \io\ overload of the file-system.

##### Deployment onto the Gricad computing grid

The end goal is of course to deploy our controllers onto the real platform.
From the software management point-of-view, it should be quite easy as we maintain a fork of \cigri\ with our modifications \cite{cigri-src}.
It will require the additional work of setting up the controller, which is done with a single JSON file.

## Potential Regulation Problems of Interest

This Section presents some potential interesting systems which suffer from regulation problems that could be addressed with tools from Autonomic Computing and Control Theory.


<!---
##### Melissa (\cite{terraz_melissa})

\todo{reservoir}

A regulation arises in the Deep-Learning version of Melissa \cite{meyer2023high}.

\mel\ is a framework to run large-scale sensitivity analyses.
Its main specificity is the online processing of data to limit usage of intermediate file storage, contrary to postmortem approaches.
It can be used in several HPC environments as it is compatible with two resource managers (RM): Slurm \cite{yoo2003slurm} and OAR \cite{capit_batch_2005}.
\mel\ implements a client/server model where the clients are simulations that generate and send data to a server that runs the statistics algorithms.

--->

###### Bebida (\cite{mercier2017big})

In Bebida, a partition of the cluster is allocated for Big-Data jobs only, and the remaining of the platform is available for both HPC jobs and Big-Data jobs.
The authors showed that their solution improves the total usage of the resources at the cost of a higher mean waiting times for the HPC jobs.

As a first approach, the size of the partitions is fixed.
An interesting question would be to try regulating Quality-of-Service metrics for the HPC jobs based on the size of the partitions.
The actuator would be the size of the partition only of Big-Data jobs, and the sensor could be the waiting times of jobs, or the bounded slowdown of the HPC jobs.

Such a regulation could provide a finer Quality-of-Service to the users, while potentially also reducing the number of Big-Data jobs killed and rescheduled.
 

###### Turning on and off machines (Chapter 7 of \cite{poquet2017simulation})

When cluster nodes are not being used, one easy way to save energy is to turn them off.
However, when users need the turned off machines to run their jobs, the switching on process takes time and energy, which would degrade the Quality-of-Service for the users.
A regulation problem thus arises.
The objective would be to decide how many nodes to keep turned on based on metrics on the jobs (waiting time, bounded slowdown, etc.)
Additional sensors on the platforms like the provisional schedule, or historical usage metrics could also be used to increase precision.
The work from \cite{cerf:tel-02272258} can give inspiration.


###### Best-effort time-sharing with accepted degradation of performance

In the spirit of harvesting idle resources, the time-sharing of HPC applications on the same node could improve the usage of machines at a finer grain than \cigri\ for example.
A best-effort application could run in a `cgroup` which resource quotas are dynamically regulated to guarantee an accepted degradation of performance by the priority application running on the same node.
Similarly to \cite{cerf2021sustaining}, the priority application would need to be iterative and send a "heartbeat" \cite{ramesh2019understanding} to the controller at the end of each iteration.
The heartbeat frequency would be the signal to control, and the actuator the resources quotas of the `cgroup` hosting the best-effort application.

