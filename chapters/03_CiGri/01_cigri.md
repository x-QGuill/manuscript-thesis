## The Gricad Mesocenter

### Presentation

\cigri\ \cite{cigri} is a grid middleware in production at the French *Gricad* meso-center \footnote{\url{https://gricad.univ-grenoble-alpes.fr/index_en.html}}.
The goal of \cigri\ is to use the idle resources of the meso-center.
It interacts with several clusters managed by \oar\ \cite{capit_batch_2005} batch schedulers.

Users of \cigri\ submit *Bag-of-Tasks* applications to the middleware.
Such applications are composed of thousands of short, independent and similar tasks are classified as *embarrassingly parallel* which make them a good candidate for "filling the holes" in the cluster schedules.
Monte-Carlo simulations or parameter sweeps are examples of *Bag-of-Tasks* applications.

Once the set of tasks submitted to \cigri, the middleware will submit sub sets of jobs to the different schedulers of the grid.
The jobs are submitted with the lowest priority in order to allow premium users of the clusters to get the resources used by \cigri\ jobs if needed.

\begin{figure}
  \centering
  \includegraphics[width = 0.55\textwidth]{./figs/folding/schemas/cigri.png}
  \caption{%
  Interactions between \cigri\ and the schedulers \oar\ of the computing grid.
  \cigri\ users submit \textit{Bag-of-Tasks} applications, whose jobs are then submitted to the different cluster schedulers of the computing grid.
  The computing clusters are shared with users with more priority, thus \cigri\ jobs must be killed if one premium user requires the resources.}
  \label{fig:cigri}
\end{figure}

Figure \ref{fig:cigri} depicts the interactions between \cigri\ and different clusters of the grid.

### Limitation of \cigri

One problem of \cigri\ is its submission algorithm.
\cigri\ will submit a batch of jobs the one scheduler, and wait for the completion of the batch to submit again.
This strategy can lead to an underutilization of the cluster resources. 
Moreover, one objective of \cigri\ is to harvest in a *non-intrusive* fashion.
Meaning that the premium users of the different clusters must not notice the impact of the \cigri\ jobs on the platform.
However, once executing, the \cigri\ jobs are using the shared resources of the cluster (file-system, network, etc.), which can have an impact on the performance of every user jobs.

### Feedback Loop Regulation

We address these limitations of the current \cigri\ submission algorithm by considering the problem from the point of view of Autonomic Computing\ \cite{kephart2003vision}.
One aspect of Autonomic Computing is the self-regulation of systems, where the controlled systems is cyclically monitored via sensors, and then based on the sensors, the autonomic controller will act on the system to direct it towards a desired state.
The implementation of the decision process can be done via multiple techniques (IA, rules, modelisation and optimal solving, etc.).
But in our work\ \cite{guilloteau:icstcc, guilloteau:mfc}, we implement the autonomic controller with tools from Control Theory.
Control Theory is a field from physical engineering for the regulation of dynamic systems.
It has been used for centuries on physical systems, and its properties have been proven mathematically.
Its usage on computing systems is only recent.
To implement a controller with Control Theory tools, the definition of the signals as well as their dynamic must be clearly identified and modeled. 

