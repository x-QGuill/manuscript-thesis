# Conclusion and Perspectives {#chap:conclu_expe}

Distributed experiments are complex.
In this Thesis, we explored several research questions linked to distributed experiments.
In Chapter \ref{chap:nxc} we presented \nxc, a tool to set up and deploy reproducible distributed environment.
With \nxc\ we claim to have a tool to enable researchers to develop quickly and efficiently reproducible distributed experiments. 
This tool has already been used in various research and educational contexts (European projects, PEPRs, teaching of distributed systems, internships, etc.).
Chapter \ref{chap:folding} exhibits a method to reduce the number of machines to deploy when performing experiments involving a distributed file-system.
By folding a cluster, we can reduce the cost of a distributed experiment while keeping similar performance/behavior as the full scale system. 
Finally, in Chapter \ref{chap:batcigri}, we show a proof of concept to reduce even more the experimental cost of experimenting with \cigri\ by using simulation techniques.
These chapters aim to give an overview of the experimental spectrum: from full scale experiments, to reduced deployment, to simulation on a single machine.
Each of these techniques trade some realism for reduced experimental cost (either speed or number of machines required).

## Perspectives and open questions

##### Simulation of distributed file-systems

One of the limitations of the simulation of \cigri\ in \bat\ presented in Chapter \ref{chap:batcigri} is the lack of support for the distributed file-system required to reduce the experimental costs of studies like in Chapters \ref{chap:harvest_fs} and \ref{chap:reuse}.
The simulation of PFS is a complex task that some work have tried to tackle.
PFSSim \cite{liu2011towards} was the most promising project by developing a framework to evaluate \io\ scheduling algorithms, but the project has been inactive for 10 years.
StorAlloc \cite{monniot2022storalloc} focuses on the simulation of intermediate storage, such as burst buffers, between the compute nodes and the file-system. 
Recent work on \io\ model validation in Simgrid \cite{lebre2015adding} could lead to a validated framework to evaluating \io\ scheduling.
For now, Simgrid is able to simulate remote disks, files, and MPIIO calls.
But PFS are much more complex: metadata servers, data servers, \io, etc.


Being able to simulate the behavior of classical PFS within Simgrid would lead to a decrease of experimental costs.
Integration with \bat\ would be a plus.
Moreover, having a framework to test designs of PFS would enable system administrators to find the best PFS configuration for their system.

<!---
###### Porting \batcigri\ in Julia

A limitation of \batcigri, is the implementation in Python of the scheduler.
As the Ctrl-CiGri project involves scientists from the Control Theory field, the implementation of toolboxes similar to Matlab are not implemented in Python.
In an effort of open and reproducible science, using Matlab is out of question.
A solution to this issue would be to implement the \cigri\ scheduler for \bat\ in the Julia programming language\ \cite{julia}.
Julia has a large community for scientific computing and packages for Control Theory full of features\ \cite{ControlSystems.jl}.
An implementation of the \bat\ protocol in Julia has been initiated \cite{batsim.jl}, but needs to be continued.

##### OAR in Batsim

\todo{?}
--->

##### Integration between \nxc\ and \enos

As for now, \nxc\ does not provide its own engine to conduct experiments, and this is not our desire.
We developed an integration of \nxc\ into the Execo engine \cite{imbert_execo}.
Discussions have been started to support deployments with \nxc\ in \enos.
This integration would benefit \nxc\ with the large collection of tools from \enos, and would also benefit \enos\ with the integration of fully reproducible environments.

##### Filesytems and Functional Package Managers on HPC centers

The architecture of store-based package managers (\nix, *Guix*, or Spack) creates new kinds of stress on the underlying filesystems designed for classical FHS\footnote{Filesystem Hierarchy Standard} filesystems.
We recently added support in \nxc\ to deploy \nixos\ images with `kexec` which `/nix/store` are located on the shared NFS of \grid.
This reduces the size of the ramdisk image as it does not contain the store and thus increases deployment speed.
But it also raised interesting new questions.
As the systems are booting up, the different `systemd` services are starting, and as the `/nix/store` is located on the NFS, a **lot** of syscalls are being made to resolve the dynamic libraries of the services.
This problem has been called the *stat storm* (because of the `stat` system call).
The blog post \cite{stat_storm} gives a very nice presentation of the issue.
The hierarchy of the `/nix/store` catalyzes this problem by the explosion of the combination exploration required to resolve the dependencies.
Variations of this problem have starting to be addressed in the literature \cite{zakaria2022mapping, shaffer2017taming, frings2013massively}, but are mostly focused on the loading of user code and often require to modify it. 
During the Master 1 internship of Samuel Brun \cite{brun}, we started investigating a solution for \nxc\ based on FUSE \cite{szeredi2010fuse}, where the nodes loading the services will cache the result of the `openat` and `stat` syscalls from the NFS and share the results by multicast to the other nodes of the deployment.

##### Mixed deployments

When deploying for \cigri\ experiments for example, we do not deploy a 1-to-1 scale cluster, but instead, we define several computing resources (from the point-of-view of \oar) on a single machine.
This allows us to reduce the experimental cost while keeping the same production software stack.
To increase the realism of this deployment, \nxc\ could deploy several virtual machines containing the environment of a computing node onto one or several physical nodes.
This would allow for isolation between computing resources while reducing the number of physical nodes to perform an experiment.


##### Study of the lifetime of artifacts

The introduction of the artefacts' evaluation in the reviewing process in 2011 for conferences is a good step towards ensuring reproducibility of the submitted work.
However, as the artifacts are evaluated right after their creation, they pass the reviewing process.
In practice, a 6-month-old artifact that passed the reviewing process has a high probability to not build, or build to a different state than the original one.

A study of the *lifetime* of the artifacts, meaning the duration during which the artifacts rebuild from source the correct/intended one, of the top conferences implementing an artifact reviewing process would most likely reveal the imperfection of the usual tools, as seen in Chapter \ref{chap:distributed_expes}.

<!---
\todo{insister sur l'introduction de variation}
--->


##### Reproducibility metric and partial reproducibility

<!---
Not every research work can be fully reproducible.
In the context of distributed systems, the machines could reach the end of their life and get replaced.
Researchers can try to control as much as possible all the factors of their environments, but there will always be imponderable.
--->
The current badge systems during artifact reviews do not capture all the potential loss of reproducibility presented in Section \ref{sec:expe:repro:sw}.
A system indicating the pitfalls of the work, on several dimensions, might be more insightful for the readers, \eg\ "machine dependent", "using a non-free library".
Such criteria be evaluated by artifact reviewers with the help of a "checklist", without having to replay any experiments, only by inspecting the source code.
The filled checklist would then be attached to the paper after publication.
The creation of such a checklist that would cover most reproducibility cases would be a great asset for reviewer to ease the reviewing process, readers to know what to expect from the presented results, and authors to know what to improve.

The potential issue with such an approach would be the backlash from the community.
Today, authors gain badges for doing more in terms of reproducibility, and this is a way for the publisher to gently invite authors to improve the quality of their submission.
However, as the artifact review is not always mandatory, and that failing the reproducibility review does not stop the publication of the paper.
A system as presented above takes the problem from the other side and would explicitly point to the shortfalls of the work, which could be seen as "public shaming" by authors. 

The current artifact reviewing system is far from perfect, and must evolve towards something more strict and precise.
Considering that it took 10 years for the artifact reviews to be integrated into most conferences, changing the existing system might be long.


###### Environmental cost of reproducibility in computer science

Reproducing research works also has costs.
First, humans need to evaluate the reproducibility of the papers, which is time-consuming and often require a lot of efforts\ \cite{artefact}.
But also an environmental cost.
Replaying experiments requires using additional computing resources, and in the case of large scale and long-lasting experiments this could represent a significant energetic cost.

Similar, but at smaller scales, problematics arise from the use of functional package managers.
When replaying an experiment that was packaged with a FPM, the first step is to set up the software environment.
This step can be very long as it will need to download a lot of packages and their dependencies.
Moreover, if the work is not recent, or is introducing variation deep into the software stack, the packages required will not be present in the binary caches, and will thus need to be recompiled from scratch.
Storage is also an issue of FPMs as the stores have the tendency to grow very large with every variation of packages.

Such questions should be addressed by the community.
Is a long-lasting experiment requiring hundreds of machines really reproducible?
Should reviewers even try to reproduce it?
Verifying reproducibility has a cost, but also ensures research quality.
Is there a tradeoff between the two?
Is the peer-review scheme reaching its limits?




<!---

##### Implement Nix/Guix support in SnakeMake

\todo{?}
--->
