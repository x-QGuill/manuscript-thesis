{
  description = "A very basic flake";


  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/22.11";
    sput.url = "github:GuilloteauQ/stupid-tracker";
  };

  outputs = { self, nixpkgs, sput }:
    let
      system = "x86_64-linux";
      # system = "aarch64-darwin";
      pkgs = import nixpkgs { inherit system; };
    in
    {

      packages.${system} = rec {

        thesis = with pkgs;
          stdenv.mkDerivation {
            name = "thesis";
            src = ./.;
            buildInputs = [
              snakemake
              rubber
              pandoc
              texlive.combined.scheme-full
            ];
          };
        default = thesis;
        dockerImage = pkgs.dockerTools.buildImage {
         name = "registry.gitlab.inria.fr/qguillot/manuscript-thesis";
          tag = "0.0";
          contents = with pkgs; [ 
              snakemake
              rubber
              pandoc
              texlive.combined.scheme-full
              dockerTools.binSh
              bashInteractive
              coreutils
              gnugrep
              bibtool
          ];
        };
      };
      devShells.${system} = {
        dev = pkgs.mkShell {
          buildInputs = with pkgs; [
            snakemake
            rubber
            pandoc
            bibtool
            texlive.combined.scheme-full
            graphviz
          ];
        };
        default = pkgs.mkShell {
          buildInputs = [
            sput.packages.${system}.tracker
            sput.packages.${system}.plot
            pkgs.gnome.pomodoro
          ];
        };
      };
    };
}
