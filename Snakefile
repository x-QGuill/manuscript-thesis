configfile: "config/config.yaml"

INTRO =  [
    "01_Introduction/00_header",
    "01_Introduction/01_need_hpc",
    "01_Introduction/02_hpc_issues",
    "01_Introduction/03_harvesting",
    "01_Introduction/04_ac",
    "01_Introduction/99_conclu"
]
SOTA = [
    "02_SotA/00_hpc",
    "02_SotA/01_harvesting",
    "02_SotA/02_ac",
    "02_SotA/03_control",
    "02_SotA/04_distributed_expes",
    "02_SotA/99_conclu"
]
CIGRI = [
    "03_CiGri/00_header",
    "03_CiGri/01_cigri"
]
JOBS_MODEL = [
    "04_Jobs_CiGri/00_header",
    "04_Jobs_CiGri/01_Introduction",
    # "04_Jobs_CiGri/02_Context",
    "04_Jobs_CiGri/03_Study",
    "04_Jobs_CiGri/04_Conclusion"
]
HARVESTING = [
    "05_harvesting/pi"
]
FF = [
    "08_feedforward/00_header",
    "08_feedforward/01_Introduction",
    # "08_feedforward/context",
    # "08_feedforward/control_methodo",
    "08_feedforward/03_complete_usage",
    # "08_feedforward/02_SotA"
    "08_feedforward/07_conclu"
]

NXC = [
    "10_nxc/00_header",
    "10_nxc/01_Introduction",
    "10_nxc/02_SotA",
    "10_nxc/03_nxc",
    "10_nxc/04_how_it_works",
    "10_nxc/06_melissa",
    "10_nxc/05_eval",
    "10_nxc/07_conclu"
]

FOLDING = [
    "11_folding/00_header",
    "11_folding/01_Introduction",
    "11_folding/02_Motivating_example",
    "11_folding/03_defs",
    "11_folding/04_protocol",
    "11_folding/05_eval",
    "11_folding/06_conclusion",
    "11_batcigri/batcigri"
]

DIST_EXPES = [
    "09_distributed_expes/main"
]


CHAPTERS = INTRO + ["part1"] + SOTA + JOBS_MODEL + HARVESTING + ["07_reuse/main"] + FF + ["conclu_part1/main", "part2"] + DIST_EXPES + NXC + FOLDING + ["conclu_part2/main"] + ["meta_conclu"]


INDIVIDUAL_CHAPTERS = [INTRO, SOTA, JOBS_MODEL, FF, DIST_EXPES, NXC, FOLDING]


BAD_EXTENSIONS = [
    "aux",
    "bbl",
    "blg",
    "bcf",
    "lof",
    "log",
    "lot",
    "rubbercache",
    "run.xml",
    "toc"    
]

rule all:
	input:
		"main.pdf",
        "slides.pdf",
	#	expand(["{chap_name}.pdf"], chap_name=config["chapters"]),

rule thesis:
    input:
        "deps.pdf",
        "main.tex",
        "references.bib"
    output:
        "main.pdf"
    shell:
        "pdflatex main.tex && biber main.bcf && pdflatex main.tex && pdflatex main.tex"
        # "nix develop .#dev --command pdflatex main.tex && biber main.bcf && pdflatex main.tex && pdflatex main.tex"

rule slides:
    input:
        slides="slides.tex",
        refs="references.bib"
    output:
        "slides.pdf"
    shell:
        "pdflatex {input.slides} && biber slides.bcf && pdflatex {input.slides} && pdflatex {input.slides}"

rule slides_tex:
    input:
        slides="slides.md",
    output:
        "slides.tex"
    shell:
        "pandoc --from=markdown --to=beamer --template=template_slides.tex --output={output} {input.slides}"



rule thesis_tex:
    input:
        "main.md",
        expand(["chapters/{chap_name}.md"], chap_name = CHAPTERS) 
    output:
        "main.tex"
    shell:
        "pandoc --from=markdown --to=latex --template=template.tex --output={output} {input} --top-level-division=chapter"

rule gen_chap_deps:
    input:
        "chapters/deps.dot"
    output:
        "deps.pdf"
    shell:
        "dot {input} -Tpdf > {output}"

def get_files(wildcards):
    chap_name = wildcards.chap_name
    return list(map(lambda x: f"chapters/{chap_name}/{x}.md", config["chapters"][chap_name]))

rule chapters_tex:
    input:
        "main.md",
        lambda wc: get_files(wc)
        # expand("{file}", file=params.files) #config["chapters"][wildcards.chap_name])  
    output:
        "{chap_name}.tex"        
    shell:
        "nix develop .#dev --command pandoc --from=markdown --to=latex --template=template_chap.tex --output={output} {input} --top-level-division=chapter"

rule chapters:
    input:
        "{chap_name}.tex",
        "references.bib"
    output:
        "{chap_name}.pdf",
        # expand(["{{chap_name}}.{ext}"], ext = BAD_EXTENSIONS) 
    shell:
        # "nix develop .#dev --command pdflatex {wildcards.chap_name}.tex && biber {wildcards.chap_name}.bcf && pdflatex {wildcards.chap_name}.tex && pdflatex {wildcards.chap_name}.tex"
        "nix develop .#dev --command rubber -d {wildcards.chap_name}.tex"
		
rule movev_chapters:
    input:
        expand(["{chap_name}.pdf"], chap_name = CHAPTERS)
    output:
        expand(["out/{chap_name}.pdf"], chap_name = CHAPTERS) 
    shell:
        "mkdir -p out && mv {input} out"
		
rule clean:
    shell:
        "rm *.aux *.bbl *.blg *.bcf *.lof *.log *.lot *.rubbercache *.run.xml *.toc"        
